package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import javax.swing.JFrame;


public class ClientV3 extends JFrame{
	private static final long serialVersionUID = 1L;

	// TODO send pong every 30 seconds
	// Windows
	static WindowLayout windows = new WindowLayout();

	// Streams
	BufferedReader in;
	PrintWriter out;

	// initiate timer
	Timer timer = new Timer();
	ScheduleTask sched = new ScheduleTask();

	// booleans
	boolean running = true;
	boolean processingQuery = false;
	boolean processingDisplay = false;
	boolean processingUpdate = false;
	boolean processingChat = false;
	boolean processingGame = false;
	boolean processingItems = false;
	boolean processingLeave = false;
	boolean processingPlayer = false;
	boolean displayQuery = true;
	boolean loggedIn = false;

	// Strings // not used
	String line = "";
	String sessionName = "";
	String sessionPassword = "";
	String playerName = "";
	public static String[] itemsArray;
	public static String[] leaveArray;
	public static String[] sylladexArray;

	public ClientV3(){

		// Initiate Gui
		gui();

		// open stream
		try {
			initStream();
		} catch (IOException e) {
			e.printStackTrace();
			displayGameText("Skaianet is Unavailable. This window will close in 5 seconds.");
			try {
				TimeUnit.SECONDS.sleep(5);
			} catch (InterruptedException e1) {
				System.exit(0);
			}
			System.exit(0);
		}
	}

	public void gui(){
		//setUndecorated(true); //borderless
		add(windows);

		setTitle("MultiSburb V.002");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(900, 550); //setSize(410, 280);
		setLocationRelativeTo(null);
		setVisible(true);
		setResizable(false);
	}

	public void initStream() throws IOException{
		// Make connection and initialize streams
		String serverAddress = "127.0.0.1";//"174.27.17.242"; //
		Socket socket = new Socket(serverAddress, 9001);
		in = new BufferedReader(new InputStreamReader(
				socket.getInputStream()));
		out = new PrintWriter(socket.getOutputStream(), true);
		windows.setOut(out);
		timer.scheduleAtFixedRate(sched, 3000, 3000); // start timer

		// Process all messages from server, according to the protocol.
		while(running) {
			// If Socket is Ready, get data
			if(in.ready()){
				line = in.readLine();
				System.out.println("Received: "+line);
				if(line.startsWith("CHAT")){ processingChat = true; System.out.println("Processing chat");}
				else if(line.startsWith("GAME")){ processingGame = true; System.out.println("Processing game");}
				else if(line.startsWith("UPDATE")){ processingUpdate = true; System.out.println("Processing update");}
				else if(line.startsWith("ITEMS")){ processingItems = true; System.out.println("Processing Items");}
				else if(line.startsWith("LEAVE")){ processingLeave = true; System.out.println("Processing Leave");}
				else if(line.startsWith("PLAYER")){ processingPlayer = true; System.out.println("Processing Player Movement");}
				else{ displayGameText("Invalid Server Responce: "+ line); }
			}

			if(processingGame){
				// Trim GAME off of line
				String gameMessage = line.substring(5);
				// If Text is for displaying
				if(gameMessage.startsWith("DISP ")){
					gameMessage = gameMessage.substring(5);
					windows.comdTextArea.append(gameMessage + '\n');
					//processingGame = false;
				}
				// If Text is a question
				if(gameMessage.startsWith("Q ")){
					//if(displayQuery == true){
					displayGameText(line.substring(6));
					windows.allowInputCommand(true); // allow entering text
					//displayQuery = false;
					//}
					//getGameInput();
				}
				processingGame = false;
			}
			if(processingChat){
				// Trim CHAT off of line
				String chatMessage = line.substring(5);
				// If Text is for displaying
				if(chatMessage.startsWith("DISP ")){
					chatMessage = chatMessage.substring(5);
					displayChatText(chatMessage);
				}
				else if(chatMessage.startsWith("LIST+ ")){
					String user = chatMessage.substring(6);
					System.out.println(user);
					// Append name if not already there
					if(!windows.userList.getText().contains(user)){
						windows.userList.append(user+'\n');
					}
				}
				else if(chatMessage.startsWith("LIST- ")){
					String user = chatMessage.substring(6);
					// If user is on list
					if(windows.userList.getText().contains(user)){
						// Remove user from list
						int indexStart = windows.userList.getText().indexOf(user);
						windows.userList.replaceRange("", indexStart, indexStart+user.length()+1);
					}
				}
				else if(chatMessage.startsWith("LOGGEDIN")){
					windows.setChatEditable();
					loggedIn = true;
				}
				else{
					displayGameText("Error CHAT message not understood: " + chatMessage);
				}
				processingChat = false;
			}
			if(processingItems){
				//TODO Process Items
				// Trim "ITEMS "
				String items = line.substring(6);
				System.out.println("items: \"" +items + "\"");
				// Send to RoomPanel
				windows.roomPan.setItems(items);
				// Send to itemsArray
				itemsArray = items.split(" \\| ");
				Arrays.sort(itemsArray);
				processingItems = false;
			}
			if(processingLeave){
				//TODO Process Leave
				// Trim "LEAVE "
				String leave = line.substring(6);
				// Send to RoomPanel
				windows.roomPan.setLeave(leave);
				// Send to leaveArray
				leaveArray = leave.split(" \\| ");
				Arrays.sort(leaveArray);
				processingLeave = false;
			}
			if(processingPlayer){
				//TODO Process Player Movement
			}
			if(processingUpdate){
				String update = line.substring(7); // remove "Update"
				if(update.startsWith("STAT ")){
					String stat = update.substring(5);
					if(stat.startsWith("HP ")){
						// TODO write a setter in StatPanel
						// TODO write something on server that sends this
						//windows.displayedHP = Integer.parseInt(stat.substring(3));
					}
				}
				else if(update.startsWith("ECHELADDER ")){
					String eche = update.substring(11);
					windows.statPan.setEche(eche); //TODO make something send this on server
				}
				else if(update.startsWith("INVENTORY ")){
					String inventoryString = update.substring(10); // remove "inventory"
					System.out.println("Inventory: \""+ inventoryString+"\""); 
					// create array
					String[] inv;
					if(inventoryString.contains(" ")){
					inv = inventoryString.split(" ");
					}
					else{
						inv = new String[1];
						inv[0] = "";
					}
					for(int i = 0; i<inv.length;i++){
						System.out.println("Slot:" + i + " Item: \"" + inv[i] +"\"");
					}
					// update Inventory
					if(!inv.equals(null)){
						windows.sylladexPan.updateInventory(inv);
						//windows.repaint();
					}
					// Send to sylladexArray[]
					sylladexArray = inv;
					Arrays.sort(sylladexArray);
				}
				else if(update.startsWith("ROOM ")){
					// Draw Square
					windows.mapPan.setRoom(update.substring(5));
				}
				else if(update.startsWith("LOCATION ")){
					// Trim Location off of string
					String types = update.substring(9); // contains width height and roomTypes
					System.out.println("Update Location:"+types);
					// Get types/ Draw Map
					if(!types.equals("0 0")){
						windows.mapPan.setRoomTypes(types);
					}
					else{
						System.out.println("Empty Location Change");
					}
				}
				else if(update.startsWith("COLORS ")){
					windows.mapPan.setPlanetColors(update.substring(7));
				}
				else if(update.startsWith("TIMER ")){
					String time = update.substring(6);
					windows.statPan.setTimer(time);
				}
				processingUpdate = false;
			}
			try {
				//System.out.println("Sleep 10ms");
				TimeUnit.MILLISECONDS.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		socket.close();
	}

	public void displayGameText(String s){
		windows.displayCommand(s);
	}
	
	public void displayChatText(String s){
		windows.displayChat(s);
	}

	public static void main(String[] args) {
		new ClientV3();
	}

	class ScheduleTask extends TimerTask {
		public void run() {
			out.println("P");
		}
	}
}
