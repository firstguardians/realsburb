package main;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintWriter;
import java.util.Arrays;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import panels.MapPanel;
import panels.RoomPanel;
import panels.StatPanel;
import panels.SylladexPanel;


public class WindowLayout extends JPanel{
	private static final long serialVersionUID = 1L;
	PrintWriter out;

	//JPanel a = new JPanel(); // Sylladex
	public SylladexPanel sylladexPan = new SylladexPanel();
	RoomPanel roomPan = new RoomPanel(); // Items and Players in room
	//JPanel c = new JPanel(); // Map
	public MapPanel mapPan = new MapPanel();
	StatPanel statPan = new StatPanel(); // Grist/Boondollars
	JPanel comdPan = new JPanel(); // Input
	JPanel chatPan = new JPanel(); // Chat

	// Command Line
	JTextArea comdTextArea = new JTextArea();
	JScrollPane comdScroll = new JScrollPane(comdTextArea); 
	JTextField comdBox = new JTextField();

	// Chat
	JTextArea chatTextArea = new JTextArea();
	JScrollPane chatScroll = new JScrollPane(chatTextArea); 
	JTextField chatBox = new JTextField();
	// Users
	JPanel userPanel = new JPanel();
	JTextArea userList = new JTextArea();

	//
	String userInput = ""; // stores what user typed
	public int displayedHP; // Set but not Displayed
	public boolean inputReady; // Signals moving on in the process


	public WindowLayout(){
		initGui();
	}

	public void initGui(){
		commandBoxE();
		chatBoxF();

		// Main Background color - Divider color
		setBackground(Color.DARK_GRAY);
		// Main Layout
		GridLayout gE = new GridLayout(2, 3);
		gE.setHgap(5);
		gE.setVgap(5);
		setLayout(gE);

		// Panel Labels
		//a.add(new JLabel(" Panel A "));
		//b.add(new JLabel(" Panel B "));
		//c.add(new JLabel(" Panel C "));
		//d.add(new JLabel(" Panel D "));
		//e.add(new JLabel(" Panel E "));
		//f.add(new JLabel(" Panel F "));

		// Background color
		sylladexPan.setBackground(Color.BLACK);
		roomPan.setBackground(Color.black);
		mapPan.setBackground(Color.black);
		statPan.setBackground(Color.black);
		chatPan.setBackground(Color.black);

		// Add panels to main panel
		add(sylladexPan);
		add(statPan);
		add(mapPan);
		add(roomPan);
		add(comdPan);
		add(chatPan);
	}

	private void commandBoxE(){ // Game Chat
		// E - Text Area
		comdTextArea.setSelectedTextColor(Color.black);
		comdTextArea.setSelectionColor(Color.green);
		comdTextArea.setEditable(false);
		comdTextArea.setLineWrap(true);
		comdTextArea.setWrapStyleWord(true);
		comdTextArea.setMargin(new Insets(0,2,0,0)); // Top,Left
		comdScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		comdScroll.getVerticalScrollBar().setBackground(Color.black); // Works

		comdTextArea.setBackground(Color.BLACK);
		comdTextArea.setForeground(Color.GREEN);

		// E - Input Box
		comdBox.setCaretColor(Color.green);
		comdBox.setBackground(Color.BLACK);
		comdBox.setForeground(Color.GREEN);
		comdBox.setEditable(true);
		comdBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				userInput = "";
				userInput = comdBox.getText();
				String lowInput = "";
				lowInput = userInput.toLowerCase(); //I'm not typing out userInput.toLowerCase() that many times.
				System.out.println("lowInput:"+lowInput);
				//Check if room item is required
				if (lowInput.startsWith("captcha ")||lowInput.startsWith("c ")||lowInput.startsWith("inspect ")||lowInput.startsWith("i ")) {
					String focusItem = lowInput.replaceFirst("\\S+\\s", ""); //Remove command prefix
					//System.out.println("focusItem:"+focusItem);
					for (int i = 0; i < ClientV3.itemsArray.length; i++) {
						//System.out.println("Testing:"+ClientV3.itemsArray[i]);
						if (ClientV3.itemsArray[i].toLowerCase().matches(focusItem+".*")) {
							userInput = userInput.replaceFirst("\\s.*", " "+ClientV3.itemsArray[i]); //Replace object in user input with new object
							comdBox.setText(userInput); //For when it's actual tab completion. Also for the input display to work.
							System.out.println("Match:"+ClientV3.itemsArray[i]);
							
							i = ClientV3.itemsArray.length; //End loop on first match.
						}
					}
				}
				//Check if inventory item is required
				if (lowInput.startsWith("decaptcha ")||lowInput.startsWith("d ")) {
					String focusItem = lowInput.replaceFirst("\\S+\\s", ""); //Remove command prefix
					//System.out.println("focusItem:"+focusItem);
					for (int i = 0; i < ClientV3.sylladexArray.length; i++) {
						//System.out.println("Testing:"+ClientV3.sylladexArray[i]);
						if (ClientV3.sylladexArray[i].toLowerCase().matches(focusItem+".*")) {
							userInput = userInput.replaceFirst("\\s.*", " "+ClientV3.sylladexArray[i]); //Replace object in user input with new object
							comdBox.setText(userInput); //For when it's actual tab completion. Also for the input display to work.
							System.out.println("Match:"+ClientV3.sylladexArray[i]);
							
							i = ClientV3.sylladexArray.length; //End loop on first match.
						}
					}
				}
				//Check if use item (Inventory or Room)
				if (lowInput.startsWith("use ")||lowInput.startsWith("u ")) {
					String focusItem = lowInput.replaceFirst("\\S+\\s", ""); //Remove command prefix
					//System.out.println("focusItem:"+focusItem);
					for (int i = 0; i < ClientV3.itemsArray.length; i++) {
						//System.out.println("Testing:"+ClientV3.itemsArray[i]);
						if (ClientV3.itemsArray[i].toLowerCase().matches(focusItem+".*")) {
							userInput = userInput.replaceFirst("\\s.*", " "+ClientV3.itemsArray[i]); //Replace object in user input with new object
							comdBox.setText(userInput); //For when it's actual tab completion. Also for the input display to work.
							System.out.println("Match:"+ClientV3.itemsArray[i]);
							
							i = ClientV3.itemsArray.length; //End loop on first match.
						}
					}
					for (int i = 0; i < ClientV3.sylladexArray.length; i++) {
						//System.out.println("Testing:"+ClientV3.sylladexArray[i]);
						if (ClientV3.sylladexArray[i].toLowerCase().matches(focusItem+".*")) {
							userInput = userInput.replaceFirst("\\s.*", " "+ClientV3.sylladexArray[i]); //Replace object in user input with new object
							comdBox.setText(userInput); //For when it's actual tab completion. Also for the input display to work.
							System.out.println("Match:"+ClientV3.sylladexArray[i]);
							
							i = ClientV3.sylladexArray.length; //End loop on first match.
						}
					}
				}
				//Check if leave is required
				if (lowInput.startsWith("goto ")||lowInput.startsWith("g ")) {
					String focusItem = lowInput.replaceFirst("\\S+\\s", ""); //Remove command prefix
					//System.out.println("focusItem:"+focusItem);
					for (int i = 0; i < ClientV3.leaveArray.length; i++) {
						//System.out.println("Testing:"+ClientV3.leaveArray[i]);
						if (ClientV3.leaveArray[i].toLowerCase().matches(focusItem+".*")) {
							userInput = userInput.replaceFirst("\\s.*", " "+ClientV3.leaveArray[i]); //Replace object in user input with new object
							comdBox.setText(userInput); //For when it's actual tab completion. Also for the input display to work.
							System.out.println("Match:"+ClientV3.leaveArray[i]);
							
							i = ClientV3.leaveArray.length; //End loop on first match.
						}
					}
				}
				comdTextArea.append("=> " + comdBox.getText()+'\n'); // Display Input
				comdBox.setText(""); // Clear Input Box
				out.println("GAME "+userInput);
				System.out.println("Sent: GAME " + userInput);
				//inputReady = true; // Signal that text is ready to be sent
			}
		});

		// Layout E
		comdPan.setLayout(new BorderLayout());

		// Color E
		comdPan.setBackground(Color.black);

		// Add components to panel E
		comdPan.add(comdScroll, BorderLayout.CENTER);
		comdPan.add(comdBox, BorderLayout.SOUTH);
	}

	private void chatBoxF(){ // Pesterchum Lite
		// Pester Banner
		JPanel pesterBanner = new JPanel();
		pesterBanner.setBackground(Color.orange);
		JLabel pester = new JLabel("PESTERCHUM LITE");
		pester.setForeground(Color.white);



		// F - Text Area
		chatTextArea.setSelectedTextColor(Color.black);
		chatTextArea.setSelectionColor(Color.LIGHT_GRAY);
		chatTextArea.setEditable(false);
		chatTextArea.setLineWrap(true);
		chatTextArea.setWrapStyleWord(true);
		chatTextArea.setMargin(new Insets(0,2,0,0)); // Top,Left
		chatScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		chatScroll.getVerticalScrollBar().setBackground(Color.orange); // Works

		chatTextArea.setBackground(Color.white);
		chatTextArea.setForeground(Color.black);

		// F - Input Box
		chatBox.setBackground(Color.LIGHT_GRAY); // light grey when not editable
		chatBox.setForeground(Color.black);
		chatBox.setEditable(false); // Cannot enter until logged in
		chatBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//textAreaF.append("??: " + inputBoxF.getText()+'\n');
				String typed = chatBox.getText(); // +'\n'
				out.println("CHAT " + typed);
				System.out.println("Sent: CHAT " + typed);
				chatBox.setText("");
			}
		});

		// Layout F
		chatPan.setLayout(new BorderLayout());

		// Color F
		chatPan.setBackground(Color.black);
		userPanel.setBackground(Color.white);
		userList.setBackground(Color.white);

		// Add components to panel F
		pesterBanner.add(pester);
		userPanel.add(userList);
		chatPan.add(userPanel, BorderLayout.EAST);
		chatPan.add(pesterBanner, BorderLayout.NORTH);
		chatPan.add(chatScroll, BorderLayout.CENTER);
		chatPan.add(chatBox, BorderLayout.SOUTH);
	}

	public void displayCommand(String s){
		comdTextArea.append(s + '\n');
		comdTextArea.setCaretPosition(comdTextArea.getDocument().getLength());
	}
	
	public void displayChat(String s){
		chatTextArea.append(s + '\n');
		chatTextArea.setCaretPosition(chatTextArea.getDocument().getLength());
	}

	public void allowInputCommand(Boolean tf){
		comdBox.setEditable(tf);
		if(tf)
		{
			comdBox.setBackground(Color.black);
		}
		else{
			comdBox.setBackground(Color.gray);
		}
	}

	public void allowInputChat(Boolean tf){
		chatBox.setEditable(tf);
		if(tf)
		{
			chatBox.setBackground(Color.white);
		}
		else{
			chatBox.setBackground(Color.LIGHT_GRAY);
		}
	}

	public void setOut(PrintWriter o) {
		out = o;
	}

	public void setChatEditable() {
		chatBox.setEditable(true);
		chatBox.setBackground(new Color(0xFFFFE0));
	}

}
