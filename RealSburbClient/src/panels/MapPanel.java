package panels;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;


public class MapPanel extends JPanel{
	private static final long serialVersionUID = 1L;

	Boolean readyForDrawing = false;

	// Location
	int mapW;
	int mapH;

	//String location;
	int[][] roomType;
	int rx;
	int ry;

	// Map Drawing variables
	int squareSize;
	int squareSpacing;

	// Planet Colors // These will come from the server
	Color liquid = new Color(0x99CCFF);
	Color alt1 = new Color(0xFFCC99);
	Color alt2 = new Color(0xFF9966);
	Color alt3 = new Color(0xCC9933);
	Color alt4 = new Color(0x996600);
	Color alt5 = new Color(0x663300);

	// Drawing x and y
	int xCo = 0;
	int yCo = 0;

	public MapPanel(){
		this.setVisible(true);
	}


	/**
	 * Set the location of the yellow square.
	 * @param xy
	 */
	public void setRoom(String xy){
		xy.trim();
		String[] splitxy = xy.split(" ");
		rx = Integer.parseInt(splitxy[0]);
		ry = Integer.parseInt(splitxy[1]);
		repaint();
	}
	/**
	 * Draw the entire map
	 * @param stringBlob
	 */
	public void setRoomTypes(String stringBlob){
		System.out.println("StringBlob: "+stringBlob);
		// Get mapW
		int spaceIndex = stringBlob.indexOf(" ");

		mapW = Integer.parseInt(stringBlob.substring(0,spaceIndex));//+1; // Dimension
		System.out.println("mapW: " + mapW);
		stringBlob = stringBlob.replaceFirst(Integer.toString(mapW), ""); // was mapW-1
		stringBlob = stringBlob.trim(); // remove first space

		// Get mapH
		spaceIndex = stringBlob.indexOf(" ");
		try{
			mapH = Integer.parseInt(stringBlob.substring(0,spaceIndex));//+1;
		}
		catch(IndexOutOfBoundsException e){
			e.printStackTrace();
			System.out.println("H: " + mapH);	
		}
		stringBlob = stringBlob.replaceFirst(Integer.toString(mapH), ""); // was mapH-1
		stringBlob = stringBlob.trim(); // remove first space

		// Print final String
		System.out.println("Removed W/H: "+stringBlob);

		// String to String Array
		String[] stringArray = stringBlob.split(" ");
		System.out.println("SplitString: ");
		for(int i = 0; i< stringArray.length;i++){
			System.out.print(stringArray[i]+", ");
		}
		System.out.print('\n');

		// StringBlob to Ints
		int[] intBlob = new int[mapW * mapH]; // true dimensions 20*20=400 squares for planet, 12*1 for house
		System.out.println("StringArray Length "+stringArray.length);
		for(int i = 0;i< stringArray.length;){
			intBlob[i] = Integer.parseInt(stringArray[i]);
			i++;
		}

		roomType = new int[mapW][mapH];

		// convert Int[] to Int[][]
		System.out.print("roomType: ");
		int z = 0;
		for(int y = 0; y < mapH;){
			for(int x = 0; x <mapW;){
				roomType[x][y] = intBlob[z]; // string to int
				System.out.print(roomType[x][y] + " ");
				z++;
				x++;
			}
			y++;
		}
		System.out.print('\n');

		readyForDrawing = true;
		repaint();
	}

	public void paint(Graphics g){
		System.out.println("Painting");
		// Get Screen Dimensions
		int w = getWidth();
		int h = getHeight();

		// Draw Background
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, w, h);
		// Set color
		g.setColor(Color.GREEN);
		//TODO Write Location Name

		// Draw Rooms
		// Starting point for drawing
		xCo = 40;
		yCo = 25;

		if(readyForDrawing){
			System.out.print("Type: ");
			for(int y = 0; y < mapH;){
				// label x
				//g.setColor(Color.cyan);
				//g.drawString(x+"", xCo-10, yCo);
				for(int x = 0; x < mapW;){ 
					System.out.print(roomType[x][y] + " ");
					if(roomType[x][y] == -1){
						g.setColor(Color.black);
					}
					if(roomType[x][y] == 0){
						g.setColor(liquid);
					}
					if(roomType[x][y] == 1){
						g.setColor(alt1);
					}
					if(roomType[x][y] == 2){
						g.setColor(alt2);
					}
					if(roomType[x][y] == 3){
						g.setColor(alt3);
					}
					if(roomType[x][y] == 4){
						g.setColor(alt4);
					}
					if(roomType[x][y] == 5){
						g.setColor(alt5);
					}
					// SKAIA
					if(roomType[x][y] == 12){ // Black Skaia
						g.setColor(new Color(0x212121));
					}
					if(roomType[x][y] == 13){ // White Skaia 
						g.setColor(Color.white);
					}
					// PROSPIT
					if(roomType[x][y] == 37){ // Shops
						g.setColor(new Color(0xe88b00));
					}
					if(roomType[x][y] == 38){ // Castle
						g.setColor(Color.white);
					}
					if(roomType[x][y] == 39){ // Road
						g.setColor(Color.yellow);
					}
					if(roomType[x][y] == 40){ // Houses
						g.setColor(new Color(0xe8b400));
					}
					if(roomType[x][y] == 41){ // Fountain 
						g.setColor(new Color(0x99CCFF));
					}
					if(roomType[x][y] == 42){ // Chain
						g.setColor(new Color(0x756400));
					}
					// DERSE
					if(roomType[x][y] == 43){ // Shops
						g.setColor(new Color(0xb418af));
					}
					if(roomType[x][y] == 44){ // Castle
						g.setColor(new Color(0x7F00FF)); // Med Slateblue
					}
					if(roomType[x][y] == 45){ // Road
						g.setColor(new Color(0x2E0854)); // indigo - very dark
					}
					if(roomType[x][y] == 46){ // Houses
						g.setColor(new Color(0x9B30FF)); // purple1 - mid
					}
					if(roomType[x][y] == 48){ // Chain
						g.setColor(new Color(0xc51471));
					}

					if(mapH < 20){
						squareSize = 20;
					}
					else{
						squareSize = 10;
					}
					// Draw Square
					g.fillRect(xCo, yCo, squareSize, squareSize); // 10x10 for planet 20x20 for other maps

					// label y
					//g.setColor(Color.white);
					//g.drawString(y+"", xCo, yCo);

					// Current Location Marker
					if(x == rx && y == ry){
						g.setColor(Color.yellow);
						g.drawRect(xCo-1, yCo-1, squareSize + 1, squareSize + 1);
					}
					// House Marker
					// Quest Bed Marker
					// Transportalizer Markers
					x++;
					xCo = xCo + squareSize+1; // 21
				}
				y++;
				xCo = 40;
				yCo = yCo + squareSize+1;
			}
			System.out.print('\n');
			xCo = 40;
			yCo = 25;
		}
	}

	public void setPlanetColors(String colorString){
		// 0 1 2 3 4 5 6 7 8
		// String to String[]
		String[] split = colorString.split(" ");
		// String[] to int[]
		int[] values = new int[split.length];
		for(int i=0; i<values.length;i++){
			values[i] = Integer.parseInt(split[i]);
		}
		// int[] to color[]
		Color[] c = new Color[values.length/3];
		int r = 0;
		for(int i=0;i<values.length/3;i++){
			c[i] = new Color(values[r],values[r+1],values[r+2]);
			r = r+3;
			System.out.println("Color"+i+" :" + c[i].toString());
		}
		liquid = c[0];
		alt1 = c[1];
		alt2 = c[2];
		alt3 = c[3];
		alt4 = c[4];
		alt5 = c[5];
	}
}
