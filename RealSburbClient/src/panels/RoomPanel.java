package panels;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;

public class RoomPanel extends JPanel{

	TitledBorder itemsTitle = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(0x254117)),"Items on Map");
	TitledBorder leaveTitle = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(0x254117)), "Accessable Areas");
	TitledBorder playerTitle = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(0x254117)), "Players on Map");

	JTextArea itemsArea = new JTextArea("None");
	JScrollPane itemsPane = new JScrollPane(itemsArea);
	JTextArea leaveArea = new JTextArea("None");
	JScrollPane leavePane = new JScrollPane(leaveArea);
	JTextArea playerArea = new JTextArea("None");
	JScrollPane playerPane = new JScrollPane(playerArea);
	GridLayout grid = new GridLayout(3, 1);

	public RoomPanel(){
		itemsTitle.setTitleColor(Color.green);
		leaveTitle.setTitleColor(Color.green);
		playerTitle.setTitleColor(Color.green);

		itemsArea.setBackground(Color.black);
		itemsArea.setForeground(Color.green);
		itemsArea.setSelectedTextColor(Color.black);
		itemsArea.setSelectionColor(Color.green);
		itemsPane.setBackground(Color.black);
		itemsPane.setBorder(itemsTitle);
		itemsArea.setEditable(false);
		itemsArea.setLineWrap(true);
		itemsArea.setWrapStyleWord(true);

		leaveArea.setBackground(Color.black);
		leaveArea.setForeground(Color.green);
		leaveArea.setSelectedTextColor(Color.black);
		leaveArea.setSelectionColor(Color.green);
		leavePane.setBackground(Color.black);
		leavePane.setBorder(leaveTitle);
		leaveArea.setEditable(false);
		leaveArea.setLineWrap(true);
		leaveArea.setWrapStyleWord(true);

		playerArea.setBackground(Color.black);
		playerArea.setForeground(Color.green);
		playerArea.setSelectedTextColor(Color.black);
		playerArea.setSelectionColor(Color.green);
		playerPane.setBackground(Color.black);
		playerPane.setBorder(playerTitle);
		playerArea.setEditable(false);
		playerArea.setLineWrap(true);
		playerArea.setWrapStyleWord(true);

		setLayout(grid);
		add(itemsPane);
		add(leavePane);
		add(playerPane);
		setVisible(true);
	}

	public void setItems(String items){
		if(items.equals("")){
			itemsArea.setText("None");
		}
		else{
			itemsArea.setText(items);
		}
	}

	public void setLeave(String leaves){
		if(leaves.equals("")){
			leaveArea.setText("None");
		}
		else{
			leaveArea.setText(leaves);
		}
	}

	public void setPlayers(String players){
		if(players.equals("")){
			itemsArea.setText("None");
		}
		else{
			playerArea.setText(players);
		}
	}

}
