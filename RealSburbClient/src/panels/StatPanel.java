package panels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

public class StatPanel extends JPanel{
	private static final long serialVersionUID = 1L;

	// Layout
	JLabel notoriety = new JLabel("--");
	TitledBorder notorietyTitle = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(0x254117)),"Notoriety");

	JLabel boondollars = new JLabel("0");
	TitledBorder boonTitle = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(0x254117)),"Boondollars");

	JLabel eche = new JLabel("--");
	TitledBorder echeTitle = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(0x254117)),"Rung");

	JLabel hp = new JLabel("0");
	TitledBorder hpTitle = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(0x254117)),"Gel Viscosity");

	JLabel mangrit = new JLabel("0");
	TitledBorder mangritTitle = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(0x254117)),"Mangrit");

	JLabel gent = new JLabel("0");
	TitledBorder gentTitle = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(0x254117)),"Gent's Manor");

	JLabel vigor = new JLabel("0");
	TitledBorder vigorTitle = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(0x254117)),"Youth Vigor");

	JLabel magic = new JLabel("0");
	TitledBorder magicTitle = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(0x254117)),"Magiykks");

	JLabel exhaustion = new JLabel("0");
	TitledBorder exhaustTitle = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(0x254117)),"Exhaustion");

	JLabel timer = new JLabel("00:00:00");
	TitledBorder timerTitle = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(0x254117)),"Timer");

	public void setName(String pname) {
		notoriety.setText(pname);
	}

	public void setEche(String ech) {
		eche.setText(ech);
	}

	public void setTimer(String time) {
	//// Split max time
				int r = Integer.parseInt(time);
				hours = 0;
				minutes = 0;
				seconds = 0;
				// hours
				while(r-3600 >= 0){
					r = r-3600;
					hours++;
				}
				// minutes
				while(r-60 >= 0){
					r = r-60;
					minutes++;
				}
				// seconds
				while(r-1 >= 0){
					r = r-1;
					seconds++;
				}
				
				// Format Time
				secondsStr = String.format("%02d", seconds);
				minutesStr = String.format("%02d", minutes);
				hoursStr = String.format("%02d", hours);
				
		timer.setText(hoursStr + ":" + minutesStr + ":" + secondsStr);
	}

	// Timer
	Boolean showTimer = true;
	public int timeLeft = 0;
	int hours;
	int minutes;
	int seconds;
	String hoursStr = "--";
	String minutesStr = "--";
	String secondsStr = "--";

	public StatPanel(){
		// JPanel this
		setVisible(true);
		GridLayout grid = new GridLayout(5, 2);
		setLayout(grid);

		// Name
		notoriety.setForeground(Color.green);
		notorietyTitle.setTitleColor(Color.green);
		notoriety.setBorder(notorietyTitle);

		// Timer
		timer.setForeground(Color.green);
		timerTitle.setTitleColor(Color.green);
		timer.setBorder(timerTitle);

		// Boondollars
		boondollars.setForeground(Color.green);
		boonTitle.setTitleColor(Color.green);
		boondollars.setBorder(boonTitle);

		// Echeladder
		eche.setForeground(Color.green);
		echeTitle.setTitleColor(Color.green);
		eche.setBorder(echeTitle);

		// HP
		hp.setForeground(Color.green);
		hpTitle.setTitleColor(Color.green);
		hp.setBorder(hpTitle);

		// Viscosity
		mangrit.setForeground(Color.green);
		mangritTitle.setTitleColor(Color.green);
		mangrit.setBorder(mangritTitle);

		// Mangrit
		gent.setForeground(Color.green);
		gentTitle.setTitleColor(Color.green);
		gent.setBorder(gentTitle);

		// Imagination
		vigor.setForeground(Color.green);
		vigorTitle.setTitleColor(Color.green);
		vigor.setBorder(vigorTitle);

		// Intellect
		magic.setForeground(Color.green);
		magicTitle.setTitleColor(Color.green);
		magic.setBorder(magicTitle);

		// Prankster's Gambit
		exhaustion.setForeground(Color.green);
		exhaustTitle.setTitleColor(Color.green);
		exhaustion.setBorder(exhaustTitle);

		add(notoriety);
		add(timer);
		add(boondollars);
		add(eche);
		add(hp);
		add(mangrit);
		add(gent);
		add(vigor);
		add(magic);
		add(exhaustion);
	}
}
