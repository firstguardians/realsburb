package panels;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class SylladexPanel extends JPanel{
	private static final long serialVersionUID = 1L;

	int capNumber = 30;
	int capNumberAdd = 15;
	int drawHere = 30;
	int spaceBetweenEntries = 15;

	String[] inventory = new String[10];


	public SylladexPanel(){
		this.setVisible(true);
	}
	
	public void paint(Graphics g){
		//System.out.println("Sylladex graphics");
		// Get Screen Dimensions
		int w = getWidth();
		int h = getHeight();

		// Draw Background
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, w, h);
		// Set color
		g.setColor(Color.GREEN);

		// Draw Header
		int width = this.getWidth();
		System.out.println("Sylladex panel width: "+width);
		g.drawString("## Sylladex ##",(width/2) - 40,15);

		// Draw Numbers
		for(int forI1 = 1; forI1 <= 10;){
			String leadingNum = String.format("%02d", forI1);
			g.drawString(leadingNum + ": ",5,capNumber);
			capNumber += capNumberAdd;
			forI1++;
		}
		capNumber = 30;

		// Draw Items
		captchaHandler(g);
	}

	public void captchaHandler(Graphics g){
		//TODO still recieving an empty row here
		if(inventory[0] != null){
			for(int itemNum = 0; itemNum < inventory.length;){
				if(inventory[itemNum] != null){
					String lowerInv = inventory[itemNum];
					System.out.println(inventory[itemNum]);
					// TODO out of bounds exception because ""  - fixed but might have something more to it
					if(!lowerInv.equals("")){
					g.drawString(lowerInv.substring(0, 1).toUpperCase()+lowerInv.substring(1),25,drawHere);
					}
					drawHere = drawHere + spaceBetweenEntries;
					itemNum++;
				}
				else {
					break;
				}
			}
			drawHere = 30;
		}
	}

	public void updateInventory(String[] newInventory){
		inventory = newInventory;
		this.repaint();
	}
}
