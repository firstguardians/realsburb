package spammer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EmuPlayer extends Thread {
	private CyclicBarrier gate;

	// Streams
	BufferedReader in;
	PrintWriter out;

	// Pong Timer
	Timer timer = new Timer();
	ScheduleTask sched = new ScheduleTask();

	String password = "fox";
	String name = "Test";
	String handle;

	public EmuPlayer(String handle, CyclicBarrier gate){
		this.handle = handle;
		this.gate = gate;
	}
	
	public void run(){
		try {
			System.out.println("gate waiting");
			gate.await();
			System.out.println("gate ran");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BrokenBarrierException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// open stream
		try {
			initStream();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
	}

	public void initStream() throws IOException{
		// Make connection and initialize streams
		String serverAddress = "127.0.0.1";//"174.27.17.242";//
		Socket socket = new Socket(serverAddress, 9001);
		in = new BufferedReader(new InputStreamReader(
				socket.getInputStream()));
		out = new PrintWriter(socket.getOutputStream(), true);
		// Pong
		timer.scheduleAtFixedRate(sched, 3000, 3000); // start timer

		// Emulate Player
		createPlayer();
	}

	private void createPlayer() {
		System.out.println(handle + " Start Create Player");
		// TODO Auto-generated method stub
		String input;
		try {
			input = in.readLine();
			System.out.println("recieved: " + input);
			out.println("GAME player");
			input = in.readLine();
			System.out.println("recieved: " + input);
			out.println("GAME " + handle);
			// Handle accepted
			input = in.readLine();
			System.out.println("recieved: " + input);
			// First password
			input = in.readLine();
			System.out.println("recieved: " + input);
			out.println("GAME " +password);
			System.out.println("Sent:" + "GAME " +password);
			input = in.readLine();
			System.out.println("recieved: " + input);
			out.println("GAME " +password);
			System.out.println("Sent:" + "GAME " +password);
			// Password Accepted
			input = in.readLine();
			System.out.println("recieved: " + input);
			// Player name
			input = in.readLine();
			System.out.println("recieved: " + input);
			out.println("GAME " +name);
			// Player Gender
			input = in.readLine();
			System.out.println("recieved: " + input);
			out.println("GAME " +"boy");
			// Finished notification
			input = in.readLine();
			System.out.println("recieved: " + input);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(handle + " Finish Create Player");
	}

	class ScheduleTask extends TimerTask {
		public void run() {
			out.println("P");
		}
	}

}
