package spammer;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class SpammerV3 extends Thread{

	CyclicBarrier gate = new CyclicBarrier(4);

	public SpammerV3(){
		
		new EmuPlayer("batMan", gate).start();
		new EmuPlayer("fatMan", gate).start();
		new EmuPlayer("ratMan", gate).start();
		
		// Gate
		try {
			gate.await();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BrokenBarrierException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new SpammerV3();
	}

}
