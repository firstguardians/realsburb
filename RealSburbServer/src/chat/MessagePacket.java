package chat;

public class MessagePacket {

	private int recievingPid;
	private int recievingSid;
	private String message;
	private String group = "pid"; // Private by default
	private boolean game = false;

	// CHAT Private Message
	public MessagePacket(int pidTST, String mess){
		recievingPid = pidTST;
		message = mess;
	}

	// CHAT Global Message or Session Message
	public MessagePacket(String grouP, int sid, String mess){
		group = grouP;
		message = mess;
		recievingSid = sid;
	}

	// GAME Private Message
	public MessagePacket(boolean gameM,int pidTST, String mess){
		recievingPid = pidTST;
		message = mess;
		game = true;
	}

	// GAME Global Message or Session Message
	public MessagePacket(boolean gameM, String grouP, int sid, String mess){
		group = grouP;
		message = mess;
		recievingSid = sid;
		game = true;
	}

	public String getMessage(){
		if(game == false){
			message = "CHAT DISP " + message;
		}
		// Game messages will have their prefix included
		return message;
	}

	public String getGroup(){
		return group;
	}
	public int getRecievingPID(){
		return recievingPid;
	}
	public int getRecievingSID(){
		return recievingSid;
	}
}
