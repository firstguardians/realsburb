package databaseFunctions;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Scanner;

public final class CreateDatabaseV3 {
	public CreateDatabaseV3(){
		// INITIALIZE DATABASE
		createDatabase();
		storeBasicItems();
		System.out.println("Database Created.");
	}

	public void createDatabase(){
		// create Database
		Connection c = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		System.out.println("Created skaia.db successfully");
		// create Tables
		c = null; // Might not need
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			//System.out.println("Opened database successfully");
			// PLAYERS
			stmt = c.createStatement();
			String sql = "CREATE TABLE PLAYERS " +
					//(PlanetID, SessionID, DreamerID, Handle, Name, Password, Gender, Rung, RungName, Boondollars, Class, Aspect, Location, HouseID, X, Y, Server, Client, Equipped1,Equipped2
					// ,Kind1, Kind2, Kind3, Notoriety, GelViscosity, Mangrit, Magiykks, Exhaustion,GentsManor,YouthVigor)
					"(PlayerID 	INTEGER PRIMARY KEY AUTOINCREMENT," + // PRIMARY KEY AUTOINCREMENT    "Simply don't provide data for the autoincrement column"
					" PlanetID        INT     NOT NULL, " +
					" SessionID       INT	  NOT NULL, " + 
					" DreamerID       INT	  NOT NULL, " +
					" Handle          TEXT    NOT NULL, " + 
					" Name            TEXT    NOT NULL, " + 
					" Password        TEXT    NOT NULL, " + 
					" Gender          TEXT    NOT NULL, " +
					" Rung            INT     NOT NULL, " + 
					" RungName        TEXT    NOT NULL, " +
					" Boondollars     INT     NOT NULL, " + 
					" Class           TEXT    NOT NULL, " + 
					" Aspect          TEXT    NOT NULL, " +
					// Location info
					" LocationName    TEXT    NOT NULL, " + // Name of map
					" MapID		      INT    NOT NULL, " + // Current Location
					" HouseID         INT	 NOT NULL, " + // House MID for prior to joining a session and setting up maps
					" X            	  INT    NOT NULL, " + // Current room x
					" Y            	  INT    NOT NULL, " + // Current room y 
					" Server          INT   NOT NULL, " +
					" Client          INT   NOT NULL, " +
					// Fighting
					" Equipped1      INT   NOT NULL, " + // ItemKey
					" Equipped2      INT   NOT NULL, " +
					" Kind1          TEXT   NOT NULL, " + 
					" Kind2          TEXT   NOT NULL, " +
					" Kind3          TEXT   NOT NULL, " + 
					// Stats
					" Notoriety       TEXT    NOT NULL, " + // Plus for good deeds, Minus for bad
					" GelViscosity	  INT   NOT NULL, " + // HP
					" Mangrit         INT   NOT NULL, " + // Damage
					" Magiykks	      INT   NOT NULL, " + // God tier ability bonus
					" Exhaustion	  INT   NOT NULL, " + // Limits god tier ability usage may also cap YouthVigor // Fall asleep if it reaches 0
					" GentsManor	  INT   NOT NULL, " + // bonus to friendship
					" YouthVigor      INT	NOT NULL)"; // Determines who goes first and dodging traps
			// Clothing (ItemIDs) //TODO need to store that items are equipped and not on the map or in the inventory
			//" Head			  INT   NOT NULL, " +
			//" Face		      INT   NOT NULL, " + 
			//" Torso		      INT   NOT NULL, " + 
			//" Legs			  INT   NOT NULL, " +  
			//" Back            INT   NOT NULL, " + 
			//" Feet		      INT   NOT NULL)";
			// ? CruxiteName
			// ? Prankster's gambit
			// ? Imagination
			stmt.executeUpdate(sql);

			// DREAMERS //TODO unused
			stmt = c.createStatement();
			sql = "CREATE TABLE DREAMERS " +
					"(DreamerID 	INTEGER PRIMARY KEY AUTOINCREMENT," + // PRIMARY KEY AUTOINCREMENT    "Simply don't provide data for the autoincrement column"
					" PlayerID        INT     NOT NULL, " +
					" MapID       	  INT     NOT NULL, " +  // Derse, Prospit or Derspit // Location is used to get a mapID from a name
					" X            	  INT    NOT NULL, " + // Current room x
					" Y            	  INT    NOT NULL, " + // Current room y
					" HP          	  INT		NOT NULL)"; // Dreamer's own HP // Uses waking self's base offense to attack
			stmt.executeUpdate(sql);

			// GLOBAL ITEMS - Definitions of items
			stmt = c.createStatement();
			sql = "CREATE TABLE GLOBALITEMS " +
					"(ItemID 	INTEGER PRIMARY KEY     AUTOINCREMENT," +
					" ItemName   	 TEXT    NOT NULL, " +
					" Pickup    	 TEXT    NOT NULL, " + 
					" UseType   	 TEXT	 NOT NULL, " + // Use x
					" Offense	 	 INT    NOT NULL, " + // Weapon Damage
					" WeaponType 	 TEXT	 NOT NULL," + // Blunt,Gun,Polearm,Staff // How weapon is used
					" WeaponEffect1	 TEXT    NOT NULL, " + // Additional Effect
					" WeaponEffect2	 TEXT    NOT NULL, " + // Additional Effect
					" HP			 TEXT	 NOT NULL," + // Amount of damage before broken
					" Kind1		 	 TEXT	 NOT NULL," +
					" Kind2 		 TEXT    NOT NULL, " +
					" Kind3 		 TEXT	 NOT NULL," +
					" Description    TEXT    NOT NULL)"; 
			stmt.executeUpdate(sql);
			//(ItemID, ItemName, Pickup, UseType, Offense, WeaponType, WeaponEffect1, WeaponEffect2, HP, Kind1, Kind2, Kind3, Description)

			// ITEM INSTANCES - Individual items
			stmt = c.createStatement();
			sql = "CREATE TABLE ITEMINSTANCES " +
					"(Key 	INTEGER PRIMARY KEY     AUTOINCREMENT," +
					" ItemID 	INT		NOT NULL," + // Global Items
					" PlayerID 	INT		NOT NULL," + // 0 if not in an inventory
					// Location if item is on a map
					" MapID 	INT		NOT NULL," +
					" X      	INT     NOT NULL, " + 
					" Y      	INT     NOT NULL," +
					// Holds a modifying value such as a strength modifier or a map location for gotos
					" Modifier  TEXT     NOT NULL," + // none if no modifier // this can hold cruxite name
					" QTY      	INT     NOT NULL)"; 
			stmt.executeUpdate(sql);
			//(Key, ItemID, PlayerID, MapID, X, Y, Modifier, QTY)

			// CONTAINED ITEMS // Unused // Fill on individual Container Creation
			stmt = c.createStatement();
			sql = "CREATE TABLE CONTAINEDITEMS " +
					"(Key 		INTEGER PRIMARY KEY AUTOINCREMENT," +
					" ContainerID 		INT		NOT NULL," +
					" ItemID          INT    NOT NULL)"; 
			stmt.executeUpdate(sql);

			// EQUIPABLE ITEMS // Unused //TODO refine
			stmt = c.createStatement();
			sql = "CREATE TABLE EQUIPABLEITEMS " +
					"(ItemID 		INTEGER 	NOT NULL," +
					" EquipType 	TEXT		NOT NULL," +
					" Damage 		TEXT		NOT NULL," +
					" Defense 		TEXT		NOT NULL," +
					" Speed	        INTEGER         NOT NULL)"; 
			stmt.executeUpdate(sql);

			// CREATED ITEMS // Unused // Stores Which ID to create an instance of
			stmt = c.createStatement();
			sql = "CREATE TABLE CREATEDITEMS " +
					"(ItemID 		INTEGER 	NOT NULL," +
					" CreatedID01 	INTEGER		NOT NULL," +
					" CreatedID02 	INTEGER		NOT NULL," +
					" CreatedID03 	INTEGER		NOT NULL)"; // Might not need 3
			stmt.executeUpdate(sql);

			// SESSIONS
			stmt = c.createStatement();
			sql = "CREATE TABLE ALLSESSIONS " +
					"(SessionID 	INTEGER PRIMARY KEY     AUTOINCREMENT," +
					" SessName           TEXT    NOT NULL, " + 
					" SessPassword       TEXT     NOT NULL, " +
					" ProspitID          INTEGER    NOT NULL, " +
					" DerseID            INTEGER    NOT NULL, " +
					" SkaiaID            INTEGER    NOT NULL)";
			// ? add Jack's NPCID here
			// ? royalty's NPCIDs here
			stmt.executeUpdate(sql);

			// MAP IDS
			stmt = c.createStatement();
			sql = "CREATE TABLE ALLMAPIDS " +
					"(MapID			INTEGER PRIMARY KEY 	AUTOINCREMENT," +
					" SessionID         TEXT	NOT NULL, " +
					" LocationName      TEXT   	NOT NULL," +
					" Width       	INTEGER   	NOT NULL," +
					" Height      	INTEGER   	NOT NULL," +
					" RoomTypes     TEXT    	NOT NULL)"; //Altitude/room type array
			stmt.executeUpdate(sql);

			// PLANETS
			stmt = c.createStatement(); //(MapID,SessionID,PlanetName01,PlanetName02,Consorts,Gate01,Gate02,Gate03,Gate04,Gate05,Gate06,Gate07,Color01,Color02,Color03,Color04,Color05,WaterColor,HouseX,HouseY)
			sql = "CREATE TABLE ALLPLANETS " +
					"(PKID 	INTEGER PRIMARY KEY     AUTOINCREMENT," +
					" MapID      	 INTEGER NOT NULL, " + 
					" SessionID      INTEGER NOT NULL, " + 
					" PlanetName01   TEXT    NOT NULL, " + 
					" PlanetName02   TEXT    NOT NULL, " +
					" DenizenID      INT	 NOT NULL, " +
					" ConsortSpecies TEXT    NOT NULL, " +
					" ConsortNoise   TEXT    NOT NULL, " +
					" Gate01         INT	 NOT NULL, " + // No gates assigned
					" Gate02         INT 	 NOT NULL, " +
					" Gate03         INT	 NOT NULL, " +
					" Gate04         INT	 NOT NULL, " +
					" Gate05         INT	 NOT NULL, " +
					" Gate06         INT	 NOT NULL, " +
					" Gate07         INT	 NOT NULL, " +
					" Colors 	  	 TEXT     NOT NULL)"; // 6 rgb values starting with water r g b r g b 
			stmt.executeUpdate(sql);
			//(MapID, SessionID, PlanetName01, PlanetName02, DenizenID, ConsortSpecies, ConsortNoise, Gate01,Gate02,Gate03,Gate04,Gate05,
			// Gate06, Gate07,Colors,HouseX,HouseY)

			// Booleans, Triggers, and etc
			stmt = c.createStatement();
			sql = "CREATE TABLE TRIGGERS " + // Assign on Player Creation
					"(TID 	INTEGER PRIMARY KEY AUTOINCREMENT," + // PRIMARY KEY AUTOINCREMENT    "Simply don't provide data for the autoincrement column"
					" PlayerID        INT     NOT NULL, " +
					" MeteorTimer     INT	  NOT NULL, " + 
					" EnteredMedium   TEXT    NOT NULL, " + 
					" SpriteReleased  TEXT    NOT NULL, " +
					" ProtoTypeCount  INT     NOT NULL," +
					" IsAwake         TEXT  	NOT NULL, " +
					" CardSet         TEXT  	NOT NULL, " +
					" AlchSet         TEXT  	NOT NULL, " +
					" CruxSet         TEXT  	NOT NULL, " +
					" LatheSet         TEXT  	NOT NULL, " +
					" CruxtruderOpened  TEXT		NOT NULL, " +
					" RunTimer  	    TEXT		NOT NULL, " +
					" AlchimeterUpgradeLv  INT    NOT NULL)";
			//(PlayerID,MeteorTimer,EnteredMedium,SpriteReleased,SpriteLocation,SpriteName,ProtoTypeCount,IsAwake,CardSet,AlchSet,CruxSet,LatheSet,AlchimeterUpgradeLv)
			stmt.executeUpdate(sql);

			// NPCS // Jack Noir to Consorts to monsters // unused
			stmt = c.createStatement();
			sql = "CREATE TABLE NPCS " +
					"(NPCID 	INTEGER PRIMARY KEY AUTOINCREMENT," + // PRIMARY KEY AUTOINCREMENT    "Simply don't provide data for the autoincrement column"
					" Name      		INT     NOT NULL, " +
					" MapID        		INT     NOT NULL, " +
					" X     			INT	  NOT NULL, " + 
					" Y   				INT    NOT NULL, " + 
					" Personality	    TEXT    NOT NULL, " + // determines flavor text
					//TODO needs a hostility setting
					" Mangrit		    INT    NOT NULL, " + // HP
					" Offense      		INT     NOT NULL, " +
					" HeldItem		 	INT     NOT NULL," +
					" AttackEffect      TEXT  	NOT NULL, " +
					" QuestType         TEXT  	NOT NULL, " +
					" QuestQuery        TEXT  	NOT NULL, " +
					" QuestAnswer       TEXT  	NOT NULL)";
			//
			stmt.executeUpdate(sql);

			// NPC FRIENDSHIP //unused
			stmt = c.createStatement();
			sql = "CREATE TABLE NPCFRIENDSHIP " +
					"(Key 	INTEGER PRIMARY KEY AUTOINCREMENT," + // PRIMARY KEY AUTOINCREMENT    "Simply don't provide data for the autoincrement column"
					" NPCID      		INT     NOT NULL, " +
					" PlayerID          INT     NOT NULL, " +
					" Value    			INT	  NOT NULL)";
			//
			stmt.executeUpdate(sql);

			// close
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		System.out.println("Tables created successfully");

	}

	private void storeBasicItems() {
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);
			String csvFile = "basicItems.csv";
			Scanner s = new Scanner(new File(csvFile));
			String header = s.nextLine();
			System.out.println(header);
			while(s.hasNextLine()){
				String line = s.nextLine();
				String[] words = line.split(",");
				String name = words[0];
				String pickup = words[1];
				String useType = words[2];
				int offense = Integer.parseInt(words[3]);
				String weaponType = words[4];
				String weaponEffect1 = words[5];
				String weaponEffect2 = words[6];
				int hp = Integer.parseInt(words[7]);
				String kind1 = words[8];
				String kind2 = words[9];
				String kind3 = words[10];
				String description = words[11];

				// in the while loop write each line
				stmt = c.createStatement();
				String sql = "INSERT INTO GLOBALITEMS (ItemID, "+header+") " +
						"VALUES (null, '"+name+"', '"+pickup+"', '"+useType+"', "+offense+" , '"+weaponType+"', '"+weaponEffect1+"', '"+weaponEffect2+"',"
						+ " "+hp+" , '"+kind1+"', '"+kind2+"', '"+kind3+"', '"+description+"');"; 
				System.out.println(sql);
				stmt.executeUpdate(sql);
			}
			s.close(); // Close scanner
			stmt.close();
			c.commit();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		System.out.println("Item records created successfully");

	}

	public static void main(String[] args){
		new CreateDatabaseV3();
	}

}
