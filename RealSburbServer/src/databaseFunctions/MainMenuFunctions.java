package databaseFunctions;

import generation.ItemReader;
import generation.ModifiedItem;
import generation.PlyrGen;

import java.util.ArrayList;
import java.util.Random;

import maps.DreamingPlanet;
import maps.Dungeon;
import maps.House;
import maps.Map;
import maps.Skaia;
import planetGen.Planet;
import planetGen.PlanetBuilder;
import databaseQueue.DatabaseThread;

public final class MainMenuFunctions{
	DatabaseThread dt;
	ReadAccess da;
	Random rand = new Random();
	ThreadID dv;

	// Item Handling
	//ItemDefinitions def = new ItemDefinitions();
	ItemReader itemIds;

	// Player and planet generation
	PlyrGen plyrGen = new PlyrGen();
	SessionFunctions sf;

	String retrievedPass;
	Boolean loginAccepted = false;

	public MainMenuFunctions(DatabaseThread dth, ThreadID dv, ReadAccess dra){
		da = dra;
		this.dv = dv;
		dt = dth;
		itemIds = new ItemReader(dv,dra);
		sf = new SessionFunctions(dv, da);
	}

	public synchronized void createPlayer(String pName, String handle, String password, String gender){
		// Meteor Timer
		int maxTime = rand.nextInt(600)+300; // 300seconds = 5minutes min - 900seconds = 15minutes max

		// DREAMER
		String dreamer = plyrGen.genDreamer();

		//// CREATE PLAYER - Get PID
		int playerID = addPlayer(handle,pName,password,gender,"none","none",dreamer);

		// Player Triggers
		addTriggers(playerID, maxTime);

		// CREATE HOUSE
		int houseID = createMapID(-1, playerID + "house", 12, 1 ); // House Map Dimensions
		System.out.println("HouseID" + houseID+" created");

		//// GENERATE HOUSE MAP
		House h = new House(itemIds);
		writeMapArrays(houseID, h.returnMap()); // store Types and Items

		// Set HID in player
		setHID(playerID, houseID);

		System.out.println("Player created successfully");
	}

	private void setHID(int pid, int hid) {
		// Set HouseID //TODO test this
		String str = "UPDATE PLAYERS set HouseID = "+hid+", MapID = "+ hid +" where PlayerID="+pid+";";
		dt.addJobToQueue(dv.getTid(), str);
	}

	public synchronized void createPlanet(int sid, int pid){
		// Default Variables
		String planetName01 = "noName";
		String planetName02 = "noName";
		String consort = "skeleton";
		String colors; // planet colors

		// ASPECT
		String hsclass = plyrGen.genClass();
		String aspect = plyrGen.genAspect();
		System.out.print(hsclass +" of " + aspect+" | ");

		// GENERATE MAPIDS - playerID and SessionID are already accessible
		int planetID = createMapID(sid, pid+"planet", 20, 20);
		int lairID = createMapID(sid, pid + "denizen lair", 20, 20);
		//TODO int questbedID = createMapID(sid, pid + "quest bed", width, height);

		//update player class, aspect, and planetID
		String sql = "UPDATE PLAYERS set Class = '"+ hsclass +"' , Aspect = '"+ aspect +"', PlanetID = "+planetID+" WHERE PlayerID = "+pid+" ;"; 
		dt.addJobToQueue(dv.getTid(), sql);

		// GENERATE PLANET
		PlanetBuilder pb = new PlanetBuilder(aspect, itemIds);
		Planet p = pb.getPlanet();

		// PLANET NAME
		planetName01 = p.getName01();
		planetName02 = p.getName02();
		System.out.print("Land of " +planetName01+" and "+planetName02+" | ");

		// CONSORTS
		consort = p.getConsorts();
		System.out.print(consort +'\n');

		// PLANET COLORS
		//TODO just store one string water r g b 1 r g b 2 r g b ...
		colors = p.getColors();

		// INSERT PLANET into ALLPLANETS
		addPlanet(planetID,sid,planetName01,planetName02,consort,colors);

		// ----------------------------------------------------------------------------------------------------

		//// Write MAPS
		// Planet
		writeMapArrays(planetID, (Map)p);
		// Denizen lair
		Dungeon lair = new Dungeon(itemIds); // Generate Map
		writeMapArrays(lairID,lair.getMap());
		// Quest Bed
		// Array of Dungeons

		//// Write Leave Items
		generateMapLinks(p.getxLimit(),p.getyLimit(), sid, pid, planetID, lairID, sf, itemIds);

		//// Modify Existing Maps
		// TODO increase skaia
		// TODO increase dreaming moon towers
		// TODO assign moon tower
		// assign moon tower by setting dreamer's location to either derse or prospit
		// do not need to store derse or prospit dreaming location
	}

	/**
	 * @param planetXLim Planet Map Width
	 * @param planetYLim Planet Map Height
	 * @param sid Session Id
	 * @param pid Player Id
	 * @param planetID Planet Id
	 * @param lairID Denizen Lair Id
	 * @param sf Session Functions - Get attributes concerning session maps
	 * @param itemIDs Utility for retrieving the GlobalID of an item
	 */
	private void generateMapLinks(int planetXLim, int planetYLim, int sid, int pid, int planetID, int lairID,
			SessionFunctions sf, ItemReader itemIDs) {
		//TODO generate a string of numbers for each map to place markers
		ArrayList<ModifiedItem> items = new ArrayList<ModifiedItem>();
		
		// MapIDs
		int prospitID = sf.getProspitID(sid);
		int dersID = sf.getDerseID(sid);
		int skaiaID = sf.getSkiaID(sid);
		
		// Generate Locations
		int houseID = sf.getHouseID(pid);
		int hx = rand.nextInt(planetXLim);
		int hy = rand.nextInt(planetYLim);
		// Transportalizers
		int planetToProsX = rand.nextInt(planetXLim);
		int planetToProsY = rand.nextInt(planetYLim);
		int planetToDersX = rand.nextInt(planetXLim);
		int planetToDersY = rand.nextInt(planetYLim);
		int planetToSkaiaX = rand.nextInt(planetXLim);
		int planetToSkaiaY = rand.nextInt(planetYLim);
		// TODO randomize these later
		int prosToPlanetX = 5;
		int prosToPlanetY = 5;
		int dersToPlanetX = 5;
		int dersToPlanetY = 5;
		int skaiaToPlanetX = 5;
		int skaiaToPlanetY = 5;
		
		//Planet to House
		items.add(new ModifiedItem(itemIDs.getID("house"), planetID,hx,hy,houseID+" 4 0"));
		items.add(new ModifiedItem(itemIDs.getID("planet"), houseID, 4, 0, planetID + " " + hx + " " + hy));
		//-------------
		//Planet to Prospit Transportalizer
		items.add(new ModifiedItem(itemIDs.getID("transportalizer"), planetID, planetToProsX, planetToProsY, prospitID +" "+prosToPlanetX+" "+prosToPlanetY));
		items.add(new ModifiedItem(itemIDs.getID("transportalizer"), prospitID, prosToPlanetX, prosToPlanetY, planetID +" "+planetToProsX+" "+planetToProsY));
		//-------------
		//Planet to Derse Transportalizer
		items.add(new ModifiedItem(itemIDs.getID("transportalizer"),planetID,planetToDersX,planetToDersY,dersID+" "+dersToPlanetX+" "+dersToPlanetY));
		items.add(new ModifiedItem(itemIDs.getID("transportalizer"),dersID,dersToPlanetX,dersToPlanetY,planetID+" "+planetToDersX+" "+planetToDersY));
		//-------------
		//Planet to Skaia Transportalizer
		items.add(new ModifiedItem(itemIDs.getID("transportalizer"),planetID,planetToSkaiaX,planetToSkaiaY,skaiaID+" "+skaiaToPlanetX+" "+skaiaToPlanetY));
		items.add(new ModifiedItem(itemIDs.getID("transportalizer"),skaiaID,skaiaToPlanetX,skaiaToPlanetY,planetID+" "+planetToSkaiaX+" "+planetToSkaiaY));
		//-------------
		//Planet to Denizen Lair opening is always at 10, 10
		int lx = rand.nextInt(planetXLim);
		int ly = rand.nextInt(planetYLim);
		items.add(new ModifiedItem(itemIDs.getID("denizen lair"),planetID,lx,ly,lairID+" 10 10"));
		//Denizen Lair to Planet
		items.add(new ModifiedItem(itemIDs.getID("denizen lair"),lairID,10,10,planetID+" "+lx+" "+ly));
		//-------------
		//Planet to Quest Bed
		//-------------

		//// WRITE LEAVE ITEMS
		writeMapLinks(items);
	}

	private synchronized void writeMapArrays(int MapID, Map m) {
		System.out.println("Started Writing Map Arrays for " + MapID);
		System.out.println("Adding mapID "+MapID + " to Database");
		Map map = m;
		// Assign generated arrays
		ArrayList<ArrayList<ArrayList<Integer>>> tItemIDs = m.getItemIDsArray();
		int xLim = map.getxLimit();
		int yLim = map.getyLimit();

		// Job Variables
		ArrayList<String> sqlStatements = new ArrayList<String>();
		System.out.println(dv.getTid() + ": MID:" + MapID +" xMax: "+xLim + " yMax: "+yLim);
		//// WRITE ITEMS
		//if(tItemIDs != null && tItemIDs.size() !=0){
		for(int y=0; y<yLim;){
			for(int x=0; x<xLim;){
				// For every itemID in y,x insert an entry
				for(int i : tItemIDs.get(y).get(x)){ // for as many times as there are entries
					String sql = "INSERT INTO ITEMINSTANCES (ItemID, PlayerID, MapID, X, Y, Modifier, QTY) " +
							"VALUES ("+i+","+ "0,"+MapID+","+ x +","+y+",'none',1);";
					sqlStatements.add(sql);
				}
				x++;
			}
			y++;
		}

		//		//MODIFIED ITEMS //Not Implemented Yet
		//		ArrayList<ModifiedItem> modifiedItems = m.getLeaveItems();
		//		for(ModifiedItem li : modifiedItems){
		//			String sql = "INSERT INTO ITEMINSTANCES (ItemID, PlayerID, MapID, X, Y, Modifier, QTY) " +
		//					"VALUES ("+li.getItemid()+","+ "0,"+li.getMapid()+","+ li.getX() +","+li.getY()+",'"+li.getModifier()+"',1);";
		//			sqlStatements.add(sql);
		//		}

		//// ROOM TYPE - One line string of numbers
		System.out.println("RoomTypeString: " + m.getRoomTypeString());
		String sql = "UPDATE ALLMAPIDS set RoomTypes = '"+m.getRoomTypeString()+"' where MapID = "+MapID+";";
		sqlStatements.add(sql);

		// add arrayList of commands to a job
		dt.addJobToQueue(dv.getTid(), sqlStatements);
	}

	private void writeMapLinks(ArrayList<ModifiedItem> items){
		ArrayList<String> sqlStatements = new ArrayList<String>();
		// UNPACK AND FORMAT QUERIES
		for(ModifiedItem it : items){
			String sql = "INSERT INTO ITEMINSTANCES (ItemID, PlayerID, MapID, X, Y, Modifier, QTY) " +
					"VALUES ("+it.getItemid()+","+ "0,"+it.getMapid()+","+ it.getX() +","+it.getY()+",'"+it.getModifier()+"',1);";
			sqlStatements.add(sql);
		}
		// WRITE
		dt.addJobToQueue(dv.getTid(), sqlStatements);
	}

	public synchronized void createSession(String sessName, String password){
		int sid = -1;
		// Create Session Entry
		String str = "INSERT INTO ALLSESSIONS (SessionID,SessName,SessPassword,ProspitID,DerseID,SkaiaID) " +
				"VALUES (null,'"+sessName+"','"+password+"',-1,-1,-1);";
		dt.addJobToQueue(dv.getTid(), str, true);
		//TODO get KeyID for session
		sid = dv.getReturnedID();
		System.out.println(dv.getTid() + ": Returned SessionID:" + sid);

		// ---------------------------------------------------
		// TODO GENERATE MAPS

		// SKAIA
		Skaia skaia = new Skaia(itemIds);
		// PROSPIT & DERSE
		DreamingPlanet prospit = new DreamingPlanet(itemIds);
		DreamingPlanet derse = new DreamingPlanet(itemIds);
		// TODO Moons
		// ---------------------------------------------------
		// WRITE MAPS

		// STARTING SKAIA
		int skaiaID = createMapID(sid, "skaia", 10, 10 );
		writeMapArrays(skaiaID, skaia.returnMap());
		// PROSPIT PLANET
		int prospitID = createMapID(sid, "prospit", 10, 10 );
		writeMapArrays(prospitID, prospit.returnMap("prospit"));
		// DERSE PLANET
		int derseID = createMapID(sid, "derse", 10, 10 );
		writeMapArrays(derseID, derse.returnMap("derse"));
		// LINK SESSION MAPS
		setSessionMapIDs(sid, skaiaID, prospitID, derseID);
		// ---------------------------------------------------
		System.out.println("Session created successfully");
	}

	private void setSessionMapIDs(int sessID, int skaiaID, int prosID, int dersID){
		String str = "UPDATE ALLSESSIONS set SkaiaID = "+skaiaID+", ProspitID = " +prosID + ", DerseID ="+dersID+" where SessionID="+sessID+";";
		dt.addJobToQueue(dv.getTid(), str);
	}


	public boolean handleExists(String handleName){
		String retrievedHandle = "none";
		retrievedHandle = da.readString("Handle", "SELECT Handle FROM PLAYERS WHERE Handle = '"+handleName+"';");
		// TODO Check all nulls should probably return none rather than null
		if(retrievedHandle == null || retrievedHandle.equals("")){ // No handle found
			System.out.println("Handle == \"null\"");
			return false;
		}
		else{
			System.out.println("Handle Exists. :\"" + retrievedHandle +"\"");
			return true;
		}
	}

	public boolean sessionExists(String sessName){
		String retrievedSession = da.readString("SessName", "SELECT SessName FROM ALLSESSIONS WHERE SessName = '"+sessName+"';");
		if(retrievedSession.equals("")){
			return false;
		}
		else{
			return true;
		}
	}

	public boolean verifyLogin(String handle, String password){
		String retrievedPass = da.readString("Password", "SELECT Password FROM PLAYERS WHERE Handle = '"+handle+"';");
		System.out.println("retrieved password: " +retrievedPass); // TODO returning nothing
		boolean passwordMatches = false;
		if(!retrievedPass.equals(null) && !retrievedPass.equals("")){ // if something is returned
			if(retrievedPass.equals(password)){
				passwordMatches = true;
			}
			else{
				passwordMatches = false;
			}
		}
		else{ // Nothing is returned
			passwordMatches = false;
		}
		return passwordMatches;
	}

	public int getPid(String handle, String password){
		int pid = da.readInt("PlayerID","SELECT PlayerID FROM PLAYERS WHERE Handle = '"+handle+"' AND Password = '"+password+"';");
		if(pid == -1){
			System.out.println(dv.getTid() + " getPid returned -1.");
		}
		else{
			System.out.println(dv.getTid() + " getPid returned:"+ pid);
		}
		return pid;
	}

	public int getSid(int pid){
		int sid = da.readInt("SessionID", "SELECT SessionID FROM PLAYERS WHERE PlayerID = '"+pid+"';");

		if(sid == -1){
			System.out.println(dv.getTid() + " ERROR: GetSid returned -1.");
		}
		return sid;
	}

	public int verifyJoin(String name, String password) {
		int sid = da.readInt("SessionID", "SELECT SessionID FROM ALLSESSIONS WHERE SessName = '"+name+"' AND SessPassword = '"+password+"';");

		// sid 0 or -1 is not a valid sid
		if(sid == 0 || sid == -1){
			System.out.println("VerifyJoin Session Id Failed:"+sid);
		}
		return sid;
	}

	public void joinSession(int pid,int sid) {
		setSID(pid, sid);
		createPlanet(sid, pid);
	}

	private void setSID(int pid, int sid){
		// Update Player
		String str = "UPDATE PLAYERS set SessionID = "+sid+" where PlayerID="+pid+";";
		dt.addJobToQueue(dv.getTid(), str);

		// Get HID - Read
		int hid = da.readInt("HouseID", "SELECT HouseID FROM PLAYERS WHERE PlayerID = "+pid+";");
		System.out.println(dv.getTid() + ": SetSid returned: Hid: " + hid);

		// Update House SID
		str = "UPDATE ALLMAPIDS set SessionID = "+sid+" where MapID="+hid+";";
		dt.addJobToQueue(dv.getTid(), str);
	}

	public String getPlayerLocation(int pid){
		String location = da.readString("LocationName", "SELECT LocationName FROM PLAYERS WHERE PlayerID = '"+pid+"';");
		// TODO fix null
		if(location.equals(null)){
			System.out.println("Player "+pid+"'s Location is null");
		}
		return location;
	}

	private synchronized int addPlayer(String handle, String pName, String password, String gender, String hsclass, String aspect, String dreamer){
		String s = "INSERT INTO PLAYERS (PlanetID, SessionID, DreamerID, Handle, Name,"
				+ " Password, Gender, Rung, RungName, Boondollars,"
				+ " Class, Aspect, LocationName, MapID, HouseID, X, Y,"
				+ " Server, Client, Equipped1,Equipped2,"
				+ "Kind1, Kind2, Kind3, Notoriety, GelViscosity,"
				+ " Mangrit, Magiykks, Exhaustion,GentsManor,YouthVigor) " +
				"VALUES (-1, -1 ,-1,'"+handle+"','"+pName+"',"
				+ "'"+password+"','"+gender+"',0,'Green CandyHorn',0,"
				//TODO will want to change house to pid+house once players can get to other planets
				//TODO need to set mapID to houseID
				+ "'"+ hsclass+"','"+ aspect+"','house', -1 , -1 , 0, 0,"
				+ " -1,-1,'unset','unset',"
				+ "'unset','unset','unset', 0, 100,"
				+ "0,0,0,0,0);"; 
		// Add insert to Job queue and set return to true
		dt.addJobToQueue(dv.getTid(), s, true);
		// Wait for return and then set it to playerID
		int playerID = dv.getReturnedID();
		System.out.println(dv.getTid() + ": AddPlayer returned: Pid: " + playerID);
		return playerID;
	}

	private synchronized void addTriggers(int playerID, int maxTime){
		String str = "INSERT INTO TRIGGERS (TID,PlayerID,MeteorTimer,EnteredMedium,SpriteReleased,ProtoTypeCount,IsAwake,CardSet,AlchSet,CruxSet,LatheSet,CruxtruderOpened,RunTimer,AlchimeterUpgradeLv) " +
				"VALUES (null,"+playerID+","+maxTime+",'F','F',0,'F','F','F','F','F','F','F',0);"; 
		dt.addJobToQueue(dv.getTid(), str);
	}

	private int createMapID(int sid, String locName, int width, int height){
		System.out.println("Started CreateMapID for " + locName);
		// Add MID to ALLMAPIDS
		String str = "INSERT INTO ALLMAPIDS (MapID,SessionID,LocationName,Width,Height,RoomTypes)" +
				"VALUES (null,"+sid+",'"+ locName +"', "+width+" ,"+height+",'none');";
		dt.addJobToQueue(dv.getTid(), str, true);
		int mid = dv.getReturnedID();
		System.out.println(dv.getTid() + ": CreateMapID returned: Mid: " + mid);
		return mid;
	}

	private void addPlanet(int planetID, int sessionID, String planetName01, String planetName02, String consort, String colors){
		String str = "INSERT INTO ALLPLANETS "
				+ "(PKID, MapID, SessionID, PlanetName01, PlanetName02,"
				+ " DenizenID, ConsortSpecies, ConsortNoise, "
				+ "Gate01,Gate02,Gate03,Gate04,Gate05,Gate06,Gate07,"
				+ "Colors) " +
				"VALUES "
				+ "(null, "+ planetID +", "+sessionID+" ,'"+planetName01+"','"+planetName02+"',"
				+ "-1,'unset','unset',"
				+ "'unset','unset','unset','unset','unset','unset','unset','"
				+ colors+"');";
		dt.addJobToQueue(dv.getTid(), str);
	}
}
