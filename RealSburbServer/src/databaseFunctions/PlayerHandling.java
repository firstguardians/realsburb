package databaseFunctions;

import java.util.ArrayList;

import databaseQueue.DatabaseThread;
//TODO A lot of these values can be written on disconnect and shared peer to peer in pvp
public final class PlayerHandling{
	String databaseLoc = "jdbc:sqlite:TESTSKAIA.db";
	DatabaseThread dt;
	ReadAccess da;
	ThreadID dv;

	public PlayerHandling(DatabaseThread d,ThreadID dv, ReadAccess da){
		this.dv = dv;
		this.da = da;
		dt = d;
	}

	public int getx(int playerID){
		int retX = da.readInt( "X", "SELECT X FROM PLAYERS WHERE PlayerID = "+playerID+";");
		System.out.println("Player X: "+ retX);
		return retX;	
	}	
	public int gety(int playerID){
		int retY = da.readInt("Y", "SELECT Y FROM PLAYERS WHERE PlayerID = "+playerID+";");
		System.out.println("Player Y: "+ retY);
		return retY;
	}

	/**
	 * Gel Viscosity is HP
	 * @param pid
	 * @return
	 */
	public int getGelViscosity(int pid){
		int retGelViscosity = da.readInt("GelViscosity","SELECT GelViscosity FROM PLAYERS WHERE PlayerID = "+pid+";");
		return retGelViscosity;	
	}

	public ArrayList<Integer> getPlayerInventoryIDs(int pid){
		String[] str = {"SELECT ItemID FROM ITEMINSTANCES WHERE PlayerID = '"+pid+"';"};
		ArrayList<Integer> tempArray = da.readIntArray("ItemID", str);
		// Copy array
		ArrayList<Integer> inventoryIds = new ArrayList<Integer>();
		for(int i : tempArray){
			inventoryIds.add(i);
		}
		return inventoryIds;
	}

	public String getPlayerInventoryString(int pid){
		String invString = "";
		// Get IDs
		ArrayList<Integer> ids = getPlayerInventoryIDs(pid);
		// Get Name List for IDs
		String[] string = new String[ids.size()];
		System.out.println("id size: " + ids.size()); //TODO this is zero
		if(ids.size()>0){ // if there is anything in the inventory
			System.out.println("Inventory ID size is greater than zero");
			for(int i = 0; i<ids.size();i++){
				System.out.println("adding itemID:" + ids.get(i));
				string[i] = "SELECT ItemName FROM GLOBALITEMS WHERE ItemID = "+ids.get(i)+";";	
			}
			ArrayList<String> names = new ArrayList<String>();
			names.addAll(da.readStringArray("ItemName", string)); // Add all from the new string
			// Make string out of String List
			for(String s : names){
				invString += s + " ";
			}
		}
		else{ // if no items then return an empty string
			System.out.println("Inventory ID is empty.");
			invString = "";
		}
		System.out.println(dv.getTid() + ": Inventory string: " + invString);
		return invString;
	}

	public int countInventory(int pid){
		int invCount = 0;
		ArrayList<Integer> ids = getPlayerInventoryIDs(pid);
		invCount = ids.size();
		return invCount;
	}

	public void setGelViscosity(int pid, int GelViscosity){
		String sql = "UPDATE PLAYERS set GelViscosity = '"+GelViscosity+"' where PlayerID = "+pid+";";
		dt.addJobToQueue(dv.getTid(), sql);
	}

	public void setX(int x, int pid){
		String sql = "UPDATE PLAYERS set X = '"+x+"' where PlayerID = "+pid+";";
		dt.addJobToQueue(dv.getTid(), sql);
	}
	public void setY(int y, int pid){
		String sql = "UPDATE PLAYERS set Y = '"+y+"' where PlayerID = "+pid+";";
		dt.addJobToQueue(dv.getTid(), sql);
	}

	//TODO verify that this works
	/**
	 * @param mid MapID
	 * @param pid PlayerID
	 * @param x New x
	 * @param y New y
	 */
	public void changePlayerMap(int mid, int pid, int x, int y){ // Map and xy
		//Get name of new location //TODO verify that this is needed
		String mapName = da.readString("LocationName","SELECT LocationName FROM ALLMAPIDS WHERE MapID = "+mid+";");
		//Move to that location
		String sql = ("UPDATE PLAYERS set LocationName = '"+mapName+"', MapID = "+mid+", X = "+x+", Y = "+y+" where PlayerID = "+pid+";");
		dt.addJobToQueue(dv.getTid(), sql);
	}
	
	/**
	 * Does not change x or y coordinates
	 * @param mid MapID
	 * @param pid PlayerID
	 */
	public void changePlayerMap(int mid, int pid){ // Map
		//Get name of new location //TODO verify that this is needed
		String mapName = da.readString("LocationName","SELECT LocationName FROM ALLMAPIDS WHERE MapID = "+mid+";");
		//Move to that location
		String sql = ("UPDATE PLAYERS set LocationName = '"+mapName+"', MapID = "+mid+" where PlayerID = "+pid+";");
		dt.addJobToQueue(dv.getTid(), sql);
	}
	
	//TODO remove
	public void changePlayerLocation(String location, int pid){
		String sql = ("UPDATE PLAYERS set LocationName = '"+location+"' where PlayerID = "+pid+";");
		dt.addJobToQueue(dv.getTid(), sql);
	}

	public String getPlanetName01(int playerID){
		int mapID = getPlanetID(playerID);
		String planetName01 = da.readString("PlanetName01","SELECT PlanetName01 FROM ALLPLANETS WHERE MapID = "+mapID+";");
		return planetName01;	
	}

	public String getPlanetName02(int playerID){
		int mapID = getPlanetID(playerID);
		String planetName02 = da.readString("PlanetName02", "SELECT PlanetName02 FROM ALLPLANETS WHERE MapID = "+mapID+";");
		return planetName02;	
	}

	private int getPlanetID(int playerID) {
		int planetID = da.readInt("PlanetID", "SELECT PlanetID FROM PLAYERS WHERE PlayerID = "+playerID+";");
		if(planetID == -1){
			System.out.println("getPlanetID returned Nothing");
		}
		return planetID;	
	}

	public int getSID(int pid) {
		int sid = da.readInt("SessionID", "SELECT SessionID FROM PLAYERS WHERE PlayerID = "+pid+";");
		System.out.println("Session ID: "+ sid);
		if(sid == -1){
			System.out.println("GetSID returned no session");
		}
		return sid;	
	}
	public int getPID(String handle) {
		int pid = da.readInt("PlayerID", "SELECT PlayerID FROM PLAYERS WHERE Handle = '"+handle+"';");
		return pid;	
	}
	public void setServer(int pid, int serverID) {
		ArrayList<String> sqls = new ArrayList<String>();
		String sql = "UPDATE PLAYERS set Server = "+serverID+" where PlayerID = "+pid+";";
		sqls.add(sql);
		sql = "UPDATE PLAYERS set Client = "+pid+" where PlayerID = "+serverID+";";
		sqls.add(sql);
		dt.addJobToQueue(dv.getTid(), sqls);
	}

	public int getServer(int pid) {
		int serverPID = da.readInt("Server", "SELECT Server FROM PLAYERS WHERE PlayerID = "+pid+";");
		System.out.println("Server ID: "+ serverPID);
		if(serverPID == -1){
			System.out.println("GetServer returned no ID");
		}
		return serverPID;	
	}

	public int getClient(int pid) {
		int clientPID = da.readInt("Client",  "SELECT Client FROM PLAYERS WHERE PlayerID = "+pid+";");
		System.out.println("Client ID: "+ clientPID);
		if(clientPID == -1){
			System.out.println("GetClient returned no ID");
		}
		return clientPID;	
	}

	public String getHandle(int pid) {
		String handle = da.readString("Handle", "SELECT Handle FROM PLAYERS WHERE PlayerID = "+pid+";");

		System.out.println("Handle: "+ handle);

		if(handle.equals("none")){
			System.out.println("GetHandle returned nothing");
		}
		return handle;	
	}

	public int getMid(int pid) {
		return da.readInt("MapID", "SELECT MapID FROM PLAYERS WHERE PlayerID = "+pid+";");
	}
}
