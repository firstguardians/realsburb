package databaseFunctions;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import databaseQueue.DatabaseThread;

public class ReadAccess {
	private ThreadID dv;
	//private Connection c = null;

	public static boolean reading = false;
	
	public ReadAccess(ThreadID d){
		dv = d;
		//c = newConnection();
	}

	private Connection newConnection(){
		// Open a connection to the database
		Connection c = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);
		} catch (ClassNotFoundException e) {
			System.out.println("*** Class not Found - Sqlite JDBC not found ***");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("*** Could not create a connection to the database ***");
			e.printStackTrace();
		}
		//		try {
		//			System.out.println("Connection is closed: " + c.toString() + " " + c.isClosed());
		//		}catch (NullPointerException e1) {
		//			if(c == null){
		//				System.out.println("Null Connection");
		//			}
		//			e1.printStackTrace();
		//		}catch (SQLException e1) {
		//			// TODO Auto-generated catch block
		//			e1.printStackTrace();
		//		}
		//System.out.println("Client connection successful: " + c);
		return c;
	}

	public Connection getDatabaseConnection(){
		//System.out.println("con: " + c);
		//return c;
		return newConnection();

	}

	private void sleep(int miliseconds) {
		try {
			TimeUnit.MILLISECONDS.sleep(miliseconds);
		} catch (InterruptedException e) {
			System.out.println("Sleep failed in Use");
			e.printStackTrace();
		}
	}

	public int readInt(String toget, String sqlStatement){
		boolean databaseLocked = false;
		int retrievedInt = -1;
		while (DatabaseThread.writing) { sleep(100); };
		ReadAccess.reading = true;
		//System.err.println("Requested ReadInt");
		Connection c = getDatabaseConnection();
		do{
			Statement stmt = null;
			try {
				stmt = c.createStatement();
				ResultSet rs = stmt.executeQuery(sqlStatement);
				//System.out.println("Sql: " + sqlStatement);

				while ( rs.next() ) {
					retrievedInt = rs.getInt(toget);
					//TODO UNTODOSystem.out.println("Retrieved: " + retrievedInt);
					//TODO UNTODOSystem.out.println("ReturnedInt: " + retrievedInt);
				}
				// TODO these might need to be commented out
				rs.close();
				stmt.close();
				c.close();
				//TODO UNTODOSystem.out.println("ReadInt finished");

			} catch ( Exception e ) {
				System.err.println( e.getClass().getName() + ": " + e.getMessage() );
				if(e.getMessage().contains("locked")){
					System.out.println(dv.getTid() + " Database Locked on read - Trying again");
					sleep(100);
					databaseLocked = true;
				}
				else{
					System.out.println(dv.getTid() + " Error executing: " + sqlStatement);
					System.out.println(dv.getTid() + " Database error does not contain locked - aborting program");
					System.exit(0);
				}
			}
		} while(databaseLocked);
		//System.err.println("Finished ReadInt");
		ReadAccess.reading = false;
		return retrievedInt;
	}

	public ArrayList<Integer> readIntArray(String toget, String[] sqls){
		boolean databaseLocked = false;
		ArrayList<Integer> retrievedInts = new ArrayList<Integer>();
		while (DatabaseThread.writing) { sleep(100); };
		ReadAccess.reading = true;
		//System.err.println("Requested IntArray");
		Connection c = getDatabaseConnection();

		do{
			//Connection c = null;
			Statement stmt = null;
			try {
				//Class.forName("org.sqlite.JDBC");
				//c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
				//c.setAutoCommit(false);
				ResultSet rs = null;
				int max = sqls.length;

				for(int i = 0; i<max;i++){
					stmt = c.createStatement();
					System.out.println("length: " + max + " index: " + i);
					System.out.println("Executing IntArray Sql: " + sqls[i]);
					rs = stmt.executeQuery(sqls[i]);

					while ( rs.next() ) {
						int inter = rs.getInt(toget);
						retrievedInts.add(inter);
					}
				}

				rs.close();
				stmt.close();
				c.close();

			} catch ( Exception e ) {
				System.err.println( e.getClass().getName() + ": " + e.getMessage() );
				if(e.getMessage().contains("locked")){
					System.out.println(dv.getTid() + " Database Locked on read - Trying again");
					sleep(100);
					databaseLocked = true;
				}
				else{
					System.out.println(dv.getTid() + " Error executing last statement");
					System.out.println(dv.getTid() + " Database error does not contain locked - aborting program");
					System.exit(0);
				}
			}
		} while(databaseLocked);
		//System.err.println("Finished IntArray");
		ReadAccess.reading = false;
		return retrievedInts;
	}


	public String readString(String toget, String sqlStatement){
		boolean databaseLocked = false;
		String retrievedStr = "";
		while (DatabaseThread.writing) { sleep(100); };
		ReadAccess.reading = true;
		//System.err.println("Requested ReadString");
		Connection c = getDatabaseConnection();

		do{
			System.out.println("Started ReadStringFromDatabase DOWHILE");
			//Connection c = null;
			Statement stmt = null;
			try {
				//Class.forName("org.sqlite.JDBC");
				//c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
				//c.setAutoCommit(false);


				stmt = c.createStatement();
				ResultSet rs = stmt.executeQuery(sqlStatement);
				//System.out.println("Sql: " + sqlStatement);

				while ( rs.next() ) {
					retrievedStr = rs.getString(toget);
					//System.out.println("toget: \"" + toget + "\"");
					//System.out.println("ReturnedStr: " + retrievedStr);
				}

				rs.close();
				stmt.close();
				c.close();

			} catch ( Exception e ) {
				System.err.println( e.getClass().getName() + ": " + e.getMessage() );
				if(e.getMessage().contains("locked")){
					System.out.println(dv.getTid() + " Database Locked on read - Trying again" + " Sql: " + sqlStatement);
					sleep(100);
					databaseLocked = true;
				}
				else{
					System.out.println(dv.getTid() + " Error executing: " + sqlStatement);
					System.out.println(dv.getTid() + " Database error does not contain locked - aborting program");
					System.exit(0);
				}
			}
		} while(databaseLocked);
		//System.err.println("Finished ReadString");
		ReadAccess.reading = false;
		return retrievedStr;
	}


	/**
	 * Takes in an array of strings if only one request is needed only one string need be entered in the array
	 * If the array needs many sql statements run it will run as many as there are in the array
	 * @param toget
	 * @param sqlStatements
	 * @return
	 */
	public ArrayList<String> readStringArray(String toget, String[] sqlStatements){
		boolean databaseLocked = false;
		ArrayList<String> retrievedStrings = new ArrayList<String>();
		while (DatabaseThread.writing) { sleep(100); };
		ReadAccess.reading = true;
		//System.err.println("Requested ReadStringArray");
		Connection c = getDatabaseConnection();
		// Verify that the sql statements are valid
		//System.out.println("Starting readStringArray");
		//System.out.println("Lines to run:");
		//for(int i = 0; i< sqlStatements.length; i++){
		//	System.out.println(sqlStatements[i]);
		//}
		//System.out.println("End lines to run");

		do{
			//Connection c = null;
			Statement stmt = null;
			try {
				//Class.forName("org.sqlite.JDBC");
				//c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
				//c.setAutoCommit(false);
				ResultSet rs = null;

				for(int i = 0; i < sqlStatements.length;i++){
					stmt = c.createStatement();
					//System.out.println("length: " + sqlStatements.length + " index: " + i);
					System.out.println("Executing StringArray Sql: " + sqlStatements[i]);
					rs = stmt.executeQuery(sqlStatements[i]);

					while ( rs.next() ) {
						String str = rs.getString(toget);
						System.out.println("Adding: " + str);
						retrievedStrings.add(str);
					}
				}

				rs.close();
				stmt.close();
				c.close();

			} catch ( Exception e ) {
				System.err.println( e.getClass().getName() + ": " + e.getMessage() );
				if(e.getMessage().contains("locked")){
					System.out.println(dv.getTid() + " Database Locked on read - Trying again");
					sleep(100);
					databaseLocked = true;
				}
				else{
					System.out.println(dv.getTid() + " Error executing last sql statement");
					System.out.println(dv.getTid() + " Database error does not contain locked - aborting program");
					//System.exit(0);
				}
			}
		} while(databaseLocked);
		//System.err.println("Finished ReadStringArray");
		ReadAccess.reading = false;
		return retrievedStrings;
	}
}
