package databaseFunctions;
import java.util.ArrayList;

import databaseQueue.DatabaseThread;

public final class RoomFunctions{
	DatabaseThread dt;
	ReadAccess da;
	ThreadID dv;

	public RoomFunctions(DatabaseThread d, ThreadID dv, ReadAccess dra){
		this.dv = dv;
		da = dra;
		dt = d;
	}

	public String getMapTypes(int mid){
		String sql = "SELECT RoomTypes FROM ALLMAPIDS WHERE MapID = '"+mid+"';";
		String types = da.readString("RoomTypes", sql);
		if(types.equals("")){
			System.out.println("No MapTypes returned in getMapTypes");
		}
		return types;
	}
	
	public String getMapName(int mid){
		String sql = "SELECT LocationName FROM ALLMAPIDS WHERE MapID = '"+mid+"';";
		String name = da.readString("LocationName", sql);
		if(name.equals("")){
			System.out.println("No name returned in getMapName");
		}
		return name;
	}

	// TODO this /is/ used. Might not need ItemAccess
	public int getItemID(String focusItem){
		int itemID = da.readInt("ItemID", "SELECT ItemID FROM GLOBALITEMS WHERE ItemName = '"+focusItem+"';" );
		if(itemID < 0){
			System.out.println("ItemID <0 in GetItemID when selecting: " + focusItem);
		}
		return itemID;	
	}

	public String getItemDescription(String focusItem){
		String ret = "";
		ret = da.readString("Description","SELECT Description FROM GLOBALITEMS WHERE ItemName = '"+focusItem+"';");
		if(ret.equals("")){
			System.out.println("GetItemDescription: No description returned for: " + focusItem);
		}
		return ret;	
	}

	/**
	 * Changes the stored location of the item to a new location
	 * @param mid
	 * @param X
	 * @param Y
	 * @param itemID
	 */
	public void moveToRoom(int pid, int mid, int X, int Y, int itemID){
		//TODO remove from inventory is the same as add to room - may want to create a separate method?
		// TODO when decaptcha'ing computer is added back to room but stays in inventory
		// select ONLY ONE item from inventory and add it to the room
		int key =  da.readInt("Key", "SELECT key FROM ITEMINSTANCES WHERE ItemID = "+ itemID +" AND PlayerID = "+pid+" LIMIT 1;");
		String sql = "UPDATE ITEMINSTANCES set MapID = "+ mid +" , X = "+ X +" , Y = "+ Y +", PlayerID = 0 WHERE Key = "+key+" ;"; 
		dt.addJobToQueue(dv.getTid(), sql);
	}

	public void moveToInventory(int mid, int x, int y, int itemID, int playerID){
		//TODO get the key when moving inventory items too
		int key = da.readInt("Key", "SELECT Key FROM ITEMINSTANCES WHERE ItemID = "+itemID+" AND X = "+x+" AND Y = "+y+" AND MapID = "+mid+" AND PlayerID = 0 LIMIT 1;");
		System.out.println("Adding " + itemID + " to playerID " + playerID);
		String sql = "UPDATE ITEMINSTANCES set PlayerID = "+ playerID +" WHERE Key = " + key + ";"; 
		dt.addJobToQueue(dv.getTid(), sql);
	}

	public void addItemToRoom(int mid, int x, int y, int itemID){
		String sql = "INSERT INTO ITEMINSTANCES (ItemID, PlayerID, MapID, X, Y, Modifier, QTY) " +
				"VALUES ("+itemID+","+ "0,"+mid+","+ x +","+y+",'none',1);";
		dt.addJobToQueue(dv.getTid(), sql);
	}

	public void destroyItemInRoom(int mid, int x, int y, int itemID){
		//TODO Remove from Database Entirely - does not work if item is in inventory because this looks for the mapid
		int key = -1;
		// Get the ItemKey
		key = da.readInt("Key","SELECT Key FROM ITEMINSTANCES WHERE ItemID = "+itemID+" AND X = "+x+" AND Y = "+y+" AND MapID = "+mid+" LIMIT 1;");
		System.out.println(dv.getTid() + ": Deleting key: " + key);
		// Delete Entry
		String sql = "DELETE from ITEMINSTANCES where Key="+key+";";
		dt.addJobToQueue(dv.getTid(), sql);
	}
	public void destroyItemInInventory(int pid, int itemID){
		//TODO Remove from Database Entirely - does not work if item is in room because this looks for the pid
		int key = -1;
		// Get the ItemKey
		key = da.readInt("Key","SELECT Key FROM ITEMINSTANCES WHERE ItemID = "+itemID+" AND PlayerID = "+pid+" LIMIT 1;");
		// Delete Entry
		String sql = "DELETE from ITEMINSTANCES where Key="+key+";";
		dt.addJobToQueue(dv.getTid(), sql);
	}

//	public String getPlanetLoc(int pid){
//		String land01 = "";
//		String land02 = "";
//		int planetID= -1;
//
//		planetID = da.readInt("PlanetID", "SELECT PlanetID FROM PLAYERS WHERE PlayerID = "+pid+";");
//
//		if(planetID == -1){
//			System.out.println("No planet ID returned");
//		}
//
//		land01 = da.readString("PlanetName01","SELECT PlanetName01 FROM ALLPLANETS WHERE MapID = "+planetID+";");
//		land02 = da.readString("PlanetName02","SELECT PlanetName02 FROM ALLPLANETS WHERE MapID = "+planetID+";");
//		String landloc = land01 + land02;
//		System.out.println("PlanetLoc = " + landloc);
//		return landloc;	
//	}

	//TODO maybe this should be an itemID?
	public boolean canPickup(String itemName){
		Boolean canPickup = false;
		String yesno = "";
		yesno = da.readString("Pickup", "SELECT Pickup FROM GLOBALITEMS WHERE ItemName = '"+itemName+"';").toLowerCase();
		if(yesno.equals("")){
			System.out.println("*** Error in CanPickup no item: " + itemName + " found.");
		}
		if(yesno.equals("y")){
			canPickup = true;
		}
		return canPickup;
	}

	public int getMapWidth(int mid) {
		int mapWidth = -1;
		mapWidth = da.readInt("Width","SELECT Width FROM ALLMAPIDS WHERE MapID = '"+mid+"';");

		if(mapWidth == -1){
			System.out.println("-1 Width returned in GetMapWidth");
		}
		return mapWidth;
	}

	public int getMapHeight(int mid) {
		int mapHeight = -1;
		mapHeight = da.readInt("Height","SELECT Height FROM ALLMAPIDS WHERE MapID = '"+mid+"';");

		if(mapHeight == -1){
			System.out.println("-1 Height returned in GetMapHeight");
		}
		return mapHeight;
	}

	public String getUseTypeFromName(String itemName){
		int id = getItemID(itemName);
		String useType = "none";
		useType = da.readString("UseType", "SELECT UseType FROM GLOBALITEMS WHERE ItemID = '"+id+"';");

		if(useType.equals("none")){
			System.out.println("No use type for name:"+ itemName +" id:"+id+ " in Get item Use From Name");
		}
		return useType;
	}

	public Boolean checkTFBoolean(String triggerName, int pid){
		String tfString = "none";
		Boolean tfBoolean = false;
		tfString = da.readString(triggerName, "SELECT "+ triggerName +" FROM TRIGGERS WHERE PlayerID = "+pid+";").toLowerCase();
		if(tfString.equals("none")){
			System.out.println("No boolean returned in check TF Boolean");
		}
		if(tfString.equals("t")){
			tfBoolean = true;
		}
		else if(tfString.equals("f")){
			tfBoolean = false;
		}
		else {
			System.out.println("** Error checkTFBoolean returned \"" + tfString +"\" instead of t or f");
		}
		return tfBoolean;
	}

	public void setTFBoolean(int pid, String triggerName, String tf){
		System.out.println("PID"+pid +" Setting TF for " + triggerName + "to " + tf);
		String sql = "UPDATE TRIGGERS set "+triggerName+" = '"+tf+"' where PlayerID = "+pid+";";
		dt.addJobToQueue(dv.getTid(), sql);
	}

	public int getPlanetID(int pid){
		return da.readInt("PlanetID","SELECT PlanetID FROM PLAYERS WHERE PlayerID = "+ pid +";");
	}
	
	/**
	 * Returns 
	 * @param location
	 * @return
	 */
	
	
	public int getMID(String location, int pid, int sid){
		int ret = -1;
		// Hasn't joined a session
		if(sid == -1){ // Can only be the house
			System.out.println("Hasn't Joined a Session - Looking for HouseID");
			// Get HID
			int hid = -1;
			hid = da.readInt("HouseID","SELECT HouseID FROM PLAYERS WHERE PlayerID = "+ pid +";");
			if(hid == -1){
				System.out.println("No HID found in getMID for pid: " + pid);
			}
			ret = hid; // return house mapID
			System.out.println("Returning houseID " + hid);
		}
		// Has joined a session
		else{
			System.out.println("Player is in a Session.");
			if(location.contains("house")){
				location = pid+"house";
			}
			if(location.contains("denizen lair")){
				location = pid+"denizen lair";
			}
			// search for MapID
			int mapid = -1;
			mapid = da.readInt("MapID","SELECT MapID FROM ALLMAPIDS WHERE SessionID = "+sid+" AND LocationName = '"+location+"';");
			ret = mapid;
			System.out.println("Returning mapID: " + mapid);
		}

		if(ret == -1){
			System.out.println("getMID returned -1");
			// SessionID is null
		}
		return ret;
	}

	public int getTime(int pid) {

		int ret = -1;
		ret = da.readInt("MeteorTimer", "SELECT MeteorTimer FROM TRIGGERS WHERE PlayerID = "+pid+";");
		if(ret == -1){
			System.out.println("No Timer returned in GetTime"); // TODO verify that this works
		}
		else{
			System.out.println("Timer retrieved at: " + ret + " for player: " + pid);
		}
		return ret;

	}
	public void setTime(int pid, int time) {
		String sql = "UPDATE TRIGGERS set MeteorTimer = "+time+" where PlayerID = "+pid+";";
		dt.addJobToQueue(dv.getTid(), sql);
	}

	public String getMapType(int MapID){
		String returnedMapType = "";
		returnedMapType = da.readString("RoomTypes","SELECT RoomTypes FROM ALLMAPIDS WHERE MapID = "+MapID+";");
		// return int
		if(returnedMapType == ""){
			System.out.println("No Map Type Found in getMapType for MID:"+MapID);
		}
		return returnedMapType;
	}

	public ArrayList<String> getRoomItemArray(int mapID, int x, int y){
		// Get ID then get Array from IDs
		ArrayList<Integer> roomItemIDs = new ArrayList<Integer>();
		ArrayList<String> roomItemStrings = new ArrayList<String>();
		// Get ItemIDs
		String[] string = {"SELECT ItemID FROM ITEMINSTANCES WHERE MapID = "+mapID+" AND X = "+ x +" AND Y = "+y+" AND PlayerID = 0;"};
		roomItemIDs = da.readIntArray("ItemID",string);
		if(!roomItemIDs.isEmpty()){
			// Get Names
			String[] sqls = new String[roomItemIDs.size()];
			// For as many IDs were retrieved get names create an sql statement
			for(int i=0; i < roomItemIDs.size();i++){
				sqls[i] = "SELECT ItemName FROM GLOBALITEMS WHERE ItemID = "+roomItemIDs.get(i)+";";
			}
			// Transfer strings
			ArrayList<String> tempArray = da.readStringArray("ItemName",sqls);
			for(String s : tempArray){
				roomItemStrings.add(s);
			}
		}
		else{
			System.err.println("*** RoomItemIDs is empty.");
			roomItemStrings.add("None");
		}
		return roomItemStrings;
	}

	public ArrayList<String> getLeaveArray(int MapID, int x, int y){
		ArrayList<String> leaveArray = new ArrayList<String>();
		String[] string = {"SELECT GoToText FROM GOTOTABLE WHERE MapID = "+MapID+" AND X = "+ x +" AND Y = "+y+";"};
		ArrayList<String> tempArray = da.readStringArray("GoToText", string);
		// Transfer strings
		for(String s : tempArray){
			leaveArray.add(s);
		}
		return leaveArray;
	}
	
	public String getColors(int mid){
		String colors = "none";
		colors = da.readString("Colors", "SELECT Colors FROM ALLPLANETS WHERE MapID = "+mid+";");
		return colors;
	}
	
	public String getItemModifier(int itemID, int x, int y, int mid){
		String modifier = da.readString("Modifier", "SELECT Modifier FROM ITEMINSTANCES WHERE ItemID = "+itemID+" AND X = "+x+" AND Y = "+y+" AND MapID = "+mid+" LIMIT 1;");
		return modifier;
	}
}
