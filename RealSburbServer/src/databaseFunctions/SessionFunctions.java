package databaseFunctions;

public class SessionFunctions {
	ThreadID dv;
	ReadAccess ra;
	
	public SessionFunctions(ThreadID dv, ReadAccess ra){
		this.dv = dv;
		this.ra = ra;
	}

	public int getHouseID(int pid) {
		int hid = ra.readInt("HouseID", "SELECT HouseID FROM PLAYERS WHERE PlayerID = "+pid+";");
		System.out.println("SessionFunc hid: " + hid);
		return hid;
	}

	public int getProspitID(int sid) {
		int prosid = ra.readInt("ProspitID","SELECT ProspitID FROM ALLSESSIONS WHERE SessionID = "+sid+";");
		return prosid;
	}

	public int getDerseID(int sid) {
		int dersid = ra.readInt("DerseID","SELECT DerseID FROM ALLSESSIONS WHERE SessionID = "+sid+";");
		return dersid;
	}

	public int getSkiaID(int sid) {
		int skaiaid = ra.readInt("SkaiaID","SELECT SkaiaID FROM ALLSESSIONS WHERE SessionID = "+sid+";");
		return skaiaid;
	}

}
