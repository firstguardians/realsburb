package databaseFunctions;


public class ThreadID {
	private int tid;
	private int returnedID;
	//private Connection c;

	public ThreadID(){
	}

	public int getTid() {
		return tid;
	}
	public void setTid(int tid) {
		this.tid = tid;
		System.out.println("Tid set to " + this.tid);
	}
	public int getReturnedID() {
		return returnedID;
	}
	public void setReturnedID(int returnedID) {
		this.returnedID = returnedID;
	}
}
