package databaseQueue;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import databaseFunctions.ReadAccess;
import server.Receiver.PlayerThread;

public class DatabaseThread {
	static ArrayList<Job> jobsTodo = new ArrayList<Job>();
	static boolean writeToDatabase = true;
	static Connection c = null;
	public static boolean writing = false;

	// HashMap of Threads
	private static HashMap<Integer,PlayerThread> threads = new HashMap<Integer, PlayerThread>();

	public DatabaseThread(){
		// Create a Queue
		WriteQueue queue = new WriteQueue();

		// Open a connection to the database
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);
		} catch (ClassNotFoundException e) {
			System.out.println("*** Class not Found - Sqlite JDBC not found ***");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("*** Could not create a connection to the database ***");
			e.printStackTrace();
		}

		// Start the queue
		queue.start();
	}

	public synchronized void addJobToQueue(int tid, String str){
		Job j = new Job(tid, str);
		waitThread(tid, j);
	}
	public synchronized void addJobToQueue(int tid, String str, boolean ret){
		Job j = new Job(tid, str, ret);
		waitThread(tid, j);
	}
	public synchronized void addJobToQueue(int tid, ArrayList<String> statements){
		Job j = new Job(tid, statements);
		waitThread(tid, j);
	}

	/**
	 * Shared methods of the overloaded addJobToQueue
	 * @param tid
	 * @param j
	 */
	private void waitThread(int tid, Job j){
		jobsTodo.add(j);
		System.out.println("Getting thread " + tid);
		PlayerThread thread = threads.get(tid);
		synchronized(thread){
			try {
				thread.wait();
				System.out.println(tid + " Waiting");
			} catch (InterruptedException e) {
				System.out.println(tid + ": Wait Failed in AddJobToQueue");
				e.printStackTrace();
			}
		}
	}

	private static class WriteQueue extends Thread {
		public void run(){
			System.out.println("Started WriteQueue");
			while(writeToDatabase){
				//System.out.println("Broadcast is empty: " + stringsToBroadcast.isEmpty());
				while(!jobsTodo.isEmpty()){
					System.err.println("Write Access requested");
					// Unpack Job
					Job job = jobsTodo.get(0);
					int tid = job.tid;
					String sql = job.sqlStatement;
					boolean returnID = job.returnGeneratedID;
					int id = -1;
					boolean databaseLocked = false;
					while (ReadAccess.reading) { rest(100); };
					DatabaseThread.writing = true;
					// write to database
					// new statement
					Statement stmt = null;
					// while database is locked
					int timesDoWhile = 0;
					do{
						try {
							// If it is writing a huge map entry
							if(job.writeMap){
								// For every entry in the array
								int arrayLength = job.writeArray.size();
								System.out.println(tid + " executing: an array of length " + arrayLength);
								for(int i=0; i < arrayLength;i++){
									// Get the next string
									sql = job.writeArray.get(i);
									if(sql != null){
										// Create Statement
										stmt = c.createStatement();
										// Do not print for arrays because they are huge
										// Update database
										stmt.executeUpdate(sql);
									}
									else{
										System.out.println("DT: i:" + i +" is null");
									}
								}
							}
							else{ // One write
								// Create Statement
								stmt = c.createStatement();
								System.out.println(tid + " executing: " + sql);
								// Send statement from Job
								stmt.executeUpdate(sql);
							}

							// If a created ID is wanted store it
							if(returnID == true){
								id = stmt.getGeneratedKeys().getInt(1);
								System.out.println(tid + ": Returned id: "+ id);
							}
							// Close job
							stmt.close();
							c.commit();
							//c.close();
							//System.out.println("Connection closed");
							System.out.println("Job Committed: " + job.sqlStatement);
						} catch ( SQLException e ) {
							if(e.getMessage().contains("locked")){
								databaseLocked = true;
								System.err.println(e.getMessage());
							}
							else{
								System.out.println("!!! Error in WriteQueue !!!: " + sql);
								System.out.println("DatabaseState: " + e.getSQLState());
								System.err.println( e.getClass().getName() + ": " + e.getMessage() );
								e.printStackTrace();
								try {
									c.close();
									System.out.println("Sqlite connection closed - Closing Server");
									// TODO Remove this later
									System.exit(0);
								} catch (SQLException e1) {
									System.out.println("Could not close connection in WriteQueue");
									e1.printStackTrace();
								}
							}
						} catch(Exception e){
							System.err.println( e.getClass().getName() + ": " + e.getMessage() );
							e.printStackTrace();
						}
						System.out.println("timesDoWhile: " + timesDoWhile);
						timesDoWhile++;
						if(timesDoWhile > 5){
							System.out.println("Too many attempts. - Closing Server");
							// TODO Remove this later
							System.exit(0);
						}
					}
					while(databaseLocked);
					
					System.out.println("Job completed: " + job.sqlStatement);
					DatabaseThread.writing = false;
					// TODO return values
					if(returnID){
						// Get thread at threadID // Get the variables Object /// Set the variables object
						threads.get(tid).getDatabaseVariables().setReturnedID(id);
					}
					// TODO notify finished

					// Notify the thread
					PlayerThread thread = threads.get(tid);
					synchronized(thread){
						thread.notify();
						System.out.println(tid + " Notified");
					}
					// Remove finished job
					jobsTodo.remove(0);
				}
				rest(100); // Rest when there are no jobs
			}
			System.err.println("Second rest at end of Database thread");
			rest(100);// TODO May not need
		}

		public void rest(int ms){
			try {
				TimeUnit.MILLISECONDS.sleep(ms);
			} catch (InterruptedException e) {
				System.out.println("Sleep failed in Chat thread");
				e.printStackTrace();
			}
		}
	}

	public synchronized void addThread(int threadID, PlayerThread h){
		threads.put(threadID, h);
	}
	public synchronized void removeThread(int threadID){
		//TODO verify that this is removing the thread
		threads.remove(threadID);			
	}
	public synchronized void startThread(int threadID){
		threads.get(threadID).start();
	}
}
