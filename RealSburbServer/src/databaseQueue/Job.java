package databaseQueue;

import java.util.ArrayList;


public class Job {
	
	public int tid;
	public String sqlStatement;
	public boolean returnGeneratedID = false;
	public boolean writeMap = false;
	public ArrayList<String> writeArray;
	
	
	/**
	 * Write with no return
	 * @param tid
	 * @param statement
	 */
	public Job(int tid, String statement){
		sqlStatement = statement;
		this.tid = tid;
		System.out.println("Job Tid is " + tid);
	}
	
	/**
	 * Write with a return
	 * @param tid
	 * @param statement
	 * @param id
	 */
	public Job(int tid, String statement, boolean id){
		sqlStatement = statement;
		this.tid = tid;
		System.out.println("Job Tid is " + tid);
		returnGeneratedID = id;
	}
	
	/**
	 * Write a map
	 * Replaces TLI
	 * @param tid
	 * @param statement
	 * @param map
	 */
	public Job(int tid, ArrayList<String> writeArray){
		this.tid = tid;
		System.out.println("Job Tid is " + tid);
		writeMap = true;
		this.writeArray = writeArray;
		
	}

}
