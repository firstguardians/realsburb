package gamePlay;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import server.InputHandler;
import databaseFunctions.PlayerHandling;
import databaseFunctions.RoomFunctions;


public class RoomHandler {
	InputHandler i;
	RoomFunctions rf;
	PlayerHandling ph;
	Use u;

	// Id Numbers
	// PlayerID, SessionID, MapID
	private int pid, sid, mid;

	//private int inventoryCount = 0;

	// incoming text
	private String input="";

	// Loop
	private Boolean roomLoop = false;
	private Boolean displayFlavorText = true;

	// Room Variables
	private String mapName;
	private int ty;
	private int tx;
	private int width;
	private int height;
	private String focusItem;

	// Map Variables
	private int[][] mapRoomTypes; // Room Altitude/Types as an array
	// Room Variables
	private ArrayList<String> leaveArray = new ArrayList<String>();
	private ArrayList<String> itemsArray = new ArrayList<String>(); // Strings
	private String itemsStr = "";
	private String leaveStr = "";

	// Write
	private PrintWriter out;

	public RoomHandler(PrintWriter o, PlayerHandling pl, RoomFunctions r, Use use, InputHandler ip){
		out = o;
		ph = pl;
		rf = r;
		u = use;
		i = ip;
	}

	/**
	 * Is called by GameThread
	 * Starts the RoomLoop.
	 * Changes the location in the Player Info on the database.
	 * Stores the Room Leave,Items,Types for the stored roomxy
	 * Sends Data to redraw the map on the client.
	 * Tells the Room Loop to display the flavor text.
	 * Starts the Room Loop
	 * @param location
	 */
	public void changeMap(int mid){
		//// Map Generation ////
		// ---------------------
		// Get name of new Map
		System.out.println("Changing to mid " + mid);
		mapName = rf.getMapName(mid);
		// Set new mid
		ph.changePlayerMap(mid, pid);
		// Get Current x and y - Set beforehand
		tx = ph.getx(pid);
		ty = ph.gety(pid);
		// Get Width and Height of new map
		width = rf.getMapWidth(mid);
		height = rf.getMapHeight(mid);

		// Room Types
		String roomTypes = rf.getMapTypes(mid); 	// Get map Room Types
		setRoomTypesArray(roomTypes);				// Generate the Room Type Array for the map

		// Send Map colors if planet
		if(mapName.contains("planet") && !mapName.contains("prospit")&& !mapName.contains("derse")){
			out.println("UPDATE COLORS " + rf.getColors(mid));	
		}

		// Send all Map Types so Map Can be Drawn
		out.println("UPDATE LOCATION " + rf.getMapWidth(mid)+ " "+ rf.getMapHeight(mid)+ " "+ roomTypes);

		loadRoom();

		// Start Map Loop and Display Flavor Text
		if(roomLoop == false){
			displayFlavorText = true;
			roomLoop();
		}
	}

	/**
	 * Requests Items and generates Leave Array
	 * Updates Yellow Square on Client.
	 */
	private void loadRoom(){	
		//// Room Generation ////
		// ---------------------
		// Get Current map coordinates
		mid = rf.getMID(mapName, pid, sid);
		tx = ph.getx(pid);
		ty = ph.gety(pid);
		
		// Generate Room Leave
		generateLeaveArray();
		// Request Items in Room
		System.err.println("load room mid:"+ mid);
		getRoomItemsArray(mid,tx,ty);
		
		// Update Yellow Square on Client Map
		String sendOut = "UPDATE ROOM " + tx + " " + ty;
		out.println(sendOut);
	}
	
//	private void assignRoomVariables(int mapID, String loc, int x, int y, int wid, int hei){
//		// Set Room Variables
//		mapName = loc;
//		tx = x;
//		ty = y;
//		width = wid;
//		height = hei;
//		System.out.println("MapID: "+ mapID + " x: "+x+" y: "+y);
//		// Generate Room Leave
//		generateLeaveArray();
//		// Request Items in Room
//		getRoomItemsArray(mapID,x,y);
//	}

	private void roomLoop() {
		roomLoop = true;
		while(roomLoop){ // While Player is still on the same map
			if(displayFlavorText){
				displayFlavorText();	
			}
			// Clear focus variables
			focusItem = "";

			// Request and Update Room Variables
			loadRoom();

			//Display Items in room
			out.println("ITEMS "+itemsStr);
			System.out.println("ITEMS "+itemsStr);
			//Display Leave options
			out.println("LEAVE " + leaveStr);
			//TODO Check for players on same location and room-xy
			//TODO Notify other players in same room
			//TODO Display players on map

			// Query Options
			mainMenu();
			// Get response
			input = i.waitForInput().toLowerCase(); // trim reply and lower case
			System.out.println("MainMenu Input:"+input);

			//// MAIN MENU CHECK
			if(input.startsWith("goto ")||input.startsWith("g ")){
				// Get Substring
				String locToGo;
				locToGo = input.replaceFirst("\\S+\\s", "");	
				// Check locToGo is available
				if(leaveArray.contains(locToGo)){
					String goHere = "";
					// Change location based on input
					if(locToGo.equals("north")){
						if(ty-1 >= 0){
							ph.setY(ty-1, pid);
						}
						else{
							ph.setY(height-1, pid);
						}
					}
					else if(locToGo.equals("south")){
						if(ty+1<height){
							ph.setY(ty+1, pid);
						}
						else{
							ph.setY(0, pid);
						}
					}
					else if(locToGo.equals("west")){
						if(tx-1 >= 0){
							ph.setX(tx-1, pid);
						}
						else{
							ph.setX(width-1, pid);
						}
					}
					else if(locToGo.equals("east")){
						if(tx+1 < width){
							ph.setX(tx+1, pid);
						}
						else{
							ph.setX(0, pid);
						}
					}
					else if(locToGo.equals("hallway")){
						// Upstairs Hallway
						if(tx == 0 || tx == 1 || tx == 7){
							ph.setX(2, pid);
						}
						// Downstairs Hallway
						if(tx == 4 || tx == 5 || tx == 6 || tx == 11){
							ph.setX(3, pid);	
						}
					}
					else if(locToGo.equals("bedroom")){
						ph.setX(0, pid);
					}
					else if(locToGo.equals("bathroom")){
						ph.setX(1, pid);
					}
					else if(locToGo.equals("upstairs")){
						ph.setX(2, pid);
					}
					else if(locToGo.equals("downstairs")){
						ph.setX(4, pid);
					}
					else if(locToGo.equals("livingroom")){
						ph.setX(4, pid);
					}
					else if(locToGo.equals("kitchen")){
						ph.setX(5, pid);
					}
					else if(locToGo.equals("parents room")){
						ph.setX(6, pid);
					}
					else if(locToGo.equals("attic")){
						ph.setX(7, pid);
					}
					else if(locToGo.equals("roof")){
						ph.setX(8, pid);
					}
					else if(locToGo.equals("front yard")){ //TODO this isn't used
						ph.setX(9, pid);
					}
					else if(locToGo.equals("back yard")){
						ph.setX(10, pid);
					}
					else if(locToGo.equals("garage")){
						ph.setX(11, pid);
					}
					else if(locToGo.equals("inside")){
						if(tx == 9){ // front yard
							ph.setX(4, pid);
						}
						if(tx == 10){ // back
							ph.setX(5, pid);
						}
					}
					// TODO make this the else
					else if(locToGo.equals("house") || locToGo.equals("denizen lair") || locToGo.equals("planet")
							|| locToGo.equals("prospit")|| locToGo.equals("derse") || locToGo.equals("skaia")){
						changeMap(u.getMidFromItem(rf.getItemID(locToGo), tx, ty, mid));
					}
					else{
						//change player loc directly
						//goHere = locToGo;
						System.err.println("Nothing triggering in RoomHandler goto");
					}

					// Restart Location
					System.out.println("Location: "+ mapName);
					System.out.println("Gohere: "+ goHere);
					System.out.println("Room Moved to: "+ph.getx(pid)+" "+ ph.gety(pid));

					if(goHere.equals("")){
						// If no new Location
						System.out.println("Change Room");
						loadRoom();
						System.out.println("Leave to " + locToGo);
					}
					else{
						// If new Location
						System.out.println("Change Location");
						changeMap(rf.getMID(goHere, pid, sid));
						System.out.println("Leave to " + goHere);
					}
					//roomLoop = false;
				}
				else{
					out.println("GAME DISP Location:" + locToGo + " not understood.");
				}

			}
			else if(input.startsWith("use ") || input.startsWith("u ")){
				focusItem = input.replaceFirst("\\S+\\s", "");	
				// Get UseType
				String useType = rf.getUseTypeFromName(focusItem);

				System.out.println("use:"+focusItem);
				u.useThing(focusItem,useType,mid,tx,ty,itemsArray);
			}
			else if(input.startsWith("c ")||input.startsWith("captcha ")){	
				// Assign focus
				focusItem = input.replaceFirst("\\S+\\s", "");	
				//System.out.println("Focus Item: "+focusItem);
				// Captcha
				if(itemsArray.contains(focusItem)){
					if(rf.canPickup(focusItem)){
						int inv = ph.countInventory(pid);
						System.out.println("Inventory Size: " + inv);
						if(inv<10){ //Check that the inventory is not full
							// Get ID of focusItem
							int focusID = rf.getItemID(focusItem);
							// Add item to Inventory
							System.out.println("Add id " + focusID + " to Inventory");
							rf.moveToInventory(mid, tx, ty, focusID, pid);
							//System.out.println("Inventory: "+stringOfItems);
							// Update GUI
							out.println("UPDATE INVENTORY "+ ph.getPlayerInventoryString(pid));
							// getItems
							getRoomItemsArray(mid,tx,ty);
						}
						else{
							out.println("GAME DISP You must Decaptcha an item before you can Captcha another");
						}
					}
					else{
						out.println("GAME DISP You cannot captchalogue "+ focusItem + ".");
					}
				}
			}
			else if(input.startsWith("d ")||input.startsWith("decaptcha ")){	
				// Assign focus
				focusItem = input.replaceFirst("\\S+\\s", "");	
				System.out.println("Focus Item: "+focusItem);

				// check that inventory contains item
				if(ph.getPlayerInventoryString(pid).contains(focusItem)){
					// add item to room
					rf.moveToRoom(pid, mid, tx, ty, rf.getItemID(focusItem));
					// Update GUI
					out.println("UPDATE INVENTORY "+ ph.getPlayerInventoryString(pid));
					// Get Items
					getRoomItemsArray(mid,tx,ty);
				}
				else{
					out.println("GAME DISP " + focusItem + " is not accessable.");
				}
			}
			else if(input.startsWith("inspect ")|| input.startsWith("i ")){
				// Get Focus
				focusItem = input.replaceFirst("\\S+\\s", "");	
				// Check that FocusItem exists
				if(itemsArray.contains(focusItem) || ph.getPlayerInventoryIDs(pid).contains(focusItem)){
					// Get description and Display it
					out.println("GAME DISP "+rf.getItemDescription(focusItem));
				}
				else{
					out.println("GAME DISP "+focusItem+" is not accessable.");
				}
			}
			else if(input.startsWith("actions")|| input.startsWith("a ")){
				// create string of available options
				// Query
				out.println("GAME Q | Strife Specibus[SS] | [S]prite | Cancel[x]");
				input = i.waitForInput().toLowerCase();
				if(input.startsWith("strife specibus") || input.equals("ss")){
					//TODO Strife Specibus
				}
				else if(input.startsWith("sprite ") || input.equals("s")){
					//TODO Sprite
				}
				else if(input.startsWith("Cancel ") || input.equals("x")){
					mainMenu();
				}
			}
			else{
				out.println("GAME DISP Command not understood.");
			}
			try {
				TimeUnit.MILLISECONDS.sleep(100);
			} catch (InterruptedException e) {
				System.out.println("Sleep failed in Room");
				e.printStackTrace();
			}
		}
	}
	private void displayFlavorText() {
		// Display Flavor Text
		if(mapName.contains("house")){
			if(tx == 0){
				out.println("GAME DISP You are in your bedroom.");
			}
			else if(tx == 1){
				out.println("GAME DISP You are in the bathroom.");
			}
			else if(tx == 2){
				out.println("GAME DISP You are in the upstairs hallway.");
			}
			else if(tx == 3){
				out.println("GAME DISP You are in the downstairs hallway.");
			}
			else if(tx == 4){
				out.println("GAME DISP You are in the livingroom.");
			}
			else if(tx == 5){
				out.println("GAME DISP You are in the kitchen.");
			}
			else if(tx == 6){
				out.println("GAME DISP You are in your parents' room.");
			}
			else if(tx == 7){
				out.println("GAME DISP You are in the attic.");
			}
			else if(tx == 8){
				out.println("GAME DISP You are on the roof.");
			}
			else if(tx == 9){
				out.println("GAME DISP You are in the front yard.");
			}
			else if(tx == 10){
				out.println("GAME DISP You are in the back yard.");
			}
			else if(tx == 11){
				out.println("GAME DISP You are in the garage.");
			}
		}
		else if(mapName.contains("planet")){
			out.println("GAME DISP You are on the land of "+ ph.getPlanetName01(pid) +" and " + ph.getPlanetName02(pid) );
		}
		else if(mapName.contains("denizen lair")){
			out.println("GAME DISP You are in a cavern.");
		}
		else{
			out.println("GAME DISP You are in the " + mapName);
		}
		if(!mapName.contains("house")){
			displayFlavorText = false;
		}
	}


	//// INITIATING ROOM
	private void generateLeaveArray(){
		leaveArray.clear(); // reset variable
		leaveStr = "";
		// check location to decide between house and map handling
		if(mapName.contains("house")){ // player house //TODO consort houses will be different
			switch(tx){
			case 0: //Bedroom
				leaveArray.add("hallway");
				leaveArray.add("derse");
				break;
			case 1: //Bathroom
				leaveArray.add("hallway");
				break;
			case 2: //USHall
				leaveArray.add("bedroom");
				leaveArray.add("bathroom");
				leaveArray.add("downstairs");
				leaveArray.add("attic");
				break;
			case 3: //DSHall
				leaveArray.add("livingroom");
				leaveArray.add("kitchen");
				leaveArray.add("parents room");
				leaveArray.add("garage");
				break;
			case 4: //LivingRoom
				leaveArray.add("upstairs");
				leaveArray.add("hallway");
				leaveArray.add("kitchen");
				//leaveArray.add("outside");
				break;
			case 5: //Kitchen
				leaveArray.add("hallway");
				leaveArray.add("livingroom");
				leaveArray.add("back yard");
				break;
			case 6: //ParentsRoom
				leaveArray.add("hallway");
				break;
			case 7: //Attic
				leaveArray.add("hallway");
				leaveArray.add("roof");
				break;
			case 8: //Roof
				leaveArray.add("attic");
				break;
			case 9: //FrontYard
				leaveArray.add("inside");
				break;
			case 10: //BackYard
				leaveArray.add("inside");
				break;
			case 11: //Garage
				leaveArray.add("hallway");
				leaveArray.add("front yard");
				break;
			default:
				System.err.println("RoomHandler. GenerateLeave. For house. Called a room that doesn't exist");
				break;
			}
		}
		// MAPS WITHOUT WRAPPING
		else if(mapName.contains("castle")||mapName.contains("denizen")||mapName.contains("questbed")){
			//TODO check surrounding Room Types for -1 and do not allow wrapping
			generateRoomNSEW(tx,ty); //TODO test this
		}
		// MAPS WITH WRAPPING
		else{
			leaveArray.add("north");
			leaveArray.add("south");
			leaveArray.add("west");
			leaveArray.add("east");
		}

		//TODO check for items in THIS ROOM that add leave options
		if(itemsArray.contains("house")){
			leaveArray.add("house");
		}
		if(itemsArray.contains("planet")){ //TODO add "&& rf.checkTFBoolean("EnteredMedium", pid)"
			//TODO remove doesn't work - Still shows as an item
			itemsArray.remove(itemsArray.indexOf("planet")); // If the leave option is not in the items it cannot be treated like an item
			leaveArray.add("planet");
		}
		if(itemsArray.contains("denizen lair")){
			leaveArray.add("denizen lair");
		}
		// TODO test that this string is being drawn
		// Create a string representation of the array
		leaveStr = arrayToStr(leaveArray);
	}

	private void getRoomItemsArray(int mapID, int x, int y){
		// Reset Variables
		itemsArray.clear();
		itemsStr = "";

		// Get Room Items Array (For Processing Removal and Addition)
		itemsArray = rf.getRoomItemArray(mapID, x, y);

		// Process ItemsArray to get Room Items String (To Display)
		itemsStr = arrayToStr(itemsArray);
	}

	/**
	 * Takes in the Item and Leave Arrays, Capitalizes each word and spits out
	 * a string of all Leaves or Items for displaying.
	 * @param list
	 * @return
	 */
	private String arrayToStr(ArrayList<String> list){    
		StringBuilder builder = new StringBuilder(list.size());
		for(String str: list)
		{
			// Capitalize
			str = str.substring(0, 1).toUpperCase() + str.substring(1);
			if(!str.contains("None")){
				builder.append(str + " | ");
			}
		}

		if(builder.equals("")){
			String empty = "There is nothing here.";
			return empty;
		}
		else{
			return builder.toString();
		}
	}

	private void setRoomTypesArray(String roomTypes){
		roomTypes = roomTypes.trim();
		System.out.println("RoomTypes: " + roomTypes);
		// String to String[]
		String[] split = roomTypes.split(" ");
		System.out.println("RoomTypesSplit: ");
		for(int i = 0; i<split.length;i++){
			System.out.print("\""+ split[i] + "\" ");
		}
		System.out.println();
		// String[] to Int[]
		int[] intArray = new int[width * height];
		for(int i = 0;i< split.length;){
			//System.out.println("parse " + i + " " + split[i]);
			intArray[i] = Integer.parseInt(split[i]);
			i++;
		}
		// Int[] to Int[][]
		int z = 0;
		int[][] finalTypes = new int[width][height];
		for(int y = 0; y < height;){
			for(int x = 0; x <width;){
				finalTypes[x][y] = intArray[z];
				z++;
				x++;
			}
			y++;
		}
		mapRoomTypes = finalTypes;
	}

	private void generateRoomNSEW(int curX, int curY){
		ArrayList<String> leave = new ArrayList<String>();

		//Do not convert this to points
		int north = curY -1;
		int south = curY +1;
		int east = curX + 1;
		int west = curX - 1;

		// nsew
		if(north >= 0){
			if(mapRoomTypes[curX][north] != -1){
				leave.add("north");
			}
		}
		if(south < height){
			if(mapRoomTypes[curX][south] != -1){
				leave.add("south");
			}
		}
		if(east < width){
			if(mapRoomTypes[east][curY] != -1){
				leave.add("east");
			}
		}
		if(west >= 0){
			if(mapRoomTypes[west][curY] != -1){
				leave.add("west");
			}
		}
		if(leave.isEmpty()){
			leave.add("0");
		}
		//Set leave array
		leaveArray = leave;
	}

	private void mainMenu(){
		out.println("GAME Q [G]oto X | [U]se X | [C]aptcha/[D]ecaptcha X | [I]nspect X | [A]ctions");
	}

	public void closeAllRoomLoops(){
		roomLoop = false;
	}

	public int getMid(){return mid;}
	public int getSid(){return sid;}
	public int getPid(){return pid;}
	public void setMid(int midd){mid = midd;}
	public void setSid(int sidd){sid = sidd;}
	public void setPid(int pidd){pid = pidd;}

}
