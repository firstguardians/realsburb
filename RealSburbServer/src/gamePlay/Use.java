package gamePlay;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import chat.MessagePacket;
import server.ChatThread;
import server.InputHandler;
import databaseFunctions.PlayerHandling;
import databaseFunctions.RoomFunctions;

public class Use {
	InputHandler i;
	RoomFunctions rf;
	PlayerHandling p;
	// Write
	PrintWriter out;
	ChatThread ct;

	// initiate timer
	Timer timer = new Timer();
	ScheduleTask sched = new ScheduleTask();

	// Variables
	String useWith = "none";
	int maxTime = 0;
	int pid = -1;

	// Loop Booleans
	boolean running = true;
	boolean computerIsOn;
	boolean modifyingRoom;
	boolean servingClient;

	public Use(PrintWriter o, RoomFunctions trf, PlayerHandling tp, InputHandler ip, ChatThread c){
		rf = trf;
		p = tp;
		out = o;
		i = ip;
		ct = c;
	}

	public void storeUsePID(int uPID){
		// store pid
		pid = uPID;
	}

	public void useThing(String itemName,String useType, int mid, int tx, int ty, ArrayList<String> roomInventory){

		// TOTEM LATHE
		if(useType.equals("totem lathe")){
			// Ask for input card
			out.println("GAME Q What would you like to use with the Totem lathe?");
			// Recieve
			useWith = "none";
			useWith = i.waitForInput().toLowerCase();
			// Check if useWith is in the room
			if(roomInventory.contains(useWith)){
				// check what useWith is
				if(useWith.equals("pre-punched card")){
					// remove pre-punched card
					//TODO check if in room or inventory
					rf.destroyItemInRoom(mid, tx, ty, rf.getItemID("pre-punched card"));
					// create totem
					int totemID = rf.getItemID("totem");
					rf.addItemToRoom(mid, tx, ty, totemID);
					out.println("GAME DISP The machine whirrs to life creating a totem.");
					sleep(10);
				}
				else{
					out.println("GAME DISP "+useWith+" is not compatible with the Totem Lathe.");
				}
			}
			else{
				out.println("GAME DISP "+useWith+" is not accessable.");
			}
		}
		// ALCHEMITER
		else if(useType.equals("alchemiter")){
			// Ask what to use	
			out.println("GAME Q What would you like to use with the Alchemiter?");
			// Recieve
			String useWith = "none";
			useWith = i.waitForInput().toLowerCase();
			// Check if in room
			if(roomInventory.contains(useWith)){
				// Check if Valid Item
				if(useWith.equals("totem")){
					// remove totem
					//TODO check if item is in room or inventory
					rf.destroyItemInRoom(mid, tx, ty, rf.getItemID("totem"));
					// create cruxite artifact
					rf.addItemToRoom(mid, tx, ty, rf.getItemID("cruxite artifact"));
					out.println("GAME DISP The machine whirrs to life creating a cruxite artifact.");
					sleep(10);
				}
				else{
					out.println("GAME DISP "+useWith+" is not compatible with the Alchemiter.");
				}
			}
			else{
				out.println("GAME DISP "+useWith+" is not accessable.");
			}
		}
		// CRUXTRUDER
		else if(useType.equals("cruxtruder")){
			if(rf.checkTFBoolean("CruxtruderOpened", pid)){ // True
				// add Cruxite to room
				rf.addItemToRoom(mid, tx, ty, rf.getItemID("cruxite"));
				out.println("GAME DISP The machine spits out a piece of cruxite.");
				sleep(10);
			}
			else{
				// Open
				rf.setTFBoolean(pid, "CruxtruderOpened", "t");
				// Start Timer
				maxTime = rf.getTime(pid);
				timer.scheduleAtFixedRate(sched, 10, 1000); // start timer
				rf.setTFBoolean(pid,"RunTimer", "t"); // Set Variable to run again if reloaded
				// Create Sprite
				// Create Cruxite
				rf.addItemToRoom(mid, tx, ty, rf.getItemID("cruxite"));
				out.println("GAME DISP The machine spits out a piece of cruxite and an oddly spazzing sphere.");
				sleep(10);
			}
		}
		// PRE PUNCHED CARD
		//else if(useType.equals("pre-punched card")){}
		// COMPUTER
		else if(useType.equals("computer")){
			Boolean computerIsOn = true;
			while(computerIsOn && running){
				out.println("GAME Q Sburb_Client.exe   Sburb_Server.exe   Goggle.com   FireFish.exe   |Close[X]|");
				String input = "none";
				input = i.waitForInput().toLowerCase();
				System.out.println(input);
				//computerIsOn = false; // Kills loop for shutdown
				// CLIENT CONNECTING TO SERVER
				// TODO crashes
				if(input.equals("sburb_client.exe")){
					setupServer(pid, mid, tx, ty, roomInventory);
				}
				// SERVER Controls
				else if(input.equals("sburb_server.exe")){
					serverControls(pid);
				}
				else if(input.equals("close")||input.equals("x")){
					out.println("GAME DISP You turned the computer off.");
					computerIsOn = false;
				}
				else{
					out.println("GAME DISP "+ input +" is not responding.");
				}
				sleep(100); // Sleep between actions and next menu
			}
		}
		// SPRITE
		else if(useType.equals("sprite")){

		}
		// BUFF
		else if(useType.equals("buff")){

		}
		// CONTAINER
		else if(useType.equals("container")){

		}
		// CREATE
		else if(useType.equals("create")){

		}
		// WEARABLE
		//else if(useType.equals("wearable")){}
		// cruxite
		else if(useType.equals("entermedium")){
			// Change this to get the Artifact's name from ALLPLAYERS
			out.println("GAME DISP You break the Cruxite Artifact." + '\n' + "GAME DISP The world spins around you for a moment. Upon recovering you notice that the quality of the light coming in through the windows has changed.");
			// Stop Timer
			timer.cancel();
			// EnteredMedium is true
			rf.setTFBoolean(pid, "EnteredMedium", "t");
			rf.setTFBoolean(pid, "RunTimer", "f");
			System.out.println("RunTimer set to: " + rf.checkTFBoolean("RunTimer", pid));
			System.out.println("EnteredMedium set to: " + rf.checkTFBoolean("EnteredMedium", pid));
			// remove cruxite artifact from room
			// TODO check if in inventory or room
			rf.destroyItemInRoom(mid, tx, ty, rf.getItemID(itemName));
		}
		// SBURBCD
		else if(useType.equals("sburbcd")){
			setupServer(pid, mid, tx, ty, roomInventory);
		}
	}	

	private void serverControls(int pid) {
		// Get pid of Client
		// TODO Crashes here
		int clientID = p.getClient(pid);
		System.out.println("ClientID:" + clientID);

		// Check that a connection exists
		if(clientID != -1){
			// Get House ID of Client
			int cHouseID = rf.getMID("house", clientID, p.getSID(clientID));

			Boolean servingClient = true;
			out.println("GAME DISP Connecting to Client ...");
			sleep(500);
			while(servingClient && running){
				Boolean modifyingRoom = false;
				ArrayList<String> tempItemsArray;
				String room = "none";
				// Query rooms in client's house
				out.println("GAME DISP Choose a room:");
				String roomList = "| Bedroom | Bathroom | Upstairs Hallway | Livingroom | Kitchen | Downstairs Hallway |"
						+ " Parents Room | Garage | Attic | Roof | Front Yard | Back Yard | [x]Cancel";
				out.println("GAME Q " + roomList);

				room = i.waitForInput().toLowerCase();

				if (room.equals("cancel")){
					servingClient = false;
				}
				else{
					if(roomList.toLowerCase().contains(room)){
						modifyingRoom = true;
					}
					else{
						out.println("GAME DISP " + room + " does not exist.");
					}
				}
				while(modifyingRoom && running){
					int currentX = returnX(room);

					// Display items in that room
					tempItemsArray = rf.getRoomItemArray(cHouseID, currentX, 0);
					String itemsString = "none";
					for(int i = 0; i<tempItemsArray.size();i++){
						if(i == 0){
							itemsString = tempItemsArray.get(i);
						}
						else{
							itemsString = itemsString + " | " + tempItemsArray.get(i);
						}
					}
					out.println("GAME DISP " + itemsString);
					// Display server actions
					out.println("GAME Q | [M]ove x | [B]uild | [P]hernalia Registry | [C]hange Room | [X]Close "); // Can move item into sprite
					String choice = "none";
					choice = i.waitForInput().toLowerCase();

					if(choice.contains("m ")||choice.contains("move ")){
						String focusItem = "none";
						if(choice.contains("m ")){
							focusItem = choice.substring(2);
						}
						else if(choice.contains("move ")){
							focusItem = choice.substring(5);
						}
						// Get New Location
						out.println("GAME DISP Where do you want to move " + focusItem + "?"); 
						sleep(400);
						out.println("GAME Q | Bedroom | Bathroom | Upstairs Hallway | Livingroom | Kitchen | Downstairs Hallway |"
								+ " Parents Room | Garage | Attic | Roof | Front Yard | Back Yard |");
						choice = "none";
						choice = i.waitForInput().toLowerCase();	
						int newX = returnX(choice);
						//Move item
						rf.moveToRoom(pid, cHouseID, newX, 0, rf.getItemID(focusItem));
						out.println("GAME DISP "+ focusItem + " successfully moved to "+ choice+"."); 
						if(clientIsInRoom(newX,0,clientID)){
							updateClientItems(clientID, cHouseID, newX, 0);
						}
					}
					else if(choice.equals("b") || choice.equals("build")){
						// Display grist
						// Build functions
					}
					else if(choice.equals("p")||choice.equals("phernalia registry")){
						Boolean card = rf.checkTFBoolean("CardSet", clientID);
						Boolean crux = rf.checkTFBoolean("CruxSet", clientID);
						Boolean alch = rf.checkTFBoolean("AlchSet", clientID);
						Boolean lathe = rf.checkTFBoolean("LatheSet", clientID);
						System.out.println("card: " + card);
						System.out.println("crux: " + crux);
						System.out.println("alch: " + alch);
						System.out.println("lathe: " + lathe);
						// Show list of Buildable items
						out.println("GAME DISP Phernalia Registry:");
						if(!card){
							out.println("GAME DISP [P]re-Punched Card");
						}
						if(!crux){
							out.println("GAME DISP [C]ruxtruder");
						}
						if(!alch){
							out.println("GAME DISP [A]lchimeter");
						}
						if(!lathe){
							out.println("GAME DISP [T]otem Lathe");
						}
						if(card && crux && alch && lathe){
							out.println("GAME DISP No availible phernalia items.");
						}
						else{
							// Ask if building or cancel
							out.println("GAME DISP ------------------------------");
							out.println("GAME Q | [S]et x | [x]Cancel |");
							// Get input
							String input = "none";
							input = i.waitForInput().toLowerCase();
							if(input.contains("s ")||input.contains("set ")){
								String focusItem = "none";
								if(input.contains("s ")){
									focusItem = input.substring(2);
								}
								else if(input.contains("set ")){
									focusItem = input.substring(4);
								}

								if(focusItem.equals("pre-punched card")||focusItem.equals("p")){
									if(!card){
										rf.addItemToRoom(cHouseID, currentX, 0, rf.getItemID("pre-punched card"));
										rf.setTFBoolean(clientID, "CardSet", "t");
										card = true;
										out.println("GAME DISP Pre-Punched Card successfully set.");
									}
									else{
										out.println("GAME DISP Pre-Punched Card already used.");
									}
								}
								else if(focusItem.equals("alchemiter")||focusItem.equals("a")){
									if(!alch){
										rf.addItemToRoom(cHouseID, currentX, 0, rf.getItemID("alchemiter"));
										rf.setTFBoolean(clientID, "AlchSet", "t");
										alch = true;
										out.println("GAME DISP Alchemiter successfully set.");
									}
									else{
										out.println("GAME DISP Alchemiter already used.");
									}
								}
								else if(focusItem.equals("cruxtruder")||focusItem.equals("c")){
									if(!crux){
										rf.addItemToRoom(cHouseID, currentX, 0, rf.getItemID("cruxtruder"));
										rf.setTFBoolean(clientID, "CruxSet", "t");
										crux = true;
										out.println("GAME DISP Cruxtruder successfully set.");
									}
									else{
										out.println("GAME DISP Cruxtruder already used.");
									}
								}
								else if(focusItem.equals("totem lathe")||focusItem.equals("t")){
									if(!lathe){
										rf.addItemToRoom(cHouseID, currentX, 0, rf.getItemID("totem lathe"));
										rf.setTFBoolean(clientID, "LatheSet", "t");
										lathe = true;
										out.println("GAME DISP Totem Lathe successfully set.");
									}
									else{
										out.println("GAME DISP Totem Lathe already used.");
									}
								}
								else{
									out.println("GAME DISP "+focusItem+" is not availible.");
								}
								// Update Client's screen
								if(clientIsInRoom(currentX, 0, clientID)){
									updateClientItems(clientID, cHouseID, currentX, 0);
								}
							}
							else if(input.equals("x")||input.equals("cancel")){

							}
							else{
								out.println("GAME DISP Command not understood.");
							}
						}
					}
					else if(choice.equals("c")||choice.equals("change room")){
						modifyingRoom = false;
					}
					else if(choice.equals("x")||choice.equals("close")){
						modifyingRoom = false;
						servingClient = false;
					}
					else{
						out.println("GAME DISP " + choice + " not understood.");
					}
					sleep(500);
				}
				//modifyingRoom = false; // Kills loop for shutdown
			}
			//servingClient = false; // Kills loop for shutdown
		}
		else{
			out.println("GAME DISP You do not have a Client player."+ '\n' +"GAME DISP Have your Client insert the sburb CD.");
		}
	}

	private int returnX(String room) {
		int ret = 0; // bedroom

		if(room.equals("bedroom")){
			ret = 0;
		}
		else if(room.equals("bathroom")){
			ret = 1;
		}
		else if(room.equals("upstairs hallway")){
			ret = 2;
		}
		else if(room.equals("downstairs hallway")){
			ret = 3;
		}
		else if(room.equals("livingroom")){
			ret = 4;
		}
		else if(room.equals("kitchen")){
			ret = 5;
		}
		else if(room.equals("parents room")){
			ret = 6;
		}
		else if(room.equals("attic")){
			ret = 7;
		}
		else if(room.equals("roof")){
			ret = 8;
		}
		else if(room.equals("front yard")){
			ret = 9;
		}
		else if(room.equals("back yard")){
			ret = 10;
		}
		else if(room.equals("garage")){
			ret = 11;
		}
		return ret;
	}

	private void setupServer(int pid, int mid, int tx, int ty, ArrayList<String> roomInventory){
		// If Inventory or Room contains the CD
		// TODO this if statement returns null when using Sburb_Client.exe
		if(roomInventory.contains("sburbcd")||p.getPlayerInventoryString(pid).contains("sburbcd")){
			// If the room also contains the computer
			if(roomInventory.contains("computer")){
				// Connect to Server Player
				out.println("GAME DISP Connecting to skaianet ... Please Wait ...");
				sleep(1000); // 1 second
				// Check that player is in a session
				int playerSession = p.getSID(pid);
				if(playerSession != -1){
					//// Ask for player handle
					out.println("GAME Q Please enter your server player's handle or |Cancel|:");
					String serverHandle = "none";
					serverHandle = i.waitForInput();
					if(!serverHandle.equals("cancel")){
						int serverPID = p.getPID(serverHandle);
						int serverSession = p.getSID(serverPID);
						int retServID = p.getServer(pid);
						int retClientID = p.getClient(serverPID);
						System.out.println("Server: " + retServID);
						System.out.println("Client: " + retClientID);
						//// Check that client does not already have a server
						if(retServID == -1){
							//// Check that server does not already have a client
							if(retClientID == -1){
								//// Check that player is in same session
								if(serverPID != -1){
									if(playerSession != -1){
										if(playerSession == serverSession) {
											// Assign Server Player
											p.setServer(pid, serverPID);
											out.println("GAME DISP Successfully connected to player " + serverHandle);
											// Delete CD
											//TODO verify that the item is in the room and not in the inventory
											rf.destroyItemInRoom(mid, tx, ty, rf.getItemID("sburbcd"));
										}
										else{
											out.println("GAME DISP Could not connect.");
											sleep(500);
											out.println("GAME DISP Player not found in Session.");
											sleep(500);
											// If assign fails cd pops back out
											out.println("GAME DISP The SburbCD is ejected from the cd drive.");
										}
									}
									if(playerSession == -1){
										out.println("GAME DISP Could not connect.");
										sleep(500);
										out.println("GAME DISP Error404 Session not found.");
									}
									if(serverSession == -1){
										out.println("GAME DISP Could not connect.");
										sleep(500);
										out.println("GAME DISP No session found containing player "+serverHandle+".");
									}
								}
								else{
									out.println("GAME DISP Could not connect.");
									sleep(500);
									out.println("GAME DISP Player "+serverHandle+" does not exist.");
								}
							}
							else{
								out.println("GAME DISP Player "+serverHandle+" already has a Client.");
							}
						}
						else{
							out.println("GAME DISP "+p.getHandle(retServID)+" is already your Server.");
						}
					}
					else{
						out.println("GAME DISP A computer is not accessable. The SburbCD requires a computer to run.");
						sleep(500);
					}
				}
				else{
					out.println("GAME DISP There are no players to connect to. You must be in a Session to play.");
					sleep(500);
				}
			} // Did not want to put a server name in.
		}
		else{
			out.println("GAME DISP SburbCD is not accessable.");
		}
		sleep(500); // sleep before returning to previous screen
	}

	private void sleep(int miliseconds) {
		try {
			TimeUnit.MILLISECONDS.sleep(miliseconds);
		} catch (InterruptedException e) {
			System.out.println("Sleep failed in Use");
			e.printStackTrace();
		}
	}

	class ScheduleTask extends TimerTask {
		public void run() {
			maxTime--; // reduce maxTime every second
			if(pid > -1){
				rf.setTime(pid, maxTime); // update table
				out.println("UPDATE TIMER " + maxTime);// update gui
				System.out.println("PID:" +pid+" Timer:"+maxTime);
			}
			else{
				System.out.println("tpid is -1");
				timer.cancel();
			}
			if(maxTime == 0){
				// Kill Player
				out.println("GAME DISP - BOOM -");
				sleep(10);
				out.println("GAME DISP A meteor crashes into your house ending your game before it has even begun.");
				//out.println("GAMEOVER");
				// Stop Timer
				timer.cancel();
			}
		}
	}

	public void startTimer(int time) {
		if(time > 0){
			maxTime = time;
			timer.scheduleAtFixedRate(sched, 10, 1000); // start timer
		}
	}

	public void stopTimer(){
		timer.cancel();
	}
	public void stopUseLoops(){
		running = false;
	}

	private boolean clientIsInRoom(int serverx, int servery, int clientPid){
		boolean inRoom = false;
		int clientx = p.getx(clientPid);
		int clienty = p.gety(clientPid);

		if(clientx == serverx && clienty == servery){
			inRoom = true;
		}
		return inRoom;
	}
	private void updateClientItems(int cpid, int mapID, int x, int y){
		MessagePacket mp = new MessagePacket(true, cpid,"ITEMS " +arrayToString(rf.getRoomItemArray(mapID, x, y)));
		System.out.println("Sent to RecieveMessage: " + mp.getMessage());
		ct.recieveMessage(mp);

	}

	//TODO unfinished and unused
	// Should set x and y and return a mid
	/**
	 * Sets new x and y and returns the map Id stored in Modifier
	 * @param itemID Global ID
	 * @param tx x that item is on
	 * @param ty y that item is on
	 * @param mid mapID that item is on
	 * @return mapID stored in Modifier
	 */
	public int getMidFromItem(int itemID, int tx, int ty, int mid){
		// get this item's modifier
		String modifier = rf.getItemModifier(itemID, tx, ty, mid);
		System.out.println("Use Modifier: " + modifier); // Modifier format mid x y
		// Split the string
		String[] split = modifier.split(" ");
		// set player x and y (parse the string to ints)
		p.setX(Integer.parseInt(split[1]), pid);
		p.setY(Integer.parseInt(split[2]), pid);
		return Integer.parseInt(split[0]);
	}

	public String arrayToString(ArrayList<String> list){    
		StringBuilder builder = new StringBuilder(list.size());
		for(String str: list)
		{
			// Capitalize
			str = str.substring(0, 1).toUpperCase() + str.substring(1);
			builder.append(str + " | ");
		}

		if(builder.equals("")){
			String empty = "There is nothing here.";
			return empty;
		}
		else{
			return builder.toString();
		}
	}
}
