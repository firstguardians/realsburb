package generation;

import java.util.Random;

import databaseFunctions.ReadAccess;
import databaseFunctions.ThreadID;

public final class ItemReader{
	Random rand = new Random();
	ReadAccess da;
	
	public ItemReader(ThreadID dv, ReadAccess d){
		da = d;
	}

	public int getID(String focusItem){
		//TODO UNTODOSystem.out.println("Started GetItemID");
		int itemID = da.readInt("ItemID", "SELECT ItemID FROM GLOBALITEMS WHERE ItemName = '"+focusItem+"';");
		if(itemID < 0){
			System.out.println("Returned ID# < 0 in GetItemID when selecting: " + focusItem);
		}
		return itemID;	
	}

/*	public ArrayList<Integer> getIDlist(String locType){
		System.out.println("Started GetIDList");
		ArrayList<Integer> IDlist = readIntArray("ItemID", "SELECT ItemID FROM GLOBALITEMS WHERE LocType01 = '"+locType+"' OR LocType02 = '"+locType+"' OR LocType03 = '"+locType+"';");
		return IDlist;
	}*/


}
