package generation;

public class ModifiedItem {
	private int itemid;
	private int mapid;
	private int x;
	private int y;
	private String modifier;
	
	public int getItemid() {
		return itemid;
	}

	public int getMapid() {
		return mapid;
	}
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public String getModifier() {
		return modifier;
	}

	/**
	 * @param itemid GlobalID of item
	 * @param mapid Id of map that this item is on
	 * @param x X coordinate this item is on
	 * @param y Y coordinate this item is on
	 * @param modifier For LeaveItem: mapID(togo) x y
	 */
	public ModifiedItem(int itemid, int mapid,int x,int y, String modifier){
		this.itemid = itemid;
		this.mapid = mapid;
		this.x = x;
		this.y = y;
		this.modifier = modifier;
	}
}
