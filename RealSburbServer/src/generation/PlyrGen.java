package generation;
//import java.awt.Color;
import java.util.Random;
public class PlyrGen{
	Random rand = new Random();

	//// CLASSPECT ////
	String[] hsClassArray = {"Prince", "Mage", "Thief", "Rogue", "Heir", "Maid", "Bard", "Knight", "Witch", "Seer", "Sylph", "Page"};
	String[] fClassArray = {"Mage", "Thief", "Rogue", "Heir", "Maid", "Knight", "Witch", "Seer", "Sylph"};
	String[] mClassArray = {"Prince", "Mage", "Thief", "Rogue", "Heir", "Bard", "Knight", "Seer", "Page"};
	String[] aspectArray = {"Breath", "Heart", "Hope", "Doom", "Time", "Space", "Mind", "Light", "Rage", "Void", "Blood", "Life"};

	public String genClass(){
		String hsclass = "";
		hsclass = hsClassArray[rand.nextInt(12)];
		return hsclass;
	}

	public String genAspect(){
		String aspect = "";
		aspect = aspectArray[rand.nextInt(12)];
		return aspect;
	}
	
	public String genDreamer(){
		if(rand.nextBoolean()){
			return "prospit";
		}
		else{
			return "derse";
		}
	}
}
