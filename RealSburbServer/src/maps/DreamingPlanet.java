package maps;

import generation.ItemReader;

import java.util.ArrayList;
import java.util.Random;

public class DreamingPlanet {

	Map planet = new Map();
	//TODO Map moon = new Map();
	//TODO Map tower = new Map();
	Random rand = new Random();
	ItemReader id;
	//MapItems items;

	public DreamingPlanet(ItemReader id){
		this.id = id;
		//this.items = items;
	}

	public Map returnMap(String whichMap){
		planet.setDimensions(10,10);

		// Derspit
		int shop = 49;
		int castle = 50;
		int road = 51;
		int houses = 52;
		int fountain = 41;
		int chain = 54;

		// Set Types
		if(whichMap.equals("prospit")){
			shop = 37;
			castle = 38;
			road = 39;
			houses = 40;
			chain = 42;
		}
		if(whichMap.equals("derse")){
			shop = 43;
			castle = 44;
			road = 45;
			houses = 46;
			chain = 48;
		}

		// START ITERATING THROUGH THE ROOMS
		for(int y=0;y<10;){
			ArrayList<ArrayList<Integer>> listOfY_items = new ArrayList<ArrayList<Integer>>();
			for(int x=0; x<10;){
				// ITEMS
				ArrayList<Integer> listOfX_items = new ArrayList<Integer>(); // contains IDs
				// generate a random # of items 0-5
				int numItems = rand.nextInt(6);
				for(int i = 0; i <= numItems;){
					//TODO add items
					//listOfX_items.add(id.getID(items.getRandomItem()));
					i++;
				}

				//Num controls where room types are set
				int num = (y*10)+x; // coordinate converted to single number
				if(num == 0){
					// chain
					planet.setRoomType(x, y, chain);
					System.out.print("-");
				}
				else if(num == 3 || num == 4 || num == 8 || num == 9 || num == 10 || num == 11 || num == 20 || num == 21 || num == 23 || num == 24 || 
						num == 28 || num == 29 || num == 33 || num == 34 || num == 38 || num == 39 || num == 60 || num == 61 || num == 70 ||
						num == 71 || num == 73 || num == 74 || num == 78 || num == 79 || num == 83 || num == 84 || num == 88 || num == 89 || 
						num == 90 || num == 91){
					// houses
					planet.setRoomType(x, y, houses);	
					//TODO need to generate house map here and house item for leave
					System.out.print("H");
				}
				else if(num == 26 || num == 36 || num == 40 || num == 41 || num == 53 || num == 54 || num == 58 || num == 59 || num == 76 
						|| num == 86){
					// shops
					planet.setRoomType(x, y, shop);
					//TODO Generate shop map and shop leave item
					System.out.print("S");
				}
				else if(num == 56){
					// castle
					planet.setRoomType(x, y, castle);	
					// TODO generate castle map and put linking item here
					System.out.print("W");
				}
				else if(num == 6){
					// fountain
					planet.setRoomType(x, y, fountain);
					//TODO add fountain items to this room
					System.out.print("T");
				}
				else {
					// road
					planet.setRoomType(x, y, road);
					System.out.print("#");
				}
				listOfY_items.add(listOfX_items);

				// iterate
				x++;
			}
			// add y to main array
			planet.addItemIDs(listOfY_items);
			System.out.print('\n');
			// iterate
			y++;
		}
		return planet;
	}

}
