package maps;

import generation.ItemReader;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;

public class Dungeon {
	private Random rand = new Random();
	private Map m = new Map();
	private int mapW = 20;
	private int mapH = 20;
	private Point startPoint = new Point(10,10); // Start Location Should always be 10,10
	private ArrayList<Point> occupiedRooms;
	ItemReader id;

	public Dungeon(ItemReader id){
		this.id = id;
		//this.items = items;
		generateDungeonMap();
	}

	private void generateDungeonMap(){
		// Set map dimensions
		m.setDimensions(20, 20);

		// generate roomTypes and assign them to map
		generateTunnelSystem();
		generateMap();

	}

	private void generateMap(){
		//// ITEMS
		for(int y=0;y<20;){
			// Init y arrays
			ArrayList<ArrayList<Integer>> xItemsArray = new ArrayList<ArrayList<Integer>>();
			for(int x=0; x<20;){
				ArrayList<Integer> items = new ArrayList<Integer>();
				if(m.getRoomType(x, y) != -1){ // only generate items for accessable rooms
					// TODO Add random items 
					//listOfX_items.add(id.getID(items.getRandomItem()));
				}
				xItemsArray.add(items);
				x++;
			}
			// Add items to map
			m.addItemIDs(xItemsArray);
			// iterate
			y++;
		}
	}

	private void generateTunnelSystem(){
		// Initiate
		occupiedRooms = new ArrayList<Point>();
		// Set all rooms to -1
		//System.out.println("Set all rooms to -1");
		for(int y = 0; y<mapH; y++){
			for(int x = 0; x<mapW; x++){
				m.setRoomType(x, y, -1);
			}
		}

		// create first tunnel
		occupiedRooms.add(startPoint); // Add first Room
		m.setRoomType(startPoint.x, startPoint.y, 0); // Add first Room
		generateTunnel(startPoint,50); //start at center

		// create branches
		int numberBranches = 4;
		int branchLength = 5;
		for(int n = 0; n< numberBranches;){
			// generate number
			Point startingRoom = occupiedRooms.get(rand.nextInt(occupiedRooms.size()));
			// make sure that number will have more than one room
			if(!returnAvailableRooms(startingRoom).isEmpty()){
				// generate tunnel
				generateTunnel(startingRoom, branchLength); // random tunnel off of an existing tunnel
				n++; // iterate
			}
			// Else try again
		}
		//TODO check for longest segment leading to a 2 and assign the denizen chamber to it
		showMap();
	}

	private void generateTunnel(Point firstPoint, int tunnelLength){
		Point lastPoint = firstPoint;
		System.out.println("*** Start Tunnel "+firstPoint.x+ ","+ firstPoint.y+ " ***");

		// for as many times as the tunnel is long
		for(int c = 0; c < tunnelLength;){
			// Generate new point
			Point p;
			if(lastPoint.equals(new Point(10,10))){
				// first point
				p = nextRandomPoint(firstPoint);
			}
			else{
				p = nextRandomPoint(lastPoint);
			}

			//// Add this point
			if(!(p.equals(new Point(-1,-1)))){ // if there are points to go to	
				occupiedRooms.add(p); // add new point to occupied rooms
				m.setRoomType(p.x, p.y, 1); // add new point to final map
				if(c+1 >= tunnelLength){ // if this point is at the end of the tunnel
					System.out.println("!!! End of tunnel (max) "+p.x+ ","+ p.y+" !!!");
					m.setRoomType(p.x, p.y, 2);
				}
			}
			else{ // if possibilites are empty the tunnel is at it's end ( returns -1, -1 )
				System.out.println("!!! End of tunnel (no possible move) "+lastPoint.x+ ","+ lastPoint.y+" !!!");
				m.setRoomType(lastPoint.x, lastPoint.y, 2);
				lastPoint = p;
				break;
			}
			lastPoint = p;
			c++;
		}
	}

	private Point nextRandomPoint(Point p) {
		ArrayList<Point> availableRooms = returnAvailableRooms(p);

		//// Pick a point from possible locations
		Point randPoint;
		if(!availableRooms.isEmpty()){
			randPoint = availableRooms.get(rand.nextInt(availableRooms.size()));
			//System.out.println("Random Point returning: " + randPoint.x + " " + randPoint.y);
		}
		else{
			//return -1,-1 if empty
			randPoint = new Point(-1,-1);
		}
		availableRooms.clear(); // Reset possibleLocs for next point
		return randPoint;
	}

	private ArrayList<Point> returnAvailableRooms(Point p) {
		ArrayList<Point> availableRooms = new ArrayList<Point>();
		Point north = new Point(p.x,p.y-1);
		Point south = new Point(p.x,p.y+1);
		Point east = new Point(p.x+1,p.y);
		Point west = new Point(p.x-1,p.y);

		//// CHECK WHICH ROOMS ARE AVAILABLE
		// North //y is greater than zero // Neighbors less than three
		if(north.y >= 0 && countNeighbors(north) < 3 && !occupiedRooms.contains(north)){
			availableRooms.add(north);
		}
		// Is South Clear?
		if(south.y < mapH && countNeighbors(south) < 3 && !occupiedRooms.contains(south)){
			availableRooms.add(south);
		}
		// Is West Clear? x is greater than 0
		if((west.x >= 0) && countNeighbors(west) < 3 && !occupiedRooms.contains(west)){
			availableRooms.add(west);
		}
		// Is East Clear? x is less than max
		if((east.x < mapW) && countNeighbors(east) < 3 && !occupiedRooms.contains(east)){
			availableRooms.add(east);
		}
		return availableRooms;
	}

	private void showMap(){
		for(int y = 0; y < mapW; y++){
			for(int x = 0; x < mapW; x++){
				//Print row of ten
				if(x==10 && y==10){
					System.out.print("@ ");
				}
				else if(m.getRoomType(x, y) == 2){
					System.out.print("! ");
				}
				else if(m.getRoomType(x, y) == 1){
					System.out.print(". ");
				}
				else{
					System.out.print("# ");
				}
			}
			System.out.print('\n');
		}
	}

	private int countNeighbors(Point p){
		int neighbors = 0;
		if(occupiedRooms.contains(new Point(p.x+1,p.y))){ // East
			neighbors++;
		}
		if(occupiedRooms.contains(new Point(p.x-1,p.y))){ // West
			neighbors++;
		}
		if(occupiedRooms.contains(new Point(p.x,p.y-1))){ // North
			neighbors++;
		}
		if(occupiedRooms.contains(new Point(p.x,p.y+1))){ // South
			neighbors++;
		}
		if(occupiedRooms.contains(new Point(p.x+1,p.y-1))){ // NE
			neighbors++;
		}
		if(occupiedRooms.contains(new Point(p.x-1,p.y-1))){ // NW
			neighbors++;
		}
		if(occupiedRooms.contains(new Point(p.x+1,p.y+1))){ // SE
			neighbors++;
		}
		if(occupiedRooms.contains(new Point(p.x-1,p.y+1))){ // SW
			neighbors++;
		}
		//System.out.println("Neighbors: " + neighbors);
		return neighbors;
	}	
	public Map getMap(){return m;}
}
