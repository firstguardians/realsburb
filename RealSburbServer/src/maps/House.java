package maps;

import generation.ItemReader;

import java.util.ArrayList;
import java.util.Random;

public class House {
	Random rand = new Random();
	Map m = new Map();
	ItemReader id;
	
	public House(ItemReader id){
		this.id = id;
	}

	public Map returnMap(){
		System.out.println("House makemap started");

		// Roomsize is placeholder
		System.out.print("RoomSize: ");
		m.setDimensions(12,1);
		for(int i = 0;i<12;){
			m.setRoomType(i, 0, 1);
			System.out.print(" ("+i+",0) "+ m.getRoomType(i, 0) +" ");
			i++;
		}
		System.out.print('\n');

		for(int x = 0;x < 11;){
			for(int y = 0; y<1;){
				System.out.print(" ("+x+","+y+") "+ m.getRoomType(x, y) +" ");
				y++;
			}
			x++;
		}
		System.out.print('\n');
		// IDlist needs work

		// contains all ys > Containing all xs > containing all items

		//// ITEMS
		//Bedroom
		ArrayList<Integer> bedroomI = new ArrayList<Integer>();
		bedroomI.add(id.getID("bed"));
		bedroomI.add(id.getID("computer"));
		//Bathroom	
		ArrayList<Integer> bathroomI = new ArrayList<Integer>();
		bathroomI.add(id.getID("bed"));
		//USHall
		ArrayList<Integer> ushallI = new ArrayList<Integer>();
		ushallI.add(id.getID("bed"));
		//DSHall		
		ArrayList<Integer> dshallI = new ArrayList<Integer>();
		dshallI.add(id.getID("bed"));
		//Livingroom		
		ArrayList<Integer> livingroomI = new ArrayList<Integer>();
		livingroomI.add(id.getID("bed"));
		//Kitchen		
		ArrayList<Integer> kitchenI = new ArrayList<Integer>();
		kitchenI.add(id.getID("bed"));
		//Parentsroom		
		ArrayList<Integer> parentsI = new ArrayList<Integer>();
		parentsI.add(id.getID("bed"));
		//Attic		
		ArrayList<Integer> atticI = new ArrayList<Integer>();
		atticI.add(id.getID("bed"));
		//Roof		
		ArrayList<Integer> roofI = new ArrayList<Integer>();
		roofI.add(id.getID("bed"));
		//FrontYard		
		ArrayList<Integer> fyardI = new ArrayList<Integer>();
		fyardI.add(id.getID("bed"));
		//BackYard		
		ArrayList<Integer> byardI = new ArrayList<Integer>();
		byardI.add(id.getID("bed"));
		//Garage		
		ArrayList<Integer> garageI = new ArrayList<Integer>();
		garageI.add(id.getID("bed"));

		// Sburbcd
		int room = rand.nextInt(12); //0-11
		if(room == 0){bedroomI.add(id.getID("sburbcd"));}
		else if(room == 1){bathroomI.add(id.getID("sburbcd"));}
		else if(room == 2){ushallI.add(id.getID("sburbcd"));}
		else if(room == 3){dshallI.add(id.getID("sburbcd"));}
		else if(room == 4){livingroomI.add(id.getID("sburbcd"));}
		else if(room == 5){kitchenI.add(id.getID("sburbcd"));}
		else if(room == 6){parentsI.add(id.getID("sburbcd"));}
		else if(room == 7){atticI.add(id.getID("sburbcd"));}
		else if(room == 8){roofI.add(id.getID("sburbcd"));}
		else if(room == 9){fyardI.add(id.getID("sburbcd"));}
		else if(room == 10){byardI.add(id.getID("sburbcd"));}
		else if(room == 11){garageI.add(id.getID("sburbcd"));}

		// Add all Room ItemID arrays to a single Array
		ArrayList<ArrayList<Integer>> items = new ArrayList<ArrayList<Integer>>();
		items.add(bedroomI);
		items.add(bathroomI);
		items.add(ushallI);
		items.add(dshallI);
		items.add(livingroomI);
		items.add(kitchenI);
		items.add(parentsI);
		items.add(atticI);
		items.add(roofI);
		items.add(fyardI);
		items.add(byardI);
		items.add(garageI);
		// Add Room ItemID array to ItemID array
		m.addItemIDs(items);

		System.out.println("HouseItems Work");
		return m;

	}

}
