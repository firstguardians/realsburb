package maps;

import java.util.ArrayList;

public class Map {
	private int[][] roomType;
	private ArrayList<ArrayList<ArrayList<Integer>>> itemIDs;
	//private ArrayList<ModifiedItem> leaveItems = new ArrayList<ModifiedItem>();
	private int xLimit = -1;
	private int yLimit = -1;

	public int[][] getRoomTypeArray() {
		return roomType;
	}
	public String getRoomTypeString() {
			String types = "";
			for(int y =0; y<roomType[0].length;y++){
				for(int x =0; x<roomType.length;x++){
					System.out.println("lengt0: " + roomType[0].length);
					System.out.println("length:" + roomType.length);
					System.out.println("x: " + x+ " y:" + y);
					types = types + " " + roomType[x][y];	
				}
			}
		return types;
	}

	public void setRoomType(int x, int y, int value) {
		roomType[x][y] = value;
	}
	public int getRoomType(int x, int y) {
		return roomType[x][y];
	}

	public ArrayList<ArrayList<ArrayList<Integer>>> getItemIDsArray() {
		return itemIDs;
	}
	public void setItemID(ArrayList<ArrayList<ArrayList<Integer>>> itemIDs) {
		this.itemIDs = itemIDs;
	}
	public void addItemIDs(ArrayList<ArrayList<Integer>> listOfx) {
		itemIDs.add(listOfx);
	}

	public int getxLimit(){return xLimit;}
	public int getyLimit(){return yLimit;}

	public void setDimensions(int w, int h){
		// w and h are total size 20 ... array runs 0 - 19
		xLimit = w;
		yLimit = h;
		System.out.println("Initiating map " + w + " wide and " + h + "high");
		roomType = new int[w][h]; // indexes start at zero but w and h should be dimensions
		itemIDs = new ArrayList<ArrayList<ArrayList<Integer>>>();
		System.out.println("RoomSize Finished");
	}
	
//	public void addLeaveItem(ModifiedItem item){
//		leaveItems.add(item);
//	}
//	
//	public ArrayList<ModifiedItem> getLeaveItems(){
//		ArrayList<ModifiedItem> retList = new ArrayList<ModifiedItem>();
//		for(ModifiedItem li : leaveItems){
//			retList.add(li);
//		}
//		return retList;
//	}



}
