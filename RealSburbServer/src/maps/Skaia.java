package maps;

import generation.ItemReader;

import java.util.ArrayList;
import java.util.Random;

public class Skaia {
	Random rand = new Random();
	Map m = new Map();
	ItemReader id;
	//MapItems items;
	
	public Skaia(ItemReader id){
		this.id = id;
		//this.items = items;
	}
	
	public Map returnMap(){
		m.setDimensions(10,10);
		// Room Types // 12 black // 13 white
		// Squares
		Boolean black = true;
		for(int y=0; y<10;){
			for(int x=0; x<10;){
				if(black == true){
					m.setRoomType(x, y, 12);
					black = false;
					System.out.print("#");
				}
				else{
					m.setRoomType(x, y, 13);
					black = true;
					System.out.print(".");
				}
				x++;
			}
			if(black == true){
				black = false;
			}
			else{
				black = true;
			}
			System.out.print('\n');
			y++;
		}
		// Prospit Base
		m.setRoomType(2, 2, 15);
		// Derse Base
		m.setRoomType(7, 7, 14);
		
		// Items
		for(int y=0; y< 20;){
			// COLUMN Y
			ArrayList<ArrayList<Integer>> listOfY = new ArrayList<ArrayList<Integer>>(); // contains y
			for(int x=0; x<20;){
				// ROW X
				ArrayList<Integer> listOfX = new ArrayList<Integer>(); // contains IDs
				int numItems = rand.nextInt(6);
				for(int i = 0; i <= numItems;){
					//TODO generate and add items
					//listOfX.add(id.getID(items.getRandomItem()));
					i++;
				}
				// ADD ROW X TO COLUMN Y
				listOfY.add(listOfX);
				x++;
			}
			// ADD Y COLUMN CONTAINING X ENTRIES TO ITEMID ARRAY
			m.addItemIDs(listOfY);
			y++;
		}
		System.out.println("Successfully Generated ItemID array");
		return m;
	}
}
