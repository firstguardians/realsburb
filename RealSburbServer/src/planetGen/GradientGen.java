package planetGen;


import java.awt.Color;
import java.util.Random;

public class GradientGen {
	// TODO ALL OF THESE FUNCTIONS SHOULD BE MOVED TO LAND DEFINITON
	private Random rand = new Random();
	private Color[] pColor;
	
	public GradientGen(){
	generateGradient();	
	}
	public Color[] getGradient(){
		return pColor;
	}

	private void generateGradient(){
		//TODO BEST
		System.out.println("----------------------");
		Color[] colors = new Color[6]; // 0-5
		int lowDiff, red5, green5, blue5, red1,green1,blue1,redDiff,greenDiff,blueDiff,redw,greenw,bluew;

		int timesRan = 1;
		do{
			lowDiff = 0;
			// Random Color 5 - Mountain // Palest color
			red5 = rand.nextInt(71); // 0-70
			green5 = rand.nextInt(71);
			blue5 = rand.nextInt(71);
			// Random Color 1 - Shore // Brightest Color
			red1 = rand.nextInt(156)+100; // 100-255
			green1 = rand.nextInt(156)+100;
			blue1 = rand.nextInt(156)+100;

			redDiff = (red1-red5)/5;
			greenDiff = (green1-green5)/5;
			blueDiff = (blue1-blue5)/5;

			System.out.println("Diff: " + redDiff + "," + greenDiff + "," + blueDiff);
			if(redDiff < 20)
				lowDiff++;
			if(greenDiff < 20)
				lowDiff++;
			if(blueDiff < 20)
				lowDiff++;
			System.out.println("Times ran: " + timesRan++);
		} while(lowDiff > 1);
		timesRan = 1;

		// Random Water
		//	TODO need to set a minimum distance from the land color
		// TODO ink or dairy should set the water to black or white paint should reroll for a very bright color
		int blueWater = rand.nextInt();
		if(blueWater < 75){ // blue
			redw = rand.nextInt(113);
			greenw = rand.nextInt(113);
			bluew = rand.nextInt(113)+112;
		}
		else{ // Any color including blue
			redw = rand.nextInt(226);
			greenw = rand.nextInt(226);
			bluew = rand.nextInt(226);
		}

		// Generate 4/5
		int red4 = red5 + redDiff;
		int green4 = green5 + greenDiff;
		int blue4 = blue5 + blueDiff;

		// Generate 3/5 - mid tone
		int red3 = red4 + redDiff;
		int green3 = green4 + greenDiff;
		int blue3 = blue4 + blueDiff;

		// Generate 2/5
		int red2 = red3 + redDiff;
		int green2 = green3 + greenDiff;
		int blue2 = blue3 + blueDiff;

		//TODO bugs
		// Water is too close to the color of the land
		// roll water last // while water rby is ________
		// while distance between 1 and 5 is less than 12 reroll
		// minus 0 // Minimum distance of 10?
		// 1 and 5 generating same number
		// Maybe distance can only be less than 10 in one column
		System.out.println("W: "+redw+","+greenw+","+bluew);
		System.out.println("1: "+red1+","+green1+","+blue1);
		System.out.println("2: "+red2+","+green2+","+blue2);
		System.out.println("3: "+red3+","+green3+","+blue3);
		System.out.println("4: "+red4+","+green4+","+blue4);
		System.out.println("5: "+red5+","+green5+","+blue5);

		colors[0] = (new Color(redw,greenw,bluew));
		colors[1] = (new Color(red1,green1,blue1));
		colors[2] = (new Color(red2,green2,blue2));
		colors[3] = (new Color(red3,green3,blue3));
		colors[4] = (new Color(red4,green4,blue4));
		colors[5] = (new Color(red5,green5,blue5));
		
		pColor = colors;
	}
}
