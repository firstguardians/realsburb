package planetGen;
import java.awt.Color;
import java.util.ArrayList;

import maps.Map;

/**
 * @author Rena
 *
 */
public class Planet extends Map{
	private String name01;
	private String name02;
	private ArrayList<String> itemDefinitions;

	private Color[] colors = new Color[6];
	private String consort = "skeleton";
	
	/**
	 * @param name1
	 * @param name2
	 * @param colors
	 * @param consort
	 */
	public Planet(String name1, String name2, Color[] colors, String consort, ArrayList<String> items){
		name01 = name1;
		name02 = name2;
		itemDefinitions = items;

		for(int i=0; i<colors.length; i++){
			this.colors[i] = colors[i];
		}
		
		this.consort = consort;
	}
	public String getColors(){
		String c;
		c = colors[0].getRed() + " " + colors[0].getGreen() + " " + colors[0].getBlue() + " " +
				colors[1].getRed() + " " + colors[1].getGreen() + " " + colors[1].getBlue() + " " +
				colors[2].getRed() + " " + colors[2].getGreen() + " " + colors[2].getBlue() + " " +
				colors[3].getRed() + " " + colors[3].getGreen() + " " + colors[3].getBlue() + " " +
				colors[4].getRed() + " " + colors[4].getRed() + " " + colors[4].getRed() + " " +
				colors[5].getRed() + " " + colors[5].getRed() + " " + colors[5].getRed();
		return c;
		}

	public String getConsorts(){
		return consort;
	}
	
	public String getName01(){
		return name01;
	}
	
	public String getName02(){
		return name02;
	}
	
	/**
	 * Returns all possible items on this planet.
	 * Used for generating items in each room
	 * @return arraylist<String> of possible items
	 */
	public ArrayList<String> getItems() {
		ArrayList<String> copy = new ArrayList<String>();
		for(String s : itemDefinitions){
			copy.add(s);
		}
		return copy;
	}
	
	public String getItemsString(){
		return itemDefinitions.toString();
	}
}
