package planetGen;

import generation.ItemReader;

import java.util.Random;

public class PlanetBuilder {
	Random rand = new Random();
	Planet planet;
	
	//// CONSORTS
	String[] consortColor = {"Red","green","Green","Black","White","blue","Purple","Brown","Orange","Teal","Cyan",
			"Pink","Grey"}; //24
	// Limit animals to animals with "hands"
	String[] consortAnimal = {"Iguana","Salamander","Crocodile","Turtle","Cat","Dog","Jellyfish","Mouse","Ferret",
			"Robot","Racoon","Squirrel"};

	///// DENIZEN
	String[] denizenName = {"Typheus", "", "Abraxas", "", "Hephaestus", "Echidna", "", "Cetus", "", "Nyx", "", "Hemera"}; // Blood, Rage, Mind, Doom, Heart

	public PlanetBuilder(String aspect, ItemReader itemIDs){
		GradientGen grad = new GradientGen();
		V3ItemGen items = new V3ItemGen(aspect);
		PlanetSubPackage p = items.getPlanetDefinition();
		planet = new Planet(p.getLandName01(),p.getLandName02(),grad.getGradient(),genConsort(),p.getItems());
		// Creates altitudes and stores them to the planet object
		PlanetMapGen mapGen = new PlanetMapGen();
		mapGen.buildMap(planet, itemIDs);
	}
	
	public Planet getPlanet(){
		return planet;
	}
	
	private String genConsort(){
		return consortColor[rand.nextInt(consortColor.length)] +" "+ consortAnimal[rand.nextInt(consortAnimal.length)];
	}
	
	
}
