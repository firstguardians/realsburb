package planetGen;
import generation.ItemReader;

import java.util.ArrayList;
import java.util.Random;

import maps.Map;


public class PlanetMapGen {

	Random rand = new Random();
	Planet p;
	Map denizenLair; // TODO create lair
	int alt = 0;
	ItemReader itemIDs;


	public void buildMap(Planet p, ItemReader itemIDs){
		this.p = p;
		this.itemIDs = itemIDs;

		// set Dimensions
		p.setDimensions(20, 20);

		// Generate Alt/Itms
		generateAltitudes();
		generateItems();
		
		//TODO Denizen Lair
		//TODO Quest Bed
		//TODO Array of Dungeons
	}

	private int generateRoomAltitude(int x, int y) {
		int r = rand.nextInt(3);

		if(r == 0){
			// stays the same	
		}
		else if(r == 1){ // up
			if(alt + 1 < 6){
				alt++;
			}
			else{ // if it can't go up
				if(rand.nextBoolean()){
					alt--; // go down
				}
				// else stay the same
			}
		}
		else if(r == 2){ // down
			if(alt - 1 > -1){
				alt--;
			}
			else{ // if it can't go down
				if(rand.nextBoolean()){
					alt++; // go up
				}
				// else stay the same
			}
		}
		else {
			System.out.println("Room Altitude Error");
		}
		return alt;
	}

	private int meanAlt(int x, int y){
		//System.out.println("x: " + x + " y: "+y);
		int miny;
		int plsy;
		int meanAlt;
		// Above this
		if(y-1 > -1){
			miny = y-1;}
		else{
			miny = 19;	}
		// Below this
		if(y+1 < p.getyLimit()){
			plsy = y+1;}
		else{
			plsy = 0;}
		// Mean altitudes at position
		int added = p.getRoomType(x, miny) + p.getRoomType(x, plsy);
		meanAlt = added/2;
		//System.out.println("MeanAlt:" + meanAlt);

		// get values to the left and right
		return meanAlt;
	}

	private void generateAltitudes(){
		// GENERATE ALTITUDE
		//// for every other column
		for(int y=0; y< p.getyLimit();){
			for(int x=0; x<p.getxLimit();){
				p.setRoomType(x, y, generateRoomAltitude(x,y)); //Random Number
				x++; // 0,2,4,6,8,10,12,14,16,18
			}
			y = y+2;
		}
		//// the rest
		for(int y=1; y< p.getyLimit();){
			for(int x=0; x<p.getxLimit();){
				// average the values on either side
				p.setRoomType(x, y, meanAlt(x,y));
				x++; // 1,3,5,7,9,11,13,15,17,19
			}
			y = y+2;
		}


		// Set altitude


	}

	/**
	 * Should generate all planet items, NPCs, Building Marker Items, and teleporters
	 * Building Items tell leave that it needs to give an option to go there.
	 * These also allow for buildings to be destroyed
	 */
	private void generateItems(){
		// Convert strings to IDs
		ArrayList<String> itemStrings = p.getItems();
		int[] ids = new int[itemStrings.size()];
		int it = 0;
		for(String s : itemStrings){
			ids[it] = itemIDs.getID(s);
			System.out.println("Id:" +ids[it] + " " + s);
			it++;
		}

		// Generate the contents of each room from ids
		for(int y=0; y<20;y++){
			ArrayList<ArrayList<Integer>> arrayOfx = new ArrayList<ArrayList<Integer>>();
			for(int x=0; x<20;x++){
				ArrayList<Integer> xarray = new ArrayList<Integer>();
				// Add a random itemID from the ids for 1-6 times
				for(int i=0; i<rand.nextInt(6)+1;i++){
					xarray.add(ids[rand.nextInt(ids.length)]);
				}
				arrayOfx.add(xarray);
			}	
			p.addItemIDs(arrayOfx);
		}
	}
}
