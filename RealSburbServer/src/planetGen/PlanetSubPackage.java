package planetGen;

import java.util.ArrayList;

public class PlanetSubPackage {
	private String landName01;
	private String landName02;
	private ArrayList<String> items = new ArrayList<String>();
	public String getLandName01() {
		return landName01;
	}
	public void setLandName01(String landName01) {
		this.landName01 = landName01;
	}
	public String getLandName02() {
		return landName02;
	}
	public void setLandName02(String landName02) {
		this.landName02 = landName02;
	}
	public ArrayList<String> getItems() {
		ArrayList<String> copy = new ArrayList<String>();
		for(String s : items){
			copy.add(s);
		}
		return copy;
	}
	public void addItem(String s) {
		if(!items.contains(s)){
			items.add(s);}
	}

	@Override
	public String toString(){
		return landName01 +" "+ landName02 +" "+ items;
	}
}
