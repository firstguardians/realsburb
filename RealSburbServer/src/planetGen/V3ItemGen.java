package planetGen;

import java.util.HashMap;
import java.util.Random;

/**
 * Start with a really basic map generation
 * Leave aspect out of this method for now
 * @author Rena
 *
 */
public class V3ItemGen {
	Random rand = new Random();
	PlanetSubPackage planet = new PlanetSubPackage();

	String[] geoTypes = {"rocky", "forest"};
	HashMap<String, String[]> geoHash = new HashMap<String, String[]>();

	// Wind has none, Jungle has plants, Pinwheels have specific item, Volcanoes is geologic
	String[] landNames = {"wind", "jungle", "pinwheels", "volcanoes", "caves", "treasure", "little Cubes", "Glass", "Pyramids", "Neon", "Krypton", "Junk", "Cliffs", "Anime", "Change",
			"Dragons", "Frost", "repetition"};
	// Store three item arrays for each land name
	HashMap<String, String[]> landHash = new HashMap<String, String[]>();

	//These Strings will be in objects eventually
	// Land Names
	String land1;
	String land2;

	// Land 1	// Land Name Based
	String item1;
	String item2;
	String item3;

	// Land 2	// Land Name Based
	String item4;
	String item5;
	String item6;

	// Items relating to geography type (rocky/volcanic/jungle plants)
	String geo1;
	String geo2;
	String geo3;

	/**
	 * Returns a planet object
	 * @param aspect
	 */
	public V3ItemGen(String aspect){
		// init hashmap
		initHash();
		// Generate items
		pickItems();
		System.out.println("The Land of " + land1 + " and " + land2);
		//System.out.println("Items 1:" + flora1 +", " + flora2 +", " + flora3);
		System.out.println("Items 1: " + item1 +", " + item2 +", " + item3);
		System.out.println("Items 2: " + item4 +", " + item5 +", " + item6);
		System.out.println("Geo items: " + geo1 +", " + geo2 +", " + geo3);	
		System.out.println(planet.toString());
	}
	
	public PlanetSubPackage getPlanetDefinition(){
		return planet;
	}

	private void pickItems(){
		//TODO
		// Pick two land names
		land1 = landNames[rand.nextInt(landNames.length)].toLowerCase();
		do{
			land2 = landNames[rand.nextInt(landNames.length)].toLowerCase();
		} while(land1.equals(land2));

		// Get list from hash
		String[] landItems1 = landHash.get(land1);
		String[] landItems2 = landHash.get(land2);
		String randName = "";
		try{
		if(landItems1 == null || landItems1.length == 0){
			do{
				randName = landNames[rand.nextInt(landNames.length)].toLowerCase();
				landItems1 = landHash.get(randName);
			} while(landItems1.length == 0 || landItems1 == null);}
		if(landItems2 == null || landItems2.length == 0){
			do{
				randName = landNames[rand.nextInt(landNames.length)].toLowerCase();
				landItems2 = landHash.get(randName);
			} while(landItems2.length == 0 || landItems2 == null);
		}}
		catch(NullPointerException e){
			System.err.println("Cannot find land name: " + randName);
			//e.printStackTrace();
		}
		
		planet.setLandName01(land1);
		planet.setLandName02(land2);

		// Pick three items from each
		item1 = landItems1[rand.nextInt(landItems1.length)];
		item2 = landItems1[rand.nextInt(landItems1.length)];
		item3 = landItems1[rand.nextInt(landItems1.length)];
		item4 = landItems2[rand.nextInt(landItems2.length)];
		item5 = landItems2[rand.nextInt(landItems2.length)];
		item6 = landItems2[rand.nextInt(landItems2.length)];

		// Pick a geographic type (volcano, jungle, rocky)
		String geoType = geoTypes[rand.nextInt(geoTypes.length)];
		// Pick three items from geo type
		String[] geoTemp = geoHash.get(geoType);
		geo1 = geoTemp[rand.nextInt(geoTemp.length)];
		geo2 = geoTemp[rand.nextInt(geoTemp.length)];
		geo3 = geoTemp[rand.nextInt(geoTemp.length)];
		
		// Stupidly long add stuff
		planet.addItem(item1);
		planet.addItem(item2);
		planet.addItem(item3);
		planet.addItem(item4);
		planet.addItem(item5);
		planet.addItem(item6);
		planet.addItem(geo1);
		planet.addItem(geo2);
		planet.addItem(geo3);
	}

	private void initHash(){
		//// Items for each geographic type
		String[] rocky = {"boulder","rock","sandstone"};
		String[] forest = {"tree","grass","stick","tree stump"};

		geoHash.put("rocky", rocky);
		geoHash.put("forest", forest);
		// ------------------------------------------------------------------------------------ //
		//// Items for each land name
		String[] wind = {};
		String[] jungle = {"fern","vine","tree"};
		String[] pinwheels = {"pinwheel"};
		String[] volcanoes = {"basalt","granite","diamond"};
		String[] caves = {"rock", "bone", "bat", "bear"};
		String[] treasure = {"coin", "ruby", "sapphire", "opal", "amethyst", "tiara", "crown", "sword"};
		String[] littlecubes = {"cube"};
		String[] glass = {"broken glass","mirror","glass"};
		String[] pyramids = {};
		String[] neon = {"neon light"};
		String[] krypton = {"krypton"};
		String[] junk = {"garbage","paper","newspaper"};
		String[] cliffs = {};
		String[] anime = {"manga","sword","notebook"};
		String[] change = {};
		String[] dragons = {"dragon"};
		String[] frost = {"ice","icicle","snow"};
		String[] repetition = {};
		
		landHash.put("wind", wind);
		landHash.put("jungle", jungle);
		landHash.put("pinwheels", pinwheels);
		landHash.put("volcanoes", volcanoes);
		landHash.put("caves", caves);
		landHash.put("treasure", treasure);
		landHash.put("little cubes", littlecubes);
		landHash.put("glass", glass);
		landHash.put("pyramids", pyramids);
		landHash.put("neon", neon);
		landHash.put("krypton", krypton);
		landHash.put("junk", junk);
		landHash.put("cliffs", cliffs);
		landHash.put("anime", anime);
		landHash.put("change", change);
		landHash.put("dragons", dragons);
		landHash.put("frost", frost);
		landHash.put("repetition", repetition);
	}
}
