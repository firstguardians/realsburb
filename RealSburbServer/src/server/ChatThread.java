package server;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import chat.MessagePacket;

public class ChatThread{
	Sendificator send = new Sendificator();
	private static Boolean sendificatorRunning = true;
	
	//TODO Turn all of these into a single Map of session(key) player
	// Can wait until Game is done
	private static HashSet<PrintWriter> writers = new HashSet<PrintWriter>();
	private static ArrayList<MessagePacket> messagesToBroadcast = new ArrayList<MessagePacket>();
	private static ArrayList<Integer> globalPlayers = new ArrayList<Integer>();
	private static Map<Integer, ArrayList<PrintWriter>> sessionWriters = new HashMap<Integer, ArrayList<PrintWriter>>();
	private static Map<Integer, PrintWriter> privWriters = new HashMap<Integer, PrintWriter>();
	private static ArrayList<String> onlineHandles = new ArrayList<String>();
	PrintWriter tempWriter;

	public ChatThread(){
		send.start();
	}

	public void addPlayer(int pid, String handle, PrintWriter w){
		globalPlayers.add(pid);
		writers.add(w);
		privWriters.put(pid, w);
		onlineHandles.add(handle);
		// Add Player to all online player's lists and notify that they are online
		//TODO run this through the queue
		for (PrintWriter op : writers) {
			op.println("CHAT LIST+ " + handle);
			if(op != w){ // Do not send to the player that just logged in
				op.println("CHAT DISP -- " + handle + " is online --");
			}
		}
		// Get this player's writer
		PrintWriter thisPlayer = privWriters.get(pid);
		// Send Current userlist to Added Player
		// TODO run this through the queue
		for (String handleList : onlineHandles){
			// send username
			thisPlayer.println("CHAT LIST+ " + handleList);
			//System.out.println("CHAT LIST+ " + ph.getHandle(op));
		}
	}

	public void removePlayer(int pid, String handle){
		System.out.println("** removing " + pid);
		// remove from onlinePlayers
		globalPlayers.remove(globalPlayers.indexOf(pid));
		PrintWriter w = privWriters.get(pid);
		privWriters.remove(pid); // remove writer with pid
		writers.remove(w); // remove writer
		for (PrintWriter op : writers) {
			op.println("CHAT LIST- " + handle);
			op.println("CHAT DISP -- " + handle + " went offline --");
		}
	}

	public Boolean isOnline(int pid){
		Boolean online = true;
		if (!globalPlayers.contains(pid)){
			online = false;
		}
		System.out.println("Players online:");
		for(int x : globalPlayers){
			System.out.println(x);
		}
		System.out.println("-- End --");
		return online;
	}

	public void recieveMessage(MessagePacket packet) {
		messagesToBroadcast.add(packet);
	}

	private static class Sendificator extends Thread {
		public void run(){
			System.out.println("Start sendificator");
			while(sendificatorRunning){
				//System.out.println("Broadcast is empty: " + stringsToBroadcast.isEmpty());
				while(!messagesToBroadcast.isEmpty()){
					// Unpack Packet
					MessagePacket firstPacket = messagesToBroadcast.get(0);
					String destination = firstPacket.getGroup();
					String message = firstPacket.getMessage();
					System.out.println("message: " + message);
					int sendToPid = firstPacket.getRecievingPID();
					// Send to specified players
					if(destination.equals("all")){
						for(PrintWriter op : writers){
							op.println(message);
							op.flush();
						}
					}
					else if(destination.equals("pid")){ // Sends chat message to one pid
						PrintWriter privW = privWriters.get(sendToPid);
						privW.println(message);
					}
					else if(destination.equals("session")){
						// TODO
					}
					messagesToBroadcast.remove(0);
				}
				rest100();
			}
		}

		public void rest100(){
			try {
				TimeUnit.MILLISECONDS.sleep(100);
			} catch (InterruptedException e) {
				System.out.println("Sleep failed in Chat thread");
				e.printStackTrace();
			}
		}
	}

}
