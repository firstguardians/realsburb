package server;

import gamePlay.RoomHandler;
import gamePlay.Use;

import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;

import databaseFunctions.MainMenuFunctions;
import databaseFunctions.PlayerHandling;
import databaseFunctions.RoomFunctions;

public class GameThread extends Thread{
	PrintWriter out;
	InputHandler i;
	ChatThread ct;

	MainMenuFunctions mmf;
	PlayerHandling ph;
	RoomFunctions rf;

	RoomHandler room;
	Use use; // Might not need to import - only used in Room and room is imported

	//String input = "";
	private Boolean loggedIn = false;
	@SuppressWarnings("unused") // is in fact used
	private Boolean inaRoom = false;
	
	private boolean running = true;
	
	public void killGameLoops(){
		running = false;
	}
	
	public boolean isLoggedIn(){
		return loggedIn;
	}

	public GameThread(PrintWriter o, MainMenuFunctions m, PlayerHandling p, 
			RoomFunctions r, RoomHandler rh, Use u, ChatThread c, InputHandler ip){
		out = o;
		mmf = m;
		ph = p;
		rf = r;
		room = rh;
		use = u;
		ct = c;
		i = ip;
	}

	public void run(){
		login();
	}

	/*public void recieveMessage(String message) {
		// Every time GameThread recieves a message store it in input
		System.out.println("GAME DISP Game Recieved: " + message);
		if(message.startsWith("ROOM ")){
			room.acceptInput(message.substring(5));
		}
		else{
			input = message;
		}
	}*/

	private void login(){
		String menuInput = "";
		// Temporary Variables
		String handle = "";
		String pass01 = "";
		String pass02 = "";
		String name = "";
		String gender ="";
		String sessionName ="";
		String sessPass01 = "";
		String sessPass02 = "";

		while(!loggedIn && running){
			out.println("GAME Q Do you wish to |LOGIN|, create a new |PLAYER|, |JOIN| a Session or create a new |SESSION|?");
			menuInput = i.waitForInput().toLowerCase();
			// ------------------------------------------------------------------------
			// /////////// NEW PLAYER ///////////////////////////
			// ------------------------------------------------------------------------
			if (menuInput.equals("player")){
				Boolean usernameTaken = true;
				while(usernameTaken){
					Boolean capnumPass = false;
					Boolean capposPass = false;
					out.println("GAME Q Please enter your chumhandle:");
					handle = i.waitForInput(); // Keep caps
					// check for appropriate capitalization
					if(!Character.isUpperCase(handle.charAt(0))){ // if first letter is not capital
						capposPass = true;
						int capcount = 0;
						for(int i=0; i<= handle.length()-1;i++){ // count capital
							if(Character.isUpperCase(handle.charAt(i))){
								capcount++;
							}
						}
						if(capcount != 1){
							//System.out.println("Char: " +"\""+ handle.charAt(0) +"\"");
							out.println("GAME DISP You can only have one capital letter.");
						}
						else{
							capnumPass = true;
						}
					}
					else{
						out.println("GAME DISP The first letter cannot be capital.");
					}
					if(capnumPass && capposPass){
						if(!mmf.handleExists(handle)){
							out.println("GAME DISP Handle " + handle + " Accepted.");
							usernameTaken = false;
						}
						else{
							out.println("GAME DISP Handle " + handle + " already exists. Please select another.");
							waitMS(10);
						}
					}
				}
				Boolean passwordsMismatch = true;
				while(passwordsMismatch){
					out.println("GAME Q Please enter Password:");
					pass01 = i.waitForInput();
					out.println("GAME Q Please reenter Password:");
					pass02 = i.waitForInput();
					if(pass01.equals(pass02)){
						out.println("GAME DISP Password Accepted");
						passwordsMismatch = false;
					}
					else{
						out.println("GAME DISP Passwords do not match. Please Try again.");
						waitMS(10);
					}
				}

				out.println("GAME Q Please enter Player Name:");
				String namein = i.waitForInput(); // keep caps
				name = namein.substring(0, 1).toUpperCase() + namein.substring(1); // Verify that first letter is caps
				Boolean selectingGender = true;
				while(selectingGender){
					out.println("GAME Q Is "+name+" a |BOY|, a |GIRL|, or |NEITHER|?:");
					gender = i.waitForInput().toLowerCase();
					if(gender.equals("boy")||gender.equals("girl")||gender.equals("neither")){
						if(gender.equals("neither")){
							out.println("GAME DISP "+name+" is neither gender!");	
						}
						else{
							out.println("GAME DISP "+name+" is a "+gender+"!");
						}
						selectingGender = false;
					}
					else{
						out.println("GAME DISP Please choose one of the listed genders.");
						waitMS(10);
					}
				}
				// Create player
				mmf.createPlayer(name, handle, pass01, gender);
				out.println("GAME DISP New Player "+ name + " successfully created.");
				// Pause before GAME DISPing the menu again
				loopWait();
			}
			// ------------------------------------------------------------------------
			// /////////// LOGIN ///////////////////////////
			// ------------------------------------------------------------------------
			else if (menuInput.equals("login")){
				Boolean loggingIn = true;
				while(loggingIn){
					out.println("GAME Q What is your handle?:");
					handle = i.waitForInput();
					out.println("GAME Q What is your password?:");
					pass01 = i.waitForInput();
					// find player and retrieve sessionID and playerID
					if(mmf.verifyLogin(handle, pass01))
					{
						loadInitialVariables(handle, pass01);
						if(!ct.isOnline(room.getPid())){
							out.println("CHAT LOGGEDIN");
							loggedIn = true;
							loggingIn = false;
							out.println("GAME DISP Login Successful!");
							loadGame();
						}
						else{
							out.println("GAME DISP That user is already online.");
							waitMS(10);
							out.println("GAME DISP Please contact an administrator if this is not correct.");
							waitMS(10);
							Boolean asking = true;
							while(asking){
								out.println("GAME Q Would you like to try logging in again?:([y]es|[n]o)");
								String yn = i.waitForInput().toLowerCase();
								if(yn.equals("yes")||yn.equals("y")){
									asking = false;
								}
								else if(yn.equals("no")||yn.equals("n")){
									asking = false;
									loggingIn = false;
								}
								else{
									out.println("GAME DISP "+yn+" not understood.");
									waitMS(10);
								}
							}

						}
					}
					else{
						System.out.println("Varify Fail");
						out.println("GAME Q Login Failed. Unknown Handle or Password. |C|ontinue or |Cancel|.");
						String re = i.waitForInput().toLowerCase();
						if(re.equals("cancel")){
							break;
						}
					}
					loopWait();
				}
				inaRoom = true;
			}
			// ------------------------------------------------------------------------
			// ------------------------------------------------------------------------
			else if (menuInput.equals("join")){
				// PlayerID
				out.println("GAME Q What is your handle (or Cancel[x]):");
				handle = i.waitForInput();
				if(handle.equals("cancel")|| handle.equals("x")){
					// Go to beginning of Loop
				}
				else{
					out.println("GAME Q What is your password:");
					pass01 = i.waitForInput();
					if(mmf.verifyLogin(handle, pass01)){
						// Set the PlayerID After Login
						room.setPid(mmf.getPid(handle, pass01));
						// Session Info
						out.println("GAME Q What is the name of the Session:");
						sessionName = i.waitForInput();
						out.println("GAME Q What is the password:");
						sessPass01 = i.waitForInput();
						// Get sessionId from ALLSESSIONS
						int joinSID = mmf.verifyJoin(sessionName, sessPass01);
						System.out.println("JoinSID:" + joinSID);
						// set current, ALLPLAYERS, ALLPLANETS, and ALLMAPIDS sessionID
						if(joinSID != -1){
							mmf.joinSession(room.getPid(), joinSID);
							out.println("GAME DISP Successfully Joined session "+capitalize(sessionName)+".");
							// Set SessionID
							room.setSid(joinSID); //TODO Not setting in database
							System.err.println("PID: " + room.getPid());
							System.err.println("SID set to: "+room.getSid());
						}
						else{
							out.println("GAME DISP Incorrect Session name or password.");
						}
					}
					else{
						out.println("GAME DISP Incorrect handle or password.");
					}
				}
			}
			// ------------------------------------------------------------------------
			// ------------------------------------------------------------------------
			else if (menuInput.equals("session")){
				Boolean sessionNameInUse = true;
				while(sessionNameInUse){
					out.println("GAME Q What would you like the session to be called?:");
					sessionName = i.waitForInput();
					if(!mmf.sessionExists(sessionName)){
						sessionNameInUse = false;
					}
					else{
						out.println("GAME DISP That name is in use. Please select another.");
						waitMS(10);
					}
				}
				Boolean sessionPasswordsMatch = false;
				while(!sessionPasswordsMatch){
					out.println("GAME Q Please enter password:");
					sessPass01 = i.waitForInput();
					out.println("GAME Q Please reenter password:");
					sessPass02 = i.waitForInput();
					if(sessPass01.equals(sessPass02)){
						sessionPasswordsMatch = true;
					}
					else{
						out.println("GAME DISP Entered passwords do not match. Please try again.");
						waitMS(10);
					}
				}
				// Create Session Entry
				mmf.createSession(sessionName, sessPass01);
				out.println("GAME DISP New Session "+ sessionName + " successfully created.");
			}

			// ------------------------------------------------------------------------
			// ------------------------------------------------------------------------
			else{
				out.println("GAME DISP \"" + menuInput + "\"" + " not understood.");
			}
		}
		// Else
	}

	


	public void waitMS(int ms){
		try {
			//System.out.println("Sleep 10ms");
			TimeUnit.MILLISECONDS.sleep(ms);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void loopWait(){
		try {
			TimeUnit.MILLISECONDS.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void loadGame(){
		// add player to online players
		ct.addPlayer(room.getPid(), ph.getHandle(room.getPid()),out);
		// Start timer if timer running
		if(rf.checkTFBoolean("RunTimer", room.getPid())){
			use.startTimer(rf.getTime(room.getPid()));
		}
		// Load First Map
		int mid = ph.getMid(room.getPid());
		System.out.println("Loading First Room mid:" + mid);
		room.setSid(mmf.getSid(room.getPid()));
		room.changeMap(mid);
	}

	private void loadInitialVariables(String handle, String password) {
		// Set PlayerID
		room.setPid(mmf.getPid(handle,password));
		System.out.println("Returned: " + room.getPid());
		// Set Use PID
		use.storeUsePID(room.getPid());
		// Set SessionID
		room.setSid(mmf.getSid(room.getPid()));
		// Set HP
		out.println("UPDATE STAT HP "+ph.getGelViscosity(room.getPid()));
		// Load Inventory
		out.println("UPDATE INVENTORY "+ ph.getPlayerInventoryString(room.getPid()));
	}

	public String capitalize(String s){
		return s.substring(0, 1).toUpperCase() + s.substring(1);
	}

}
