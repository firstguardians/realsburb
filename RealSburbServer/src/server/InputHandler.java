package server;

import java.util.concurrent.TimeUnit;

public class InputHandler {
	String input = "";
	boolean running = true;
	int emptyCount = 0;

	public void setInput(String s){
		input = s;
	}

	/**
	 * Pauses until input is received
	 * @return
	 */
	public String waitForInput(){
		String str = "";
		Boolean lookingForInput = true;
		while(lookingForInput && running){
			//System.out.println("WaitForInput Running");
			if(input.equals("")){
				try {
					TimeUnit.MILLISECONDS.sleep(300);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			else{
				System.out.println("GameInputRecognized: " + input);
				str = input;
				lookingForInput = false;
				input = "";
			}
		}
		// Returns an empty string on close
		System.out.println("WaitForInput Returning: "+ str);
		try {
			TimeUnit.MILLISECONDS.sleep(300);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(str.equals("")){ // If spamming an empty string ... make it crash
			if(emptyCount > 1){
				str = null;
			}
			emptyCount ++;
		}
		else{
			emptyCount = 0;
		}
		return str;	
	}

	public void killLoop(){
		running = false;
	}
}
