package server;

import gamePlay.RoomHandler;
import gamePlay.Use;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import chat.MessagePacket;
import databaseFunctions.MainMenuFunctions;
import databaseFunctions.PlayerHandling;
import databaseFunctions.ReadAccess;
import databaseFunctions.RoomFunctions;
import databaseFunctions.ThreadID;
import databaseQueue.DatabaseThread;


public class Receiver{
	//Boolean running = true;
	private static final int PORT = 9001;
	//
	int tempid = 0;

	// initiate chat thread
	static ChatThread chatThread = new ChatThread();
	static DatabaseThread databaseThread = new DatabaseThread();

	public Receiver(){

		try {
			acceptConnections();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void acceptConnections() throws Exception{
		System.out.println("Starting Accept Connections.");
		ServerSocket listener = new ServerSocket(PORT);
		try {
			while (true) {
				int locTid = tempid;
				System.out.println("Making New Thread ID" + locTid);
				PlayerThread h = new PlayerThread(listener.accept(), locTid);
				databaseThread.addThread(locTid, h);
				//call start from the list of threads
				databaseThread.startThread(locTid);
				// iterate tempID for the next connection
				tempid++;
			}
		} finally {
			listener.close();
		}
	}

	public static class PlayerThread extends Thread {
		// Database Connection
		//Connection c = openReadConnection();
		// Database Classes
		ThreadID databaseVars = new ThreadID();
		ReadAccess readAccess = new ReadAccess(databaseVars);
		PlayerHandling ph = new PlayerHandling(databaseThread, databaseVars,readAccess);
		MainMenuFunctions mmf = new MainMenuFunctions(databaseThread, databaseVars,readAccess);
		RoomFunctions rf = new RoomFunctions(databaseThread, databaseVars,readAccess);

		// Game Classes
		RoomHandler room;
		Use use;
		InputHandler i;
		GameThread gameThread;
		private int lastPong = 0;
		private int tid;

		//private String receivedData;
		private Socket socket;
		private BufferedReader in;
		private PrintWriter out;

		Timer pingTimer = new Timer();
		ScheduleTask sched = new ScheduleTask();

		/**
		 * Constructs a handler thread, squirreling away the socket.
		 * All the interesting work is done in the run method.
		 */
		public PlayerThread(Socket socket, int tempid) {
			this.socket = socket;
			tid = tempid;
			// pass tid into database functions so the queue can return to the tid thread
			databaseVars.setTid(tid);
		}

		public void run() {
			try {
				String prefix = "";

				// Create character streams for the socket.
				in = new BufferedReader(new InputStreamReader(
						socket.getInputStream()));
				out = new PrintWriter(socket.getOutputStream(), true);
				// initiate Input Handler
				i = new InputHandler();
				// initiate Use
				use = new Use(out,rf,ph,i,chatThread);
				// initiate RoomHandler
				room = new RoomHandler(out,ph,rf,use,i);

				// Start Timer
				pingTimer.scheduleAtFixedRate(sched, 3000, 3000);

				//chatThread.addPlayer(out);

				Boolean runningX = true;
				// First message sent to the Client used to be here

				// Init Game Thread
				gameThread = new GameThread(out, mmf, ph, rf, room, use, chatThread, i);
				gameThread.start();

				// Accept messages from this client
				while (runningX) {
					if(!in.equals(null)){
						// Receive input
						if(in.ready()){
							String input = in.readLine();
							if(!input.equals("")){
								// Do not show that Pong was recieved
								if(!input.equals("P")){
									System.out.println("Recieved: " + input);
								}

								// Process input
								// ------------------------------------------------------
								if(input.equals("P")){
									//reset lastPong
									lastPong = 0;
									//System.out.println("Pong Recieved");
								}
								else if(input.startsWith("CHAT ")){
									String text = input.substring(5);
									if(prefix.equals("")){
										//TODO store this on login instead of calling it repeatedly
										String handle = ph.getHandle(room.getPid());
										// split handle
										String[] r = handle.split("(?=\\p{Upper})");
										prefix = "C" + r[0].substring(0,1) + r[1].substring(0,1);
										prefix = prefix.toUpperCase() + ": ";
									}
									// send to chat // Global Chat Message atm
									//TODO make this a session message
									chatThread.recieveMessage(new MessagePacket("all", room.getPid(), prefix + text));
								}
								// ------------------------------------------------------
								else if(input.startsWith("GAME ")){
									// send to game via InputHandler
									i.setInput(input.substring(5));
								}
								// ------------------------------------------------------
								else{
									System.out.println("Received Strange Message: " + input);
								}
								// ------------------------------------------------------
							}
						}
						try {
							TimeUnit.MILLISECONDS.sleep(100);
						} catch (InterruptedException e) {
							System.out.println("Sleep failed in Receiver thread");
							e.printStackTrace();
						}
					}else{
						System.out.println("Not Ready");
						runningX = false;
						socket.close();
					}
				}
			} catch (IOException e) {
				System.out.println(e);
			} finally {
				closeThread();
			}
		}

		private void closeThread() {
			// Socket Closing - Cleanup
			System.out.println("Closing Thread.");
			// Kill Game Loops
			System.out.println("Stopping GameThread");
			gameThread.killGameLoops();
			// Stop Use Loops
			System.out.println("Stopping Use");
			use.stopUseLoops();
			// Stop room
			System.out.println("Stopping Room Handler");
			room.closeAllRoomLoops();
			// Kill Waiting for input
			System.out.println("Stopping InputHandler");
			i.killLoop();
			// Stop Timer
			System.out.println("Stopping Ping");
			pingTimer.cancel();
			// Requires being logged in to stop
			if (out != null && gameThread.isLoggedIn()) {
				// Set Pid
				int pid = room.getPid();
				System.out.println("Closing Pid: " + pid);
				//int sid = room.sid; //Use with chat later
				// Remove Player from Chat
				chatThread.removePlayer(pid, ph.getHandle(pid));
				// Stop Meteor Countdown Timer
				use.stopTimer();
			}
			// Remove thread from list
			System.out.println("Removing thread"+tid + " from HashMap");
			databaseThread.removeThread(tid);
			try {
				socket.close();
			} catch (IOException e) {
				System.out.println("Problem closing Socket.");
			}
			System.out.println("Finished Closing Thread.");
		}

		class ScheduleTask extends TimerTask {
			public void run() {
				lastPong++;
				//System.out.println("LastPong"+lastPong);
				if(lastPong > 2){ // If a pong is missed for 9 (3*3sec) seconds close the thread
					closeThread();
				}
			}
		}

		public ThreadID getDatabaseVariables(){
			return databaseVars;
		}

		public static void main(String[] args) {
			new Receiver();
		}

		public void sayID(){
			System.out.println("Thread" + this.getId() + "says hello!");
		}

		private Connection openReadConnection(){
			Connection c = null;
			// Open a connection to the database
			try {
				Class.forName("org.sqlite.JDBC");
				c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
				c.setAutoCommit(false);
			} catch (ClassNotFoundException e) {
				System.out.println("*** Class not Found - Sqlite JDBC not found ***");
				e.printStackTrace();
			} catch (SQLException e) {
				System.out.println("*** Could not create a connection to the database ***");
				e.printStackTrace();
			}
			return c;
		}

	}
}
