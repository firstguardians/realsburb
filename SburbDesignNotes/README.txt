--~~ --------------------------------- Controls --------------------------------------- ~~--

--~~ --------------------------------- Credits ---------------------------------------- ~~--

-- Programmers --
skIttles - Lead Programmer, Trickster

-- Technical Advisors --
erraticAesculapian - Experienced Technical Advice
humbleElitist - Experienced Technical Advice, Assistance Debugging the Chat/Game threads
doomedCloud - GIT setup Assistance
oneiricVariable - Technical Advice, Alpha Testing, LandGen Words


-- Beta Testers --


-- Alpha Testers --
dropJuice - Alpha testing, Array filling for items and echeladder
paramountWaterloo
cobCorn
asigentInferior
angellightSunmay
frenchHipster
blindProphet
discoPogo
billybobMario
hollisticSlacker

-- Other Credits --
nearlyNonexistent - Technical Advice
amauroticBasshead - Page after page of LandGen Words
enclosedProgrammer - LandGen Words, Assistance Debugging
perennialDissonance - Alpha Testing - Banned for inappropriate conduct 

--~~ ----------------------------------------------------------------------------------- ~~--
